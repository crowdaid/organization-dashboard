import React, { Component } from 'react';
import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';
import {
  List, Input, Button, message,
} from 'antd';
import { FormattedMessage } from 'react-intl';
import moment from 'moment';
import UserAvatar from '../../components/UserAvatar';
import CasesNotesCollection from '../../api/CasesNotes';


class Notes extends Component {
  constructor(props) {
    super(props);
    this.state = {
      addNew: false, description: '',
    };
    this.handleShowForm = this.handleShowForm.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleAddNote = this.handleAddNote.bind(this);
  }

  handleShowForm() {
    this.setState({ addNew: true });
  }

  handleInputChange(e) {
    const { value } = e.target;
    this.setState({ description: value });
  }

  handleAddNote(e) {
    e.preventDefault();
    const { caseId } = this.props;
    const { description } = this.state;
    if (!description) {
      message.error('Please enter a note!');
      return;
    }
    Meteor.call('casesNotes.methods.create', { description, caseId }, (err, res) => {
      if (!err) {
        message.success('Note added successfully');
        this.setState({ addNew: false, description: '' });
      } else message.error(err.reason);
    });
  }


  render() {
    const {
      description, addNew,
    } = this.state;
    const { notes, loading } = this.props;
    return (
      <div>
        <div className="notes__actions">
          <Button
            style={{ border: 'none' }}
            type="ghost"
            icon="form"
            onClick={this.handleShowForm}
          >
            <FormattedMessage
              id="cases.staffnotes.addnew"
              defaultMessage="Add new note"
            />
          </Button>
        </div>
        {addNew && (
        <form onSubmit={this.handleAddNote}>
          <div className="form-group">
            <Input.TextArea
              onChange={this.handleInputChange}
              value={description}
              cols={30}
              rows={4}
            />
          </div>
          <Button block icon="plus" htmlType="submit">
            <FormattedMessage
              id="cases.staffnotes.addnewbutton"
              defaultMessage="Add"
            />
          </Button>
        </form>
        )}
        {/* all notes on the case */}
        <List
          itemLayout="vertical"
          dataSource={notes}
          loading={loading}
          renderItem={({
            description: desc, title, createdAt, userId,
          }) => (
            <List.Item>
              <List.Item.Meta
                avatar={<UserAvatar userId={userId} />}
                title={(
                  <div className="notes__title">
                    {userId === Meteor.userId()
                      ? <b>You</b> : <b>{title}</b>}
                    <p style={{ color: 'gray' }}>
                      {moment(createdAt).format('ddd, MMMM, hA')}
                    </p>
                  </div>
                )}
                description={<div style={{ width: 300, whiteSpace: 'break-word' }}>{desc}</div>}
              />
            </List.Item>
          )}
        />
      </div>
    );
  }
}

export default withTracker((props) => {
  const { caseId } = props;
  const notesHandle = Meteor.subscribe('CaseNotesAll', {
    filter: {},
    limit: 0,
    caseId,
  });
  if (!notesHandle.ready()) {
    return ({
      loading: true,
      notes: [],
    });
  }


  const notes = CasesNotesCollection.find({ caseId }, {
    sort: { createdAt: -1 },
  }).fetch();
  return ({
    loading: false,
    notes,
  });
})(Notes);
