import React from 'react';
import PropTypes from 'prop-types';
import Form from '../Form';

const Create = ({ match: { params: { id } } }) => <Form formType="create" matchId={id} />;

Create.propTypes = {
  match: PropTypes.instanceOf(Object).isRequired,
};

export default Create;
