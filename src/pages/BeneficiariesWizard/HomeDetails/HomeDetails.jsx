import React, { Component } from 'react';
import { Card } from 'antd';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';

class View extends Component {
  constructor(props) {
    super(props);
    this.state = {
      homeDescription: '',
    };
  }

  componentDidMount() {
    const { homeDescription } = this.props;
    if (homeDescription) {
      this.loadBeneficiary(homeDescription);
    }
  }

  loadBeneficiary(homeDescription) {
    this.setState({
      homeDescription,
    });
  }

  render() {
    const {
      homeDescription,
    } = this.state;
    const {
      ownershipType, ceilingConditionOthers, rentMonyValue,
      ceilingCondition, wallsType, wallsTypeOthers,
      floorsCondition, notes, WC, waterAndElectric,
      resourcesTypes, room1, room2, room3, room4, farmingLand,
      properties, landFieldsNumber, otherProperties,
    } = homeDescription;
    if (!homeDescription) {
      return (
        <b>
          <FormattedMessage
            defaultMessage="No Home Description for this beneficiary"
            id="homeDescription.view.noHomeDes"
          />
        </b>
      );
    }
    return (
      <div className="container">
        <h3>
          <FormattedMessage
            defaultMessage="Home description and possessions "
            id="homeDescription.view.title"
          />
        </h3>
        <div>
          {/* family table */}
          <Card>
            <div>
              <Card
                type="inner"
                bordered={false}
                title={(
                  <FormattedMessage
                    defaultMessage="Home description"
                    id="homeDetails.form.card.homeDescription"
                  />)}
              >
                <table className="view__table">
                  <tbody className="view__table__body">
                    <tr className="view__table__body__row">
                      <td>
                        <b>
                          <FormattedMessage
                            defaultMessage="Ownership type"
                            id="homeDetails.form.ownershipType"
                          />
                        </b>
                      </td>
                      <td>
                        {ownershipType === 'rent' ? (
                          <FormattedMessage
                            defaultMessage={rentMonyValue}
                            id="homeDescription.view.ownershipType.rent"
                            values={{ rentMonyValue }}
                          />
                        ) : (
                          <FormattedMessage
                            defaultMessage="owned"
                            id="homeDetails.form.ownershipType.ownership"
                          />
                        )}

                      </td>
                    </tr>
                    <tr>
                      <td>
                        <b>
                          <FormattedMessage
                            defaultMessage="Ceiling condition"
                            id="homeDetails.form.ceilingCondition"
                          />
                        </b>
                      </td>
                      <td>{ceilingCondition !== 'others' ? ceilingCondition : ceilingConditionOthers}</td>
                    </tr>
                    <tr>
                      <td>
                        <b>
                          <FormattedMessage
                            defaultMessage="Walls type"
                            id="homeDetails.form.wallsType"
                          />
                        </b>
                      </td>
                      <td>{wallsType !== 'others' ? wallsType : wallsTypeOthers}</td>
                    </tr>
                    <tr>
                      <td>
                        <b>
                          <FormattedMessage
                            defaultMessage="Floors condition"
                            id="homeDetails.form.floorsCondition"
                          />
                        </b>
                      </td>
                      <td>{floorsCondition}</td>
                    </tr>
                    <tr>
                      <td>
                        <b>
                          <FormattedMessage defaultMessage="WC" id="homeDetails.form.WC" />
                        </b>
                      </td>
                      <td>{WC}</td>
                    </tr>
                    <tr>
                      <td>
                        <b>
                          <FormattedMessage
                            defaultMessage="Water And Electric"
                            id="homeDetails.form.waterAndElectric"
                          />
                        </b>
                      </td>
                      <td>{waterAndElectric}</td>
                    </tr>
                    <tr>
                      <td>
                        <b>
                          <FormattedMessage
                            defaultMessage=" Resources Types"
                            id="homeDetails.form.resourcesTypes"
                          />
                        </b>
                      </td>
                      <td>{resourcesTypes}</td>
                    </tr>
                    {room1
                  && (
                  <tr>
                    <td>
                      <b>
                        <FormattedMessage defaultMessage="room1" id="homeDetails.form.rooms.room1" />
                      </b>
                    </td>
                    <td>{room1}</td>
                  </tr>)}
                    {room2
                  && (
                  <tr>
                    <td>
                      <b>
                        <FormattedMessage defaultMessage="room2" id="homeDetails.form.rooms.room2" />
                      </b>
                    </td>
                    <td>{room2}</td>
                  </tr>)}
                    {room3
                  && (
                  <tr>
                    <td>
                      <b>
                        <FormattedMessage defaultMessage="room3" id="homeDetails.form.rooms.room3" />
                      </b>
                    </td>
                    <td>{room3}</td>
                  </tr>)}
                    {room4
                  && (
                  <tr>
                    <td>
                      <b>
                        <FormattedMessage defaultMessage="room4" id="homeDetails.form.rooms.room4" />
                      </b>
                    </td>
                    <td>{room4}</td>
                  </tr>)}
                    <tr>
                      <td>
                        <b>
                          <FormattedMessage defaultMessage="notes" id="homeDetails.form.notes" />
                        </b>
                      </td>
                      <td>{notes}</td>
                    </tr>
                  </tbody>
                </table>
              </Card>
              <Card
                type="inner"
                bordered={false}
                title={(
                  <FormattedMessage
                    defaultMessage="Precessions"
                    id="homeDetails.form.card.properties"
                  />)}
              >
                <table className="view__table">
                  <tbody className="view__table__body">
                    <tr className="view__table__body__row">
                      <td>
                        <p>{properties}</p>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <b>
                          <FormattedMessage
                            defaultMessage="farmingLand"
                            id="homeDetails.form.farmingLand"
                          />
                        </b>
                      </td>
                      <td>
                        {farmingLand ? landFieldsNumber : (
                          <FormattedMessage
                            defaultMessage="No data"
                            id="beneficiaries.view.notFound"
                          />
                        )}
                      </td>
                    </tr>
                    { otherProperties && (
                    <tr>
                      <td>
                        <b>
                          <FormattedMessage
                            defaultMessage="Other properties"
                            id="homeDetails.form.otherProperties"
                          />
                        </b>
                      </td>
                      <td>{otherProperties}</td>
                    </tr>)}
                  </tbody>
                </table>
              </Card>
            </div>
          </Card>
        </div>
      </div>
    );
  }
}
View.defaultProps = {
  homeDescription: null,
};
View.propTypes = {
  homeDescription: PropTypes.instanceOf(Object),
};


export default (View);
