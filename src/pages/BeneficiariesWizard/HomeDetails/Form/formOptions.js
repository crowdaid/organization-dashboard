import React from 'react';
import { FormattedMessage } from 'react-intl';


const options = {
  ceilingConditionOptions: [
    {
      label: <FormattedMessage
        id="homeDetails.options.redBricks"
        defaultMessage="Red Bricks"
      />,
      value: 'redBricks',
    },
    {
      label: <FormattedMessage
        id="homeDetails.options.brickBrick"
        defaultMessage="Brick Brick"
      />,
      value: 'brickBrick',
    },
    {
      label: <FormattedMessage
        id="homeDetails.options.wovenWood"
        defaultMessage="Woven Wood"
      />,
      value: 'wovenWood',
    },
    {
      label: <FormattedMessage
        id="homeDetails.options.others"
        defaultMessage="Others"
      />,
      value: 'others',
    },
  ],

  wallsTypeOptions: [
    {
      label: <FormattedMessage
        id="homeDetails.options.concrete"
        defaultMessage="Concrete"
      />,
      value: 'concrete',
    },
    {
      label: <FormattedMessage
        id="homeDetails.options.wood"
        defaultMessage="Wood"
      />,
      value: 'wood',
    },
    {
      label: <FormattedMessage
        id="homeDetails.options.wovenWood"
        defaultMessage="Woven Wood"
      />,
      value: 'wovenWood',
    },
    {
      label: <FormattedMessage
        id="homeDetails.options.others"
        defaultMessage="Others"
      />,
      value: 'others',
    },
  ],
  WCOptions: [

    {
      label: <FormattedMessage
        id="homeDetails.options.privet"
        defaultMessage="Privet"
      />,
      value: 'privet',
    },
    {
      label: <FormattedMessage
        id="homeDetails.options.mutual"
        defaultMessage="Mutual"
      />,
      value: 'mutual',
    },
  ],
  waterAndElectricOptions: [

    {
      label: <FormattedMessage
        id="homeDetails.options.water"
        defaultMessage="Water"
      />,
      value: 'water',
    },
    {
      label: <FormattedMessage
        id="homeDetails.options.electricity"
        defaultMessage="Electricity"
      />,
      value: 'electricity',
    },
  ],
  resourcesTypesOptions: [
    {
      label: <FormattedMessage
        id="homeDetails.options.TV"
        defaultMessage="TV"
      />,
      value: 'TV',
    },
    {
      label: <FormattedMessage
        id="homeDetails.options.refrigerator"
        defaultMessage="Refrigerator"
      />,
      value: 'refrigerator',
    },
    {
      label: <FormattedMessage
        id="homeDetails.options.washingMachine"
        defaultMessage="Washing Machine"
      />,
      value: 'washingMachine',
    },
    {
      label: <FormattedMessage
        id="homeDetails.options.homeOven"
        defaultMessage="Home oven"
      />,
      value: 'homeOven',
    },
    {
      label: <FormattedMessage
        id="homeDetails.options.satelliteDish"
        defaultMessage="Satellite Dish"
      />,
      value: 'satelliteDish',
    },
    {
      label: <FormattedMessage
        id="homeDetails.options.computer"
        defaultMessage="Computer"
      />,
      value: 'Computer',
    },
    {
      label: <FormattedMessage
        id="homeDetails.options.mobile"
        defaultMessage="Mobile phone"
      />,
      value: 'mobile phone',
    },
  ],
  propertiesOptions: [
    {
      label: <FormattedMessage
        id="homeDetails.options.buildingsLand"
        defaultMessage="Buildings Land"
      />,
      value: 'buildingsLand',
    },
    {
      label: <FormattedMessage
        id="homeDetails.options.realeStates"
        defaultMessage="Reale States"
      />,
      value: 'realeStates',
    },
    {
      label: <FormattedMessage
        id="homeDetails.options.gold"
        defaultMessage="Gold"
      />,
      value: 'gold',
    },
  ],
};

export default (options);
