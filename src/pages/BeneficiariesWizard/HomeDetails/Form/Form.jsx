import React, { Component } from 'react';
import {
  Input, Form as AntForm, Button, Card,
  message, Radio, Tooltip, Checkbox,
} from 'antd';
import { Meteor } from 'meteor/meteor';
import { withRouter } from 'react-router-dom';
import { FormattedMessage, injectIntl } from 'react-intl';
import PropTypes from 'prop-types';
import options from './formOptions';


class Form extends Component {
  static validateForm(state) {
    if (!state.ownershipType) {
      message.error('You must specify ownership type');
      return false;
    }
    if (state.ownershipType === 'rent' && !state.rentMonyValue) {
      message.error('You must specify rent money value');
      return false;
    }
    if (state.wallsType === 'other' && !state.wallsTypeOthers) {
      message.error('You must specify the other type of walls');
      return false;
    }
    if (state.ceilingCondition === 'other' && !state.ceilingConditionOthers) {
      message.error('You must specify the other type of ceiling');
      return false;
    }
    if (!state.room1 && !state.room2 && !state.room3 && !state.room4) {
      message.error('You must specify at least one room description');
      return false;
    }
    if (state.resourcesTypes.length === 0) {
      message.error('You must specify at least one accessory');
      return false;
    }
    if (!state.WC) {
      message.error('You must specify at WC type');
      return false;
    }
    if (!state.floorsCondition) {
      message.error('You must specify floors number');
      return false;
    }
    if (state.properties.length === 0) {
      message.error('You must specify properties');
      return false;
    }
    if (state.farmingLand && !state.landFieldsNumber) {
      message.error('You must specify land fields number');
      return false;
    }
    return true;
  }

  constructor(props) {
    super(props);
    this.state = {
      ownershipType: '',
      rentMonyValue: '',
      ceilingCondition: '',
      ceilingConditionOthers: '',
      wallsType: '',
      wallsTypeOthers: '',
      floorsCondition: '',
      WC: '',
      waterAndElectric: [],
      resourcesTypes: [],
      room1: '',
      room2: '',
      room3: '',
      room4: '',
      notes: '',
      properties: [],
      farmingLand: false,
      landFieldsNumber: '',
      otherProperties: '',
    };
    // set the title of the page
    const { formType } = props;
    document.title = `Home Details-${formType} | CrowdAid`;
    // bind functions to class
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleRadioSelectChanges = this.handleRadioSelectChanges.bind(this);
    this.handleWaterAndElectricChanges = this.handleWaterAndElectricChanges.bind(this);
    this.handleResourcesTypesChanges = this.handleResourcesTypesChanges.bind(this);
    this.handleInputNumberChange = this.handleInputNumberChange.bind(this);
    this.handlePropertiesChanges = this.handlePropertiesChanges.bind(this);
    this.handleFarmingLandChanges = this.handleFarmingLandChanges.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount() {
    window.scroll(0, 0);
    const { formType, homeDescription } = this.props;
    if (formType === 'edit' && homeDescription) {
      this.loadFamily(homeDescription);
    }
  }

  // load beneficiary info if edit
  loadFamily(homeDescription) {
    this.setState({
      ...homeDescription,
    });
  }

  // general input changes with name
  handleInputChange(e) {
    const { name, value } = e.currentTarget;
    this.setState({ [name]: value });
  }

  handleInputNumberChange(e) {
    const { value, name } = e.currentTarget;
    if (Number(value) / 1 === Number(value)) {
      this.setState({ [name]: value });
    }
  }


  handleRadioSelectChanges(e) {
    const { name, value } = e.target;
    if (name === 'ceilingCondition' && value !== 'others') {
      this.setState({ ceilingConditionOthers: '' });
    } else if (name === 'wallsType' && value !== 'others') {
      this.setState({ wallsTypeOthers: '' });
    }
    this.setState({ [name]: value });
  }

  handleWaterAndElectricChanges(values) {
    this.setState({ waterAndElectric: values });
  }

  handleResourcesTypesChanges(values) {
    this.setState({ resourcesTypes: values });
  }

  handlePropertiesChanges(values) {
    this.setState({ properties: values });
  }

  handleFarmingLandChanges(e) {
    const { checked } = e.target;
    this.setState({ farmingLand: checked });
  }

  handleSubmit(e) {
    e.preventDefault();
    const {
      history, matchId, formType,
    } = this.props;
    if (!Form.validateForm(this.state)) {
      return;
    }
    const homeDescription = this.state;
    Meteor.call('beneficiaries.methods.add_and_update_home_descriptions', {
      _id: matchId,
      homeDescription,
    }, (err, res) => {
      if (err) {
        message.error(`Something went wrong ${err.reason}`);
      } else {
        // redirect with message
        history.push('/beneficiaries');
        if (formType !== 'edit') {
          message.success('Home description added successfully');
        } else {
          message.success('Home description updated successfully');
        }
      }
    });
  }

  render() {
    const {
      ownershipType, ceilingConditionOthers, rentMonyValue,
      ceilingCondition, wallsType, wallsTypeOthers,
      floorsCondition, notes, WC, waterAndElectric,
      resourcesTypes, room1, room2, room3, room4, farmingLand,
      properties, landFieldsNumber, otherProperties,
    } = this.state;
    // Checkboxes Options
    const {
      ceilingConditionOptions,
      propertiesOptions,
      resourcesTypesOptions,
      wallsTypeOptions,
      waterAndElectricOptions,
      WCOptions,
    } = options;
    const { history, formType } = this.props;
    return (
      <div className="container">
        <div className="form__with-back">
          {formType === 'edit'
            ? (
              <h1>
                <FormattedMessage
                  id="homeDetails.form.title.edit"
                  defaultMessage="Edit home details for beneficiary"
                />
              </h1>
            )
            : (
              <h1>
                <FormattedMessage
                  id="homeDetails.form.title.new"
                  defaultMessage="Create home discription for beneficiary"
                />
              </h1>
            )
            }
          <div className="view__actions">
            <Button icon="arrow-left" onClick={() => history.push('/beneficiaries')}>
              <FormattedMessage defaultMessage="Back" id="global.form.backButton" />
            </Button>
            <Button icon="down" href="#properties">
              <FormattedMessage
                defaultMessage=" Home Properties"
                id="homeDetails.form.card.linkto.possessions"
              />
            </Button>
          </div>
        </div>

        <form dir="auto" onSubmit={this.handleSubmit}>
          <div className="flex column">
            <Card
              type="inner"
              size="small"
              title={(
                <FormattedMessage
                  id="homeDetails.form.card.homeDescription"
                  defaultMessage="Beneficiary home description"
                />)}
            >
              <div className="flex row">
                {/* ownershipType  */}
                <Card size="small" className="form-group ">
                  <AntForm.Item
                    label={(
                      <FormattedMessage
                        id="homeDetails.form.ownershipType"
                        defaultMessage="House ownership type"
                      />)}
                    required
                  >
                    <Radio.Group
                      onChange={this.handleRadioSelectChanges}
                      value={ownershipType}
                      name="ownershipType"
                    >
                      <Radio value="owned">
                        <FormattedMessage
                          id="homeDetails.form.ownershipType.ownership"
                          defaultMessage="Owned"
                        />
                      </Radio>
                      <Radio value="rent">
                        <FormattedMessage
                          id="homeDetails.form.ownershipType.Rent"
                          defaultMessage="Rent"
                        />
                      </Radio>
                      {ownershipType === 'rent'
                  && (
                    <Tooltip
                      placement="top"
                      trigger="focus"
                      title={(
                        <FormattedMessage
                          id="homeDetails.form.ownershipType.Rent.toolTip"
                          defaultMessage="Add the rent mony value"
                        />)}
                    >
                      <Input
                        className="homeDetailes__rent__value"
                        name="rentMonyValue"
                        placeholder="10 EGP"
                        value={rentMonyValue}
                        onChange={this.handleInputNumberChange}
                      />
                    </Tooltip>
                  )
                }
                    </Radio.Group>
                  </AntForm.Item>
                </Card>
                {/* ceilingCondition */}
                <Card size="small" className="form-group">
                  <AntForm.Item
                    required
                    label={(
                      <FormattedMessage
                        id="homeDetails.form.ceilingCondition"
                        defaultMessage="The ceiling condition"
                      />)}
                  >
                    <Radio.Group
                      onChange={this.handleRadioSelectChanges}
                      options={ceilingConditionOptions}
                      name="ceilingCondition"
                      value={ceilingCondition}
                    />
                    {ceilingCondition === 'others'
                  && (
                    <Tooltip
                      placement="bottom"
                      title={(
                        <FormattedMessage
                          id="homeDetails.form.ceilingConditionOthers.toolTip"
                          defaultMessage="Add description for the other type of the ceiling"
                        />)}
                    >
                      <Input.TextArea
                        className="homeDetailes__others__value"
                        name="ceilingConditionOthers"
                        value={ceilingConditionOthers}
                        onChange={this.handleInputChange}
                      />
                    </Tooltip>)}
                  </AntForm.Item>
                </Card>
              </div>
              <div className="flex row">
                {/* wallsType */}
                <Card size="small" className="form-group">
                  <AntForm.Item
                    required
                    label={(
                      <FormattedMessage
                        id="homeDetails.form.wallsType"
                        defaultMessage="The walls type"
                      />)}
                  >
                    <Radio.Group
                      onChange={this.handleRadioSelectChanges}
                      options={wallsTypeOptions}
                      name="wallsType"
                      value={wallsType}
                    />
                    {wallsType === 'others'
                  && (
                    <Tooltip
                      placement="bottom"
                      className="homeDetailes__others__value"
                      title={(
                        <FormattedMessage
                          id="homeDetails.form.wallsTypeOthers.toolTip"
                          defaultMessage="Add description for the other type of the walls"
                        />)}
                    >
                      <Input.TextArea
                        name="wallsTypeOthers"
                        value={wallsTypeOthers}
                        onChange={this.handleInputChange}
                      />
                    </Tooltip>)}
                  </AntForm.Item>
                </Card>

                {/* waterAndElectric */}
                <Card size="small" className="form-group">
                  <AntForm.Item
                    required
                    label={(
                      <FormattedMessage
                        id="homeDetails.form.waterAndElectric"
                        defaultMessage="Water and Electricity"
                      />)}
                  >
                    <Checkbox.Group
                      onChange={this.handleWaterAndElectricChanges}
                      options={waterAndElectricOptions}
                      value={waterAndElectric}
                    />
                  </AntForm.Item>
                </Card>
              </div>
              {/* floorsCondition */}
              <div className="form-group">
                <AntForm.Item
                  required
                  label={(
                    <FormattedMessage
                      id="homeDetails.form.floorsCondition"
                      defaultMessage="Number of floors inside the House and description for who live in it"
                    />)}
                >
                  <Input.TextArea
                    cols={50}
                    rows={4}
                    value={floorsCondition}
                    name="floorsCondition"
                    onChange={this.handleInputChange}
                  />
                </AntForm.Item>
              </div>
              <div className="flex row">
                {/* WC */}
                <Card size="small" className="form-group homeDetailes__wc__box">
                  <AntForm.Item
                    label={(
                      <FormattedMessage id="homeDetails.form.WC" defaultMessage="WC" />
                  )}
                    required
                  >
                    <Radio.Group
                      onChange={this.handleRadioSelectChanges}
                      options={WCOptions}
                      name="WC"
                      value={WC}
                    />
                  </AntForm.Item>
                </Card>
                {/* resourcesTypes */}
                <Card size="small" className="form-group ">
                  <AntForm.Item
                    label={(
                      <FormattedMessage
                        id="homeDetails.form.resourcesTypes"
                        defaultMessage="Possessions types"
                      />)}
                    required
                  >
                    <Checkbox.Group
                      onChange={this.handleResourcesTypesChanges}
                      options={resourcesTypesOptions}
                      value={resourcesTypes}
                    />
                  </AntForm.Item>
                </Card>
              </div>
              {/* rooms */}
              <h4 className="form-group">
                <FormattedMessage
                  id="homeDetails.form.rooms.description"
                  defaultMessage="Room descriptions in details"
                />
              </h4>
              <div className="flex row">
                <div className="form-group">
                  <AntForm.Item
                    required
                    label={(
                      <FormattedMessage
                        id="homeDetails.form.rooms.room1"
                        defaultMessage="Room number one"
                      />)}
                  >
                    <Input.TextArea
                      className="homeDetailes__rooms__box "
                      value={room1}
                      name="room1"
                      onChange={this.handleInputChange}
                    />
                  </AntForm.Item>
                </div>
                <div className="form-group">
                  <AntForm.Item
                    label={(
                      <FormattedMessage
                        id="homeDetails.form.rooms.room2"
                        defaultMessage="Room number two"
                      />)}
                  >
                    <Input.TextArea
                      className="homeDetailes__rooms__box "
                      value={room2}
                      name="room2"
                      onChange={this.handleInputChange}
                    />
                  </AntForm.Item>
                </div>
              </div>
              <div className="flex row">
                <div className="form-group">
                  <AntForm.Item
                    label={(
                      <FormattedMessage
                        id="homeDetails.form.rooms.room3"
                        defaultMessage="Room number three"
                      />)}
                  >
                    <Input.TextArea
                      className="homeDetailes__rooms__box "
                      value={room3}
                      name="room3"
                      onChange={this.handleInputChange}
                    />
                  </AntForm.Item>
                </div>
                <div className="form-group">
                  <AntForm.Item
                    label={(
                      <FormattedMessage
                        id="homeDetails.form.rooms.room4"
                        defaultMessage="Room number four"
                      />)}
                  >
                    <Input.TextArea
                      className="homeDetailes__rooms__box "
                      value={room4}
                      name="room4"
                      onChange={this.handleInputChange}
                    />
                  </AntForm.Item>
                </div>
              </div>
              {/* snotes  */}
              <div className="form-group">
                <AntForm.Item
                  label={(
                    <FormattedMessage
                      id="homeDetails.form.notes"
                      defaultMessage="Notes about the home"
                    />)}
                >
                  <Input.TextArea
                    cols={80}
                    rows={5}
                    name="notes"
                    value={notes}
                    onChange={this.handleInputChange}
                  />
                </AntForm.Item>
              </div>
            </Card>
            {/* ------------------------properties-------------------- */}
            <Card
              id="properties"
              type="inner"
              size="small"
              title={(
                <FormattedMessage
                  id="homeDetails.form.card.properties"
                  defaultMessage="Properties"
                />)}
            >
              {/* Properties  */}
              <div className="form-group">
                <Checkbox.Group
                  onChange={this.handlePropertiesChanges}
                  options={propertiesOptions}
                  value={properties}
                />
              </div>
              {/* Properties  */}
              <div className="form-group">
                <FormattedMessage
                  id="homeDetails.form.farmingLand"
                  defaultMessage="Farming Land"
                />
                {' '}
                <Checkbox
                  onChange={this.handleFarmingLandChanges}
                  value={farmingLand}
                />
                {farmingLand
                && (
                <div className="homeDetailes__rent__value">
                  <Tooltip
                    placement="bottom"
                    className="homeDetailes__others__value"
                    title={(
                      <FormattedMessage
                        id="homeDetails.form.landFieldsNumber.toolTip"
                        defaultMessage="Number of fields of the farming land"
                      />)}
                  >
                    <Input
                      name="landFieldsNumber"
                      placeholder="ex. 2"
                      onChange={this.handleInputNumberChange}
                      value={landFieldsNumber}
                    />
                  </Tooltip>
                </div>)}
              </div>
              <div className="form-group">
                <AntForm.Item
                  label={(
                    <FormattedMessage
                      id="homeDetails.form.otherProperties"
                      defaultMessage="Other properties"
                    />)}
                >
                  <Input.TextArea
                    cols={80}
                    rows={5}
                    name="otherProperties"
                    value={otherProperties}
                    onChange={this.handleInputChange}
                  />
                </AntForm.Item>
              </div>
            </Card>
            <div className="form-group">
              <Button
                block
                type="primary"
                htmlType="submit"
                onClick={this.handleSubmit}
              >
                <FormattedMessage
                  id="beneficiaries.form.submitButton"
                  defaultMessage={formType === 'edit' ? 'Save' : 'Create'}
                  values={{ value: formType === 'edit' ? 'Save' : 'Create' }}
                />
              </Button>
            </div>
          </div>
        </form>
      </div>
    );
  }
}


Form.defaultProps = {
  matchId: '',
  homeDescription: null,
};

Form.propTypes = {
  history: PropTypes.instanceOf(Object).isRequired,
  matchId: PropTypes.string,
  formType: PropTypes.string.isRequired,
  homeDescription: PropTypes.instanceOf(Object),
};
export default withRouter(injectIntl(Form));
