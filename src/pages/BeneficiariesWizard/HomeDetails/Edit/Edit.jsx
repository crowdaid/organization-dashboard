import React from 'react';
import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';
import PropTypes from 'prop-types';
import { Spin } from 'antd';
import Form from '../Form';
import BeneficiariesCollection from '../../../../api/Beneficiaries';

const Edit = (props) => {
  const { matchId, ready, homeDescription } = props;
  if (ready) {
    return <Form matchId={matchId} formType="edit" homeDescription={homeDescription} />;
  }
  return <Spin size="large" className="form__spinner" />;
};
Edit.defaultProps = {
  homeDescription: {},
};
Edit.propTypes = {
  ready: PropTypes.bool.isRequired,
  matchId: PropTypes.string.isRequired,
  homeDescription: PropTypes.instanceOf(Object),
};

// subscribe to beneficiary information
export default withTracker((props) => {
  const { match: { params: { id } } } = props;

  const benHandle = Meteor.subscribe('beneficiaries.single', id);
  const ben = BeneficiariesCollection.findOne({ _id: id });

  if (!benHandle.ready()) {
    return {
      ready: false,
      matchId: '',
      homeDescription: null,
    };
  }
  return {
    ready: true,
    matchId: id,
    homeDescription: ben.homeDescription,
  };
})(Edit);
