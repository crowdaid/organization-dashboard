import FCreate from './Families/Create';
import Families from './Families';
import FEdit from './Families/Edit';
import HomeDetailsCreate from './HomeDetails/Create';
import HomeDetailsEdit from './HomeDetails/Edit';
import HomeDetails from './HomeDetails';

export const CreateFamily = FCreate;
export const EditFamily = FEdit;
export const FamiliesTable = Families;
export const CreateHomeDetails = HomeDetailsCreate;
export const EditHomeDetails = HomeDetailsEdit;
export const HomeDetailsView = HomeDetails;
