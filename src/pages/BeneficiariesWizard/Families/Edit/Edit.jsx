import React from 'react';
import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';
import PropTypes from 'prop-types';
import { Spin } from 'antd';
import FamiliesCollection from '../../../../api/Families';
import Form from '../Form';

const Edit = (props) => {
  const { matchId, ready, family } = props;
  if (ready) {
    return <Form matchId={matchId} formType="edit" family={family} />;
  }
  return <Spin size="large" className="form__spinner" />;
};

Edit.propTypes = {
  ready: PropTypes.bool.isRequired,
  matchId: PropTypes.string.isRequired,
  family: PropTypes.instanceOf(Object).isRequired,
};

// subscribe to beneficiary information
export default withTracker((props) => {
  const { match: { params: { id } } } = props;
  const familyHandle = Meteor.subscribe('families.single', id);
  const family = FamiliesCollection.findOne({ _id: id });
  if (!familyHandle.ready()) {
    return {
      ready: false,
      matchId: '',
      family: null,
    };
  }
  return {
    ready: true,
    matchId: id,
    family,
  };
})(Edit);
