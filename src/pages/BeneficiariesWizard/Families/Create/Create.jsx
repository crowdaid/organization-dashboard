import React from 'react';
import PropTypes from 'prop-types';
import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';
import BeneficiariesCollection from '../../../../api/Beneficiaries';
import Form from '../Form';

const Create = ({ match: { params: { id } }, benName }) => (
  <Form
    benName={benName}
    formType="create"
    matchId={id}
  />
);

Create.defaultProps = {
  benName: '',
};

Create.propTypes = {
  match: PropTypes.instanceOf(Object).isRequired,
  benName: PropTypes.string,
};
// subscribe to beneficiary information
export default withTracker((props) => {
  const { match: { params: { id } } } = props;
  const benHandle = Meteor.subscribe('beneficiaries.single', id);

  if (!benHandle.ready()) {
    return {
      ready: false,
      matchId: '',
      benName: '',
    };
  }
  const ben = BeneficiariesCollection.findOne({ _id: id }, { fields: { name: 1 } });
  return {
    ready: true,
    matchId: id,
    benName: ben.name && ben.name.ar,
  };
})(Create);
