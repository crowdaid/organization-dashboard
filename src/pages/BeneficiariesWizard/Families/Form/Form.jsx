import React, { Component } from 'react';
import {
  Input, Form as AntForm, Button, Select, DatePicker, message, Radio, Tooltip,
} from 'antd';
import { Meteor } from 'meteor/meteor';
import { withRouter } from 'react-router-dom';
import { FormattedMessage, injectIntl, FormattedHTMLMessage } from 'react-intl';
import PropTypes from 'prop-types';
import moment from 'moment';
import { validateNationalNum } from '../../../../components/utils';


const familyRelations = [
  'son', 'daughter', 'mother', 'father',
  'aunt', 'uncle', 'grandFather', 'grandMother',
  'wife', 'husband', 'motherInLaw', 'fatherInLaw',
  'grandSon', 'grandDaughter',
];
class Form extends Component {
  constructor(props) {
    super(props);
    this.state = {
      benName: '',
      name: '',
      gender: '',
      birthDate: new Date(),
      age: '',
      IDError: '',
      relationType: '',
      profession: '',
      socialStatus: '',
      medicalConditions: '',
      notes: '',
      nationalIdNumber: '',
    };
    // set the title of the page
    const { formType } = props;
    document.title = `${formType} family member | CrowdAid`;
    // bind functions to class
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleNationalNumChange = this.handleNationalNumChange.bind(this);
    this.handleDateChange = this.handleDateChange.bind(this);
    this.handleSelectChanges = this.handleSelectChanges.bind(this);
    this.handleGenderChange = this.handleGenderChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount() {
    const { formType, family } = this.props;
    if (formType === 'edit' && family) {
      this.loadFamily(family);
    }
  }

  componentWillReceiveProps(props) {
    const { benName } = props;
    this.setState({ benName });
  }

  // load beneficiary info if edit
  loadFamily(family) {
    this.setState({
      name: family.name,
      gender: family.gender,
      birthDate: family.birthDate,
      age: family.age,
      relationType: family.relationType,
      profession: family.profession,
      socialStatus: family.socialStatus,
      medicalConditions: family.medicalConditions,
      notes: family.notes,
      nationalIdNumber: family.nationalIdNumber,
    });
  }

  // general input changes with name
  handleInputChange(e) {
    const { name } = e.currentTarget;
    const { value } = e.currentTarget;
    this.setState({ [name]: value });
  }

  handleNationalNumChange(e) {
    const nationalIdNumber = e.currentTarget.value;
    if (String(nationalIdNumber).length >= 14) {
      const { formType, matchId } = this.props;
      Meteor.call('families.methods.check_ID_number', {
        _id: formType === 'edit' ? matchId : '',
        nationalIdNumber,
      }, (err, res) => {
        if (err) {
          message.error(`Something went wrong ${err.reason}`);
        } else {
          this.setState({ IDError: res });
        }
      });
    } if (String(nationalIdNumber).length < 15) {
      this.setState({ nationalIdNumber });
    }
  }

  handleDateChange(birthDate) {
    let age = '0';
    if (moment().diff(birthDate, 'years') !== 0) {
      age = `${moment().diff(birthDate, 'years')} year`;
    } else if (moment().diff(birthDate, 'months') !== 0) {
      age = `${moment().diff(birthDate, 'months')} month`;
    } else {
      age = `${moment().diff(birthDate, 'days')} day`;
    }

    this.setState({ birthDate: birthDate.toDate(), age });
  }


  handleSelectChanges(val) {
    this.setState({ relationType: val });
  }

  handleGenderChange(e) {
    this.setState({
      gender: e.target.value,
    });
  }

  handleSubmit(e) {
    e.preventDefault();
    const {
      history, matchId, formType,
    } = this.props;
    const {
      name, age,
      gender, birthDate, relationType, profession,
      socialStatus, medicalConditions, notes, nationalIdNumber,
    } = this.state;
    // check if all the fields is filled
    if (!name || !gender || !birthDate || !relationType || !socialStatus || !nationalIdNumber) {
      message.error('Please fill all the fields below to add the beneficiary information');
      return;
    }
    // validate the national number
    if (!validateNationalNum(nationalIdNumber)) {
      message.error('Please add a valid national number');
      return;
    }
    if (!age) {
      message.error('Please choose right birth date');
      return;
    }
    // configure data object
    const data = {
      name,
      age,
      gender,
      birthDate,
      relationType,
      profession,
      socialStatus,
      medicalConditions,
      notes,
      nationalIdNumber,
      beneficiaryId: matchId,
    };
    if (formType === 'edit') {
    // edit the beneficiary
      Meteor.call('families.methods.update', { _id: matchId, ...data }, (err, benId) => {
        if (err) {
          message.error(`${err.reason}`);
        } else {
          // redirect with message
          history.replace(`/beneficiaries/${benId}/family`);
          message.success('Member updated successfully');
        }
      });
    } else {
      // create the beneficiary
      Meteor.call('families.methods.create', { ...data }, (err, benId) => {
        if (err) {
          message.error(`${err.reason}`);
        } else {
        // redirect with message
          history.replace(`/beneficiaries/${benId}/family`);
          message.success('Member created successfully');
        }
      });
    }
  }

  render() {
    const {
      name, gender, birthDate, profession,
      age, relationType, socialStatus, benName, IDError,
      medicalConditions, notes, nationalIdNumber,
    } = this.state;
    const { history, formType } = this.props;
    return (
      <div className="container">
        <div className="form__with-back">
          {formType === 'edit'
            ? (
              <h1>
                <FormattedMessage
                  id="beneficiariesFamily.form.title.edit"
                  defaultMessage={`Edit family member ${name}`}
                  values={{ name }}
                />
              </h1>
            )
            : (
              <h1>
                <FormattedMessage
                  id="beneficiariesFamily.form.title.new"
                  defaultMessage={`Create new member in ${benName} family`}
                  values={{ name: benName }}
                />
              </h1>
            )
            }
          <Button icon="arrow-left" onClick={() => history.goBack()}>
            <FormattedMessage defaultMessage="Back" id="global.form.backButton" />
          </Button>
        </div>
        <form dir="auto" onSubmit={this.handleSubmit}>
          <div className="flex column">
            <div className="flex row">
              {/* name of the family member  */}
              <div className="form-group ">
                <AntForm.Item
                  label={(
                    <FormattedMessage
                      id="beneficiariesFamily.form.name"
                      defaultMessage="Name of family member"
                    />
                )}
                  required
                >
                  <Input name="name" value={name} onChange={this.handleInputChange} />
                </AntForm.Item>
              </div>
              {/* the relation of the family member */}
              <div className="form-group families__form__select ">
                <AntForm.Item
                  required
                  label={(
                    <FormattedMessage
                      id="beneficiariesFamily.form.relation"
                      defaultMessage="The relation with the beneficiary"
                    />
                    )}
                >
                  <Select
                    defaultValue={familyRelations[0]}
                    name="relationType"
                    value={relationType || '--Choose The Relation--'}
                    onChange={this.handleSelectChanges}
                  >
                    {familyRelations.map(r => (
                      <Select.Option
                        key={r}
                        value={r}
                      >
                        {r}
                      </Select.Option>
                    ))}
                  </Select>
                </AntForm.Item>

              </div>
            </div>
            <div className="flex row">
              {/* member profession */}
              <div className="form-group">
                <AntForm.Item
                  label={(
                    <FormattedMessage
                      id="beneficiariesFamily.form.profession"
                      defaultMessage="Profession"
                    />
                  )}
                >
                  <Input name="profession" value={profession} onChange={this.handleInputChange} />
                </AntForm.Item>
              </div>
              {/* Id number */}
              <div dir="ltr" className="form-group">
                <AntForm.Item
                  className="Family__id__number"
                  label={(
                    <FormattedMessage
                      id="beneficiariesFamily.form.nationalIdNumber"
                      defaultMessage="National ID number"
                    />)}
                  required
                >
                  <Input
                    name="nationalIdNumber"
                    value={nationalIdNumber}
                    onChange={this.handleNationalNumChange}
                  />
                </AntForm.Item>
                {IDError && (
                  <p style={{ color: '#3498db' }}>
                    <FormattedHTMLMessage
                      id="nationalIdNumberError"
                      defaultMessage={`Warring: This ID is currently used in ${IDError.typeExistents}
                        organization ${IDError.orgName ? IDError.orgName : ''}
                         as ${IDError.as} with name of ${IDError.name}`}
                      values={{
                        typeExistents: IDError.typeExistents === 'the same' ? 'thesame' : IDError.typeExistents,
                        orgName: IDError.orgName ? `(${IDError.orgName})` : '',
                        as: IDError.as,
                        name: `"${IDError.name}"`,
                      }}
                    />
                  </p>)}
              </div>
            </div>
            <div className="flex row">
              {/* birth date */}
              <div dir="ltr" className="form-group ">
                <AntForm.Item
                  label={(
                    <FormattedMessage id="beneficiariesFamily.form.birthDate" defaultMessage="Birth Date" />
                  )}
                  required
                >
                  <DatePicker
                    name="birthDate"
                    allowClear={false}
                    disabledDate={date => (date.valueOf() > moment().valueOf())
                      || (date.valueOf() < moment('1900/01/1', 'YYYY/MM/DD'))}
                    value={moment(birthDate)}
                    onChange={this.handleDateChange}
                    format="YYYY/MM/DD"
                  />
                </AntForm.Item>
              </div>
              {/* auto generated age from birth date  */}
              <div className="form-group form__age__number">
                <AntForm.Item
                  label={(
                    <FormattedMessage
                      id="beneficiariesFamily.form.age"
                      defaultMessage="Age"
                    />)}
                >
                  <Tooltip
                    placement="bottom"
                    trigger="focus"
                    title={(
                      <FormattedMessage
                        id="beneficiariesFamily.form.age.tooltip"
                        defaultMessage="Auto generated from birth date"
                      />)}
                  >
                    <Input value={age} />
                  </Tooltip>
                </AntForm.Item>
              </div>
              {/* gender Male or Female */}
              <div className="form-group ">
                <AntForm.Item
                  required
                  label={(
                    <FormattedMessage
                      id="beneficiariesFamily.form.gender"
                      defaultMessage="Gender"
                    />
                  )}
                >
                  <Radio.Group onChange={this.handleGenderChange} value={gender}>
                    <Radio value="Male">
                      <FormattedMessage
                        id="beneficiariesFamily.form.gender.male"
                        defaultMessage="Male"
                      />
                    </Radio>
                    <Radio value="Female">
                      <FormattedMessage
                        id="beneficiariesFamily.form.gender.female"
                        defaultMessage="Female"
                      />
                    </Radio>
                  </Radio.Group>
                </AntForm.Item>
              </div>
              {/* social status  */}
              <div className="form-group">
                <AntForm.Item
                  required
                  label={(
                    <FormattedMessage
                      id="beneficiariesFamily.form.socialStatus"
                      defaultMessage="The member social condition"
                    />
                  )}
                >
                  <Input.TextArea
                    cols={15}
                    name="socialStatus"
                    value={socialStatus}
                    onChange={this.handleInputChange}
                  />
                </AntForm.Item>
              </div>
            </div>
            {/* the member medical conditions */}
            <div className="form-group">
              <AntForm.Item
                label={(
                  <FormattedMessage
                    id="beneficiariesFamily.form.medicalConditions"
                    defaultMessage="His medical conditions"
                  />
                  )}
              >
                <Input.TextArea
                  cols={80}
                  rows={3}
                  name="medicalConditions"
                  value={medicalConditions}
                  onChange={this.handleInputChange}
                />
              </AntForm.Item>
            </div>

            {/* notes  about the member */}
            <div className="form-group">
              <AntForm.Item
                label={(
                  <FormattedMessage
                    id="beneficiariesFamily.form.notes"
                    defaultMessage="Notes"
                  />)}

              >
                <Input.TextArea
                  cols={80}
                  rows={6}
                  name="notes"
                  value={notes}
                  onChange={this.handleInputChange}
                />
              </AntForm.Item>
            </div>
            <div className="form-group">
              <Button
                block
                type="primary"
                htmlType="submit"
                onClick={this.handleSubmit}
              >
                <FormattedMessage
                  id="beneficiaries.form.submitButton"
                  defaultMessage={formType === 'edit' ? 'Save' : 'Create'}
                  values={{ value: formType === 'edit' ? 'Save' : 'Create' }}
                />
              </Button>
            </div>
          </div>
        </form>
      </div>
    );
  }
}


Form.defaultProps = {
  matchId: '',
  family: null,
};

Form.propTypes = {
  history: PropTypes.instanceOf(Object).isRequired,
  matchId: PropTypes.string,
  formType: PropTypes.string.isRequired,
  family: PropTypes.instanceOf(Object),
};
export default withRouter(injectIntl(Form));
