import React, { Component } from 'react';
import { Meteor } from 'meteor/meteor';
import { ReactiveVar } from 'meteor/reactive-var';
import { withTracker } from 'meteor/react-meteor-data';
import PropTypes from 'prop-types';
import {
  Table, Modal, message, Button, Tag,
} from 'antd';
import { withRouter } from 'react-router-dom';
import { FormattedMessage } from 'react-intl';
import { CollectionsMeta } from '../../../api/CollectionsMeta';
import FamiliesCollection from '../../../api/Families';
// table filters and sorting prams
import { searchTables } from '../../../components/tablesFilters';

const PerPage = 20;
const PageNumber = new ReactiveVar(1);
const removeMember = (member) => {
  Modal.confirm({
    icon: 'cross',
    okText: 'Remove',
    okType: 'danger',
    title: `Remove family member: ${member.name}`,
    content: 'This member will be marked as removed.',
    onOk() {
      Meteor.call('families.methods.remove', { _id: member._id }, (err, res) => {
        if (err) {
          message.error(err.reason, 5);
        } else {
          message.success(`${member.name || member.name} removed successfully`);
        }
      });
    },
    onCancel() { Modal.destroyAll(); },
  });
};

class Families extends Component {
  constructor(props) {
    super(props);
    // set the title of the page
    this.state = { searchText: '' };
    // bind search tables with 'this' class
    this.getColumnSearchProps = searchTables.getColumnSearchProps.bind(this);
    this.handleSearch = searchTables.handleSearch.bind(this);
    this.handleReset = searchTables.handleReset.bind(this);
  }

  render() {
    const {
      familyMembers, loading, history, beneficiary, viewTable,
    } = this.props;
    const { id: beneficiaryId, fullScreen } = beneficiary;
    if (fullScreen) {
      document.title = 'Family Members | crowdAid';
    }
    const dataSource = familyMembers.map(member => ({
      _id: member._id,
      name: member.name,
      age: member.age,
      gender: member.gender,
      nationalIdNumber: member.nationalIdNumber,
      socialStatus: member.socialStatus,
      relationType: member.relationType,
      key: member._id,
    }));

    const columns = [
      {
        title: <FormattedMessage id="beneficiariesFamily.table.name" defaultMessage="Member Name" />,
        dataIndex: 'name',
        key: 'name',
        ...this.getColumnSearchProps('name', 'names', this),
      },
      {
        title: <FormattedMessage id="beneficiariesFamily.table.age" defaultMessage="Age" />,
        dataIndex: 'age',
        key: 'age',
        ...this.getColumnSearchProps('age', 'age', this),
      },
      {
        title: <FormattedMessage id="beneficiariesFamily.table.gender" defaultMessage="Gender" />,
        dataIndex: 'gender',
        key: 'gender',
      },
      {
        title: <FormattedMessage id="beneficiariesFamily.table.nationalIdNumber" defaultMessage="ID" />,
        dataIndex: 'nationalIdNumber',
        key: 'nationalIdNumber',
        ...this.getColumnSearchProps('nationalIdNumber', 'IDs', this),
      },
      {
        title: <FormattedMessage id="beneficiariesFamily.table.socialStatus" defaultMessage="Social status" />,
        dataIndex: 'socialStatus',
        key: 'socialStatus',
      },
      {
        title: <FormattedMessage id="beneficiariesFamily.table.relationType" defaultMessage="Relations" />,
        dataIndex: 'relationType',
        key: 'relationType',
      },
      {
        title: !viewTable
          ? (
            <FormattedMessage
              id="beneficiariesFamily.table.actions"
              defaultMessage="Actions"
            />
          )
          : ' ',
        key: 'actions',
        render: (text, record) => {
          if (viewTable) return '';
          if (record.isRemoved) {
            return (<Tag color="blue"> Removed </Tag>);
          }
          return (
            <span>
              <Button
                className="button"
                type="primary"
                size={fullScreen ? 'default' : 'small'}
                shape="circle-outline"
                icon="edit"
                title="Edit member"
                onClick={() => history.push(`/family/${record._id}/edit`)}
              />
              <Button
                className="button"
                size={fullScreen ? 'default' : 'small'}
                type="danger"
                title="Remove member"
                shape="circle-outline"
                icon="delete"
                onClick={() => removeMember(record)}
              />
            </span>
          );
        },
      },
    ];
    return (
      <div className="family">
        <div className="family__table ">
          <div className="family__header">
            {fullScreen
              ? (
                <h1>
                  <FormattedMessage id="beneficiariesFamily.table.title" defaultMessage="Family members" />
                </h1>)
              : (
                <h4>
                  <FormattedMessage id="beneficiariesFamily.table.title" defaultMessage="Family members" />
                </h4>)}
            {!viewTable
            && (
            <div className="view__actions">
              {fullScreen
                ? (
                  <Button icon="arrow-left" onClick={() => history.goBack()}>
                    <FormattedMessage defaultMessage="Back" id="global.form.backButton" />
                  </Button>
                )
                : (
                  <Button
                    type="dashed"
                    size={fullScreen ? 'default' : 'small'}
                    icon="arrows-alt"
                    onClick={() => history.push(`/beneficiaries/${beneficiaryId}/family`)}
                  >
                    <FormattedMessage
                      id="beneficiariesFamily.buttons.seeTable"
                      defaultMessage="Expand Table"
                    />
                  </Button>)}
              <Button
                type="primary"
                size={fullScreen ? 'default' : 'small'}
                icon="plus"
                onClick={() => history.push(`/beneficiaries/${beneficiaryId}/family/create-family`)}
              >
                <FormattedMessage
                  id="beneficiariesFamily.buttons.add"
                  defaultMessage="Add New Member"
                />
              </Button>
            </div>)}
          </div>
          <Table
            pagination={false}
            loading={loading}
            dataSource={dataSource}
            size={fullScreen ? 'default' : 'small'}
            columns={columns}
          />
        </div>
      </div>);
  }
}

Families.propTypes = {
  loading: PropTypes.bool.isRequired,
  familyMembers: PropTypes.array.isRequired,
  history: PropTypes.object.isRequired,
};

export default withTracker((props) => {
  const { benId, match } = props;
  const id = match && match.params.id;
  const beneficiary = benId ? { id: benId, fullScreen: false } : { id, fullScreen: true };
  const familyHandle = Meteor.subscribe('familiesTabular', {
    filter: {},
    limit: PerPage,
    beneficiaryId: beneficiary.id,
    skip: (PageNumber.get() - 1) * PerPage,
  });
  if (!familyHandle.ready()) {
    return ({
      loading: true,
      familyMembers: [],
      pagination: {},
      beneficiary,
    });
  }

  const membersMeta = CollectionsMeta.findOne({ _id: 'Families' });

  const familyMembers = FamiliesCollection.find({
    beneficiaryId: beneficiary.id,
    isRemoved: { $ne: true },
    relationType: { $ne: 'self' },
  }, {
    sort: { createdAt: -1 },
  });

  const totalMembers = membersMeta && membersMeta.count;

  const membersExist = totalMembers > 0;

  return {
    loading: false,
    familyMembers: membersExist ? familyMembers.fetch() : [],
    total: totalMembers,
    beneficiary,
  };
})(withRouter(Families));
