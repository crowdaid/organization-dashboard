import React, { Component } from 'react';
import {
  Input, Form as AntForm, Button, Select, DatePicker, message, Radio, Avatar,
} from 'antd';
import { Meteor } from 'meteor/meteor';
import { withRouter } from 'react-router-dom';
import { FormattedMessage } from 'react-intl';
import PropTypes from 'prop-types';
import moment from 'moment';
import Files from '../../../api/Files';
import { validateNationalNum, validateMobileNum } from '../../../components/utils';

// verification Statuses types
const verificationStatuses = [
  { label: 'Pending', value: 'pending' },
  { label: 'Verified', value: 'verified' },
  { label: 'Rejected', value: 'rejected' },
  { label: 'More info needed', value: 'moreInfoNeeded' },
];
// verification Statuses types
const areaTypes = [
  {
    label: <FormattedMessage
      id="beneficiaries.form.town"
      defaultMessage="Town"
    />,
    value: 'town',
  },
  {
    label: <FormattedMessage
      id="beneficiaries.form.countryside"
      defaultMessage="Countryside"
    />,
    value: 'countryside',
  },
];

class Form extends Component {
  constructor(props) {
    super(props);
    this.state = {
      gender: '',
      status: '',
      birthDate: new Date(),
      address: '',
      about: '',
      nameAr: '',
      nameEn: '',
      nationalIdNumber: '',
      areaType: '',
      mobile: '',
      homePhone: '',
      photo: '',
      previewPhoto: '',
    };
    // set the title of the page
    const { formType } = props;
    document.title = `Beneficiaries-${formType} | CrowdAid`;
    // bind functions to class
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleNationalNumChange = this.handleNationalNumChange.bind(this);
    this.handleDateChange = this.handleDateChange.bind(this);
    this.handlePhotoChange = this.handlePhotoChange.bind(this);
    this.handleSelectChanges = this.handleSelectChanges.bind(this);
    this.handleStatusChanges = this.handleStatusChanges.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount() {
    const { formType, ben } = this.props;
    if (formType === 'edit' && ben) {
      this.loadBeneficiary(ben);
    }
  }

  // load beneficiary info if edit
  loadBeneficiary(ben) {
    document.title = `Beneficiaries-${ben.name.ar} | CrowdAid`;
    this.setState({
      gender: ben.gender,
      status: ben.verificationStatus,
      birthDate: ben.birthDate,
      address: ben.address,
      about: ben.about,
      nameAr: ben.name.ar,
      nameEn: ben.name.en,
      nationalIdNumber: ben.nationalIdNumber,
      areaType: ben.areaType,
      mobile: ben.mobile,
      homePhone: ben.homePhone,
      photo: ben && ben.photo,
      previewPhoto: '',
    });
  }

  handleNationalNumChange(e) {
    const num = e.currentTarget.value;
    if (String(num).length < 15) {
      this.setState({ nationalIdNumber: num });
    }
  }

  handleDateChange(birthDate) {
    this.setState({ birthDate: birthDate.toDate() });
  }


  handleSelectChanges(e) {
    const { name, value } = e.target;
    this.setState({ [name]: value });
  }

  handleStatusChanges(value) {
    this.setState({ status: value });
  }

  // general input changes with name
  handleInputChange(e) {
    const { name } = e.currentTarget;
    const { value } = e.currentTarget;
    this.setState({ [name]: value });
  }

  readURL(file) {
    const reader = new FileReader();
    reader.onload = (e) => {
      this.setState({ previewPhoto: e.target.result });
    };
    reader.readAsDataURL(file);
  }

  handlePhotoChange(e) {
    const file = e.currentTarget.files[0];
    this.readURL(file);
    this.setState({ photo: file });
  }

  uploadPhoto(benId) {
    const { photo } = this.state;
    if (typeof photo === 'string' || !photo) return;

    Files.insert({
      file: photo,
      meta: {
        namespace: 'beneficiaries',
        slug: benId,
        type: 'photo',
      },
      onUploaded: (err, f) => {
        if (!err) {
          Meteor.call('beneficiaries.methods.update_photo', { benId, fileId: f._id });
        }
      },
      onError: (err, f) => {
        message.error(`Error on uploading ${f.name}, ${err.reason}`);
      },
    });
  }


  handleSubmit(e) {
    e.preventDefault();
    const {
      history, matchId, formType,
    } = this.props;
    const {
      gender, status, birthDate, nationalIdNumber, nameAr, nameEn, about,
      areaType, homePhone, mobile, photo,
    } = this.state;
    // check if all the fields is filled
    if (!nameAr || !nameEn
      || !birthDate || !about
       || !nationalIdNumber
      || !areaType || !gender
    ) {
      message.error('Please fill all the fields below to add the beneficiary information');
      return;
    }
    // validate the national number
    if (!validateNationalNum(nationalIdNumber)) {
      message.error('Please add a valid national number');
      return;
    }
    if (!validateMobileNum(mobile)) {
      message.error('Please add a valid mobile number');
      return;
    }
    if (!photo) {
      message.error('Please upload Beneficiary image');
      return;
    }
    // configure data object
    const data = {
      name: { ar: nameAr, en: nameEn },
      gender,
      nationalIdNumber,
      about,
      birthDate,
      verificationStatus: status.value || status,
      areaType,
      homePhone,
      mobile,
    };
    if (formType === 'edit') {
    // edit the beneficiary
      Meteor.call('beneficiaries.methods.update', { _id: matchId, ...data }, (err, res) => {
        if (err) {
          message.error(`Something went wrong ${err.reason}`);
        } else {
          this.uploadPhoto(matchId);
          // redirect with message
          history.push('/beneficiaries');
          message.success('beneficiary updated successfully');
        }
      });
    } else {
      // create the beneficiary
      Meteor.call('beneficiaries.methods.create', { ...data }, (err, id) => {
        if (err) {
          message.error(`Something went wrong ${err.reason}`);
        } else {
          this.uploadPhoto(id);
          // redirect with message
          history.push(`/beneficiaries/${id}/create-home-description`);
          message.success('beneficiary created successfully');
        }
      });
    }
  }

  render() {
    const {
      gender, status, nationalIdNumber,
      address, nameAr, nameEn, about, birthDate,
      areaType, homePhone, mobile, previewPhoto, photo,
    } = this.state;
    const {
      history, formType, matchId, ben,
    } = this.props;

    return (
      <div className="container">
        <div className="form__with-back">
          {formType === 'edit'
            ? (
              <h1>
                <FormattedMessage
                  id="beneficiaries.form.title.edit"
                  defaultMessage={`Edit ${nameEn} (${nameAr})`}
                  values={{ nameAr, nameEn }}
                />
              </h1>
            )
            : (
              <h1>
                <FormattedMessage
                  id="beneficiaries.form.title.new"
                  defaultMessage="Create new beneficiary"
                />
              </h1>
            )
            }
          <div className="view__actions">
            <Button icon="arrow-left" onClick={() => history.goBack()}>
              <FormattedMessage defaultMessage="Back" id="global.form.backButton" />
            </Button>
            {(formType === 'edit' && ben.homeDescription) ? (
              <Button
                type="dashed"
                onClick={() => history.push(`/beneficiaries/${matchId}/edit-home-description`)}
              >
                <FormattedMessage
                  defaultMessage="Edit home description"
                  id="beneficiaries.form.editHome"
                  values={{ value: 'edit' }}
                />
              </Button>) : formType === 'edit' && (
                <Button
                  type="dashed"
                  onClick={() => history.push(`/beneficiaries/${matchId}/create-home-description`)}
                >
                  <FormattedMessage
                    defaultMessage="Add home description"
                    id="beneficiaries.form.editHome"
                    values={{ value: 'add' }}
                  />
                </Button>)}
          </div>

        </div>
        <form dir="auto" onSubmit={this.handleSubmit}>
          <div className="flex column">
            <div className="flex row">
              <div>

                <div className="flex row">
                  <div className="form-group fill">
                    <AntForm.Item
                      label={(
                        <FormattedMessage id="beneficiaries.form.nameAr" defaultMessage="Name in Arabic" />
                )}
                      required
                    >
                      <Input name="nameAr" value={nameAr} onChange={this.handleInputChange} />
                    </AntForm.Item>
                  </div>
                  <div className="form-group fill">
                    <AntForm.Item
                      label={(
                        <FormattedMessage id="beneficiaries.form.nameEn" defaultMessage="Name in English" />
                    )}
                      required
                    >
                      <Input name="nameEn" value={nameEn} onChange={this.handleInputChange} />
                    </AntForm.Item>
                  </div>
                </div>
              </div>
              <div className="ben__image">
                <Avatar shape="square" src={previewPhoto || photo} icon="user" alt="beneficiary logo" size={135} />
                <div className="profile__upload-btn-wrapper">
                  <Button style={{ width: '19.5rem' }}>
                    <FormattedMessage id="beneficiaries.form.benImage" defaultMessage="Beneficiary Image" />
                  </Button>
                  <input type="file" name="logo" size="20" onChange={this.handlePhotoChange} />
                </div>
              </div>
            </div>
            <div className="flex row">
              <div className="form-group form__select">
                <AntForm.Item
                  required
                  label={(
                    <FormattedMessage id="beneficiaries.form.gender" defaultMessage="Gender" />
                    )}
                >
                  <Radio.Group name="gender" onChange={this.handleSelectChanges} value={gender}>
                    <Radio value="Male">
                      <FormattedMessage
                        id="beneficiaries.form.gender.male"
                        defaultMessage="Male"
                      />
                    </Radio>
                    <Radio value="Female">
                      <FormattedMessage
                        id="beneficiaries.form.gender.female"
                        defaultMessage="Female"
                      />
                    </Radio>
                  </Radio.Group>
                </AntForm.Item>
              </div>
              <div className="form-group ">
                <AntForm.Item
                  required
                  label={(
                    <FormattedMessage id="beneficiaries.form.areaType" defaultMessage="Area Type" />
                    )}
                >
                  <Radio.Group
                    name="areaType"
                    options={areaTypes}
                    onChange={this.handleSelectChanges}
                    value={areaType}
                  />
                </AntForm.Item>

              </div>
              <div className="form-group form__select">
                <AntForm.Item
                  label={(
                    <FormattedMessage id="beneficiaries.form.status" defaultMessage="Status" />
                  )}
                >
                  <Select
                    defaultValue={verificationStatuses[0].value}
                    name="status"
                    value={status || verificationStatuses[0].value}
                    onChange={this.handleStatusChanges}
                  >
                    {verificationStatuses.map(t => (
                      <Select.Option
                        key={t.value}
                        value={t.value}
                      >
                        {t.label}
                      </Select.Option>
                    ))}
                  </Select>
                </AntForm.Item>
              </div>
              <div dir="ltr" className="form-group">
                <AntForm.Item
                  label={(
                    <FormattedMessage id="beneficiaries.form.birthDate" defaultMessage="Birth Date" />
                  )}
                  required
                >
                  <DatePicker
                    name="birthDate"
                    allowClear={false}
                    disabledDate={date => (date.valueOf() > moment().valueOf())
                      || (date.valueOf() < moment('1900/01/1', 'YYYY/MM/DD'))}
                    value={moment(birthDate)}
                    onChange={this.handleDateChange}
                    format="YYYY/MM/DD"
                  />
                </AntForm.Item>
              </div>
            </div>
            <div className="flex column">
              <div className="flex row">
                <div dir="ltr" className="form-group">
                  <AntForm.Item
                    label={(
                      <FormattedMessage
                        id="beneficiaries.form.nationalIdNumber"
                        defaultMessage="National ID number"
                      />)}
                    required
                  >
                    <Input
                      name="nationalIdNumber"
                      value={nationalIdNumber}
                      onChange={this.handleNationalNumChange}
                    />
                  </AntForm.Item>
                </div>
                <div className="form-group">
                  <AntForm.Item
                    label={(
                      <FormattedMessage
                        id="beneficiaries.form.mobile"
                        defaultMessage="mobile number"
                      />)}
                  >
                    <Input
                      name="mobile"
                      value={mobile}
                      onChange={this.handleInputChange}
                    />
                  </AntForm.Item>
                </div>
                <div className="form-group">
                  <AntForm.Item
                    label={(
                      <FormattedMessage
                        id="beneficiaries.form.homePhone"
                        defaultMessage="Home number"
                      />)}
                  >
                    <Input
                      name="homePhone"
                      value={homePhone}
                      onChange={this.handleInputChange}
                    />
                  </AntForm.Item>
                </div>
              </div>
              <div className="form-group">
                <AntForm.Item
                  label={(
                    <FormattedMessage
                      id="beneficiaries.form.address"
                      defaultMessage="Address (From the case location)"
                    />)}
                >
                  <Input disabled name="address" value={address} onChange={this.handleInputChange} />
                </AntForm.Item>
              </div>
              <div className="form-group">
                <AntForm.Item
                  label={(
                    <FormattedMessage
                      id="beneficiaries.form.about"
                      defaultMessage="About"
                    />)}
                  required
                >
                  <Input.TextArea
                    rows={4}
                    cols={100}
                    name="about"
                    value={about}
                    onChange={this.handleInputChange}
                  />
                </AntForm.Item>
              </div>
            </div>
            <div className="form-group">
              <Button
                block
                type="primary"
                htmlType="submit"
                onClick={this.handleSubmit}
              >
                <FormattedMessage
                  id="beneficiaries.form.submitButton"
                  defaultMessage={formType === 'edit' ? 'Save' : 'Create'}
                  values={{ value: formType === 'edit' ? 'Save' : 'Create' }}
                />
              </Button>
            </div>
          </div>
        </form>
      </div>
    );
  }
}


Form.defaultProps = {
  matchId: '',
  ben: null,
};

Form.propTypes = {
  history: PropTypes.instanceOf(Object).isRequired,
  matchId: PropTypes.string,
  formType: PropTypes.string.isRequired,
  ben: PropTypes.instanceOf(Object),
};
export default withRouter(Form);
