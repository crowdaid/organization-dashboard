import React, { Component } from 'react';
import { Button, Spin } from 'antd';
import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import { FamiliesTable, HomeDetailsView } from '../../BeneficiariesWizard';
import BeneficiariesCollection from '../../../api/Beneficiaries';
import BenAvatar from '../../../components/BenAvarar/BenAvatar';

class View extends Component {
  constructor(props) {
    super(props);
    this.state = {
      type: '',
      status: '',
      birthDate: '',
      address: '',
      about: '',
      nameAr: '',
      nameEn: '',
      nationalIdNumber: '',
    };
    // set the title of the page
    document.title = 'Beneficiaries-View | CrowdAid';
  }

  componentWillReceiveProps({ ready, ben }) {
    if (ready && ben) {
      this.loadBeneficiary(ben);
    }
  }

  loadBeneficiary(ben) {
    document.title = `Beneficiaries-${ben.name.ar} | CrowdAid`;
    this.setState({
      id: ben._id,
      type: ben.gender,
      areaType: ben.areaType,
      homePhone: ben.homePhone,
      mobile: ben.mobile,
      homeDescription: ben.homeDescription,
      status: ben.verificationStatus,
      birthDate: new Date(ben.birthDate).toLocaleDateString(),
      address: ben.address,
      about: ben.about,
      nameAr: ben.name.ar,
      nameEn: ben.name.en,
      nationalIdNumber: ben.nationalIdNumber,
    });
  }

  render() {
    const {
      type, status, nationalIdNumber, address,
      nameAr, nameEn, about, birthDate,
      id, areaType, homePhone, mobile, homeDescription,
    } = this.state;
    const { ready, history } = this.props;
    // show spinner until the subscription is ready
    if (!ready) {
      return (
        <div className="container">
          <Spin size="large" className="form__spinner" />
        </div>
      );
    }
    return (
      <div className="container">
        <div className="view__with-back">
          <h1>{`${nameAr}`}</h1>
          <Button icon="arrow-left" onClick={() => history.goBack()}>
            <FormattedMessage defaultMessage="Back" id="global.form.backButton" />
          </Button>
        </div>
        <hr />
        <div dir="auto">
          <table className="view__table">
            <tbody className="view__table__body">
              <tr className="view__table__body__row">
                <td>
                  <b>
                    <FormattedMessage defaultMessage="beneficiary photo" id="beneficiaries.view.photo" />
                  </b>
                </td>
                <td><BenAvatar benId={id} size={130} /></td>
              </tr>
              <tr className="view__table__body__row">
                <td>
                  <b>
                    <FormattedMessage defaultMessage="Name in Arabic" id="beneficiaries.view.nameAr" />
                  </b>
                </td>
                <td>{nameAr}</td>
              </tr>
              <tr>
                <td>
                  <b>
                    <FormattedMessage defaultMessage="Name in English" id="beneficiaries.view.nameEn" />
                  </b>
                </td>
                <td>{nameEn}</td>
              </tr>
              <tr>
                <td>
                  <b>
                    <FormattedMessage defaultMessage="Birth Date" id="beneficiaries.view.birthDate" />
                  </b>
                </td>
                <td>{birthDate}</td>
              </tr>
              <tr>
                <td>
                  <b>
                    <FormattedMessage defaultMessage="areaType" id="beneficiaries.form.areaType" />
                  </b>
                </td>
                <td>{areaType}</td>
              </tr>
              <tr>
                <td>
                  <b>
                    <FormattedMessage defaultMessage="homePhone" id="beneficiaries.view.homePhone" />
                  </b>
                </td>
                <td>
                  {homePhone || (
                  <FormattedMessage
                    defaultMessage="No data"
                    id="beneficiaries.view.notFound"
                  />
                  )}
                </td>
              </tr>
              <tr>
                <td>
                  <b>
                    <FormattedMessage defaultMessage="mobile" id="beneficiaries.view.mobile" />
                  </b>
                </td>
                <td>
                  {mobile || (
                  <FormattedMessage
                    defaultMessage="No data"
                    id="beneficiaries.view.notFound"
                  />
                  )}
                </td>
              </tr>
              <tr>
                <td>
                  <b>
                    <FormattedMessage defaultMessage="Address" id="beneficiaries.view.address" />
                  </b>
                </td>
                <td>{address}</td>
              </tr>
              <tr>
                <td>
                  <b>
                    <FormattedMessage defaultMessage="Type" id="beneficiaries.view.type" />
                  </b>
                </td>
                <td>{type}</td>
              </tr>
              <tr>
                <td>
                  <b>
                    <FormattedMessage defaultMessage="Status" id="beneficiaries.view.status" />
                  </b>
                </td>
                <td>{status}</td>
              </tr>
              <tr>
                <td>
                  <b>
                    <FormattedMessage
                      defaultMessage="National ID number"
                      id="beneficiaries.view.nationalIdNumber"
                    />
                  </b>
                </td>
                <td>{nationalIdNumber}</td>
              </tr>
              <tr>
                <td>
                  <b>
                    <FormattedMessage defaultMessage="About" id="beneficiaries.view.about" />
                  </b>
                </td>
                <td>{about}</td>
              </tr>
            </tbody>
          </table>
          {/* family table */}
          <div className="view__family__table">
            <FamiliesTable benId={id} viewTable />
          </div>
          <div className="view__family__table">
            <HomeDetailsView
              className="view__family__table"
              homeDescription={homeDescription}
            />
          </div>
        </div>
      </div>
    );
  }
}

View.defaultProps = {
  ben: null,
};

View.propTypes = {
  ready: PropTypes.bool.isRequired,
  history: PropTypes.instanceOf(Object).isRequired,
  ben: PropTypes.instanceOf(Object),
};


export default withTracker((props) => {
  const { match: { params: { id } } } = props;

  const benHandle = Meteor.subscribe('beneficiaries.single', id);
  const ben = BeneficiariesCollection.findOne({ _id: id });

  if (!benHandle.ready()) {
    return {
      ready: false,
      ben: null,
    };
  }
  return {
    ready: true,
    ben,
  };
})(View);
