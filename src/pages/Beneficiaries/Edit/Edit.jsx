import React from 'react';
import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';
import PropTypes from 'prop-types';
import { Spin } from 'antd';
import Form from '../Form';
import Files from '../../../api/Files';
import BeneficiariesCollection from '../../../api/Beneficiaries';

const Edit = (props) => {
  const { matchId, ready, ben } = props;
  if (ready) {
    return <Form matchId={matchId} formType="edit" ben={ben} />;
  }
  return <Spin size="large" className="form__spinner" />;
};

Edit.propTypes = {
  ready: PropTypes.bool.isRequired,
  matchId: PropTypes.string.isRequired,
};

// subscribe to beneficiary information
export default withTracker((props) => {
  const { match: { params: { id } } } = props;

  const benHandle = Meteor.subscribe('beneficiaries.single', id);
  const photoHandle = Meteor.subscribe('files.beneficiaries.image', id);
  if (!benHandle.ready() || !photoHandle.ready()) {
    return {
      ready: false,
      matchId: '',
      ben: null,
    };
  }
  const ben = BeneficiariesCollection.findOne({ _id: id });

  const photo = Files.findOne({ _id: ben.photo, 'meta.type': 'photo' });
  ben.photo = (photo && photo.link()) || '';

  return {
    ready: true,
    matchId: id,
    ben,
  };
})(Edit);
