import React, { Component } from 'react';
import { Meteor } from 'meteor/meteor';
import { ReactiveVar } from 'meteor/reactive-var';
import { withTracker } from 'meteor/react-meteor-data';
import PropTypes from 'prop-types';
import {
  Table, Modal, message, Button, Tag, Icon, Tooltip,
} from 'antd';
import { FormattedMessage } from 'react-intl';
import BenAvatar from '../../components/BenAvarar';
import { CollectionsMeta } from '../../api/CollectionsMeta';
import BeneficiariesCollection from '../../api/Beneficiaries';
// table filters and sorting prams
import { beneficiariesFilters, searchTables } from '../../components/tablesFilters';
// family members for nested tables
import { FamiliesTable } from '../BeneficiariesWizard';

const PerPage = 10;
const PageNumber = new ReactiveVar(1);

const removeBen = (ben) => {
  Modal.confirm({
    icon: 'cross',
    okText: 'Remove',
    okType: 'danger',
    title: `Remove Beneficiary: ${ben.name_ar || ben.name_en} ?`,
    content: `This beneficiary will be marked as 
       removed if this person not at any case.

       Note: also all his family members will removed (if any).
       `,
    onOk() {
      Meteor.call('beneficiaries.methods.remove', { _id: ben.id }, (err, res) => {
        if (err) {
          message.error(err.reason, 5);
        } else {
          message.success(`${ben.name_en || ben.name_ar} removed successfully`);
        }
      });
    },
    onCancel() { Modal.destroyAll(); },
  });
};

// eslint-disable-next-line react/prop-types
const CustomExpandIcon = ({ expanded, record, onExpand }) => {
  let text;
  if (expanded) {
    text = 'Show family';
  } else {
    text = 'Hide family';
  }
  return (
    <Tooltip title={(
      <FormattedMessage
        id="expaned.icon.tooltip"
        defaultMessage={text}
        values={{ expanded, name: record.name_ar }}
      />
    )}
    >
      <Button
        type="ghost"
        icon={expanded ? 'shrink' : 'arrows-alt'}
        size="small"
        style={{ border: 'none', fontSize: 10 }}
        className="expand-row-icon"
        onClick={e => onExpand(record, e)}
      >
        <FormattedMessage
          id="expaned.icon"
          defaultMessage={text}
          values={{ expanded, name: record.name_ar }}
        />
      </Button>
    </Tooltip>
  );
};

class Beneficiaries extends Component {
  constructor(props) {
    super(props);
    // set the title of the page
    document.title = 'Beneficiaries | CrowdAid ';
    this.state = { searchText: '' };
    // bind search tables with 'this' class
    this.getColumnSearchProps = searchTables.getColumnSearchProps.bind(this);
    this.handleSearch = searchTables.handleSearch.bind(this);
    this.handleReset = searchTables.handleReset.bind(this);
  }

  render() {
    const {
      bens, pagination, loading, history,
    } = this.props;

    const dataSource = bens.map(ben => ({
      id: ben._id,
      name_ar: ben.name.ar,
      name_en: ben.name.en,
      gender: ben.gender || '-',
      areaType: ben.areaType || '-',
      key: ben._id,
      status: ben.verificationStatus,
    }));

    const columns = [
      {
        title: <FormattedMessage id="beneficiaries.view.photo" defaultMessage="beneficiary photo" />,
        key: 'photo',
        render: (text, record) => <BenAvatar benId={record.id} size={70} />,
      },
      {
        title: <FormattedMessage id="beneficiaries.table.name_ar" defaultMessage="Arabic Name" />,
        dataIndex: 'name_ar',
        key: 'name_ar',
        ...this.getColumnSearchProps('name_ar', 'arabic name', this),
      },
      {
        title: <FormattedMessage id="beneficiaries.table.name_en" defaultMessage="English Name" />,
        dataIndex: 'name_en',
        key: 'name_en',
        ...this.getColumnSearchProps('name_en', 'english name', this),
      },
      {
        title: <FormattedMessage id="beneficiaries.form.gender" defaultMessage="Gender" />,
        dataIndex: 'gender',
        key: 'gender',
      },
      {
        title: <FormattedMessage id="beneficiaries.table.status" defaultMessage="Status" />,
        dataIndex: 'status',
        key: 'status',
        filters: beneficiariesFilters.benStatus,
        onFilter: (value, record) => (record.status === value),
      },
      {
        title: <FormattedMessage id="beneficiaries.table.actions" defaultMessage="Actions" />,
        key: 'actions',
        render: (text, record) => {
          if (record.isRemoved) {
            return (<Tag color="blue"> Removed </Tag>);
          }
          return (
            <span>
              <Button
                className="button"
                type="default"
                shape="circle-outline"
                icon="eye"
                title="View beneficiary"
                onClick={() => history.push(`/beneficiaries/${record.id}`)}
              />
              <Button
                className="button"
                type="primary"
                shape="circle-outline"
                icon="edit"
                title="Edit beneficiary"
                onClick={() => history.push(`/beneficiaries/${record.id}/edit`)}
              />
              <Button
                className="button"
                type="danger"
                title="Remove beneficiary"
                shape="circle-outline"
                icon="delete"
                onClick={() => removeBen(record)}
              />
            </span>
          );
        },
      },
    ];

    return (
      <div className="bens">
        <div className="bens__table ">
          <div className="bens__header">
            <h1>
              <Icon type="heart" />
              {'  '}
              <FormattedMessage id="beneficiaries.title" defaultMessage="Beneficiaries" />
            </h1>
            <Button type="primary" icon="plus" onClick={() => history.push('/beneficiaries/create')}>
              <FormattedMessage id="beneficiaries.buttons.add" defaultMessage="Add New Beneficiary" />
            </Button>
          </div>
          <Table
            pagination={pagination}
            loading={loading}
            expandIcon={CustomExpandIcon}
            dataSource={dataSource}
            expandedRowRender={record => <FamiliesTable benId={record.id} />}
            columns={columns}
          />
        </div>
      </div>);
  }
}

Beneficiaries.propTypes = {
  loading: PropTypes.bool.isRequired,
  bens: PropTypes.array.isRequired,
  pagination: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
};

export default withTracker((props) => {
  const benHandle = Meteor.subscribe('BeneficiariesTabular', {
    filter: {},
    limit: PerPage,
    skip: (PageNumber.get() - 1) * PerPage,
  });
  if (!benHandle.ready()) {
    return ({
      loading: true,
      bens: [],
      pagination: {},
    });
  }
  const benMeta = CollectionsMeta.findOne({ _id: 'Beneficiaries' });

  const bens = BeneficiariesCollection.find({}, {
    sort: { createdAt: -1 },
  });

  const totalBens = benMeta && benMeta.count;

  const bensExist = totalBens > 0;

  return {
    loading: false,
    bens: bensExist ? bens.fetch() : [],
    total: totalBens,
    pagination: {
      position: 'bottom',
      total: totalBens,
      defaultCurrent: 1,
      pageSize: PerPage,
      onChange(page, perPage) {
        PageNumber.set(page);
      },
    },
  };
})(Beneficiaries);
