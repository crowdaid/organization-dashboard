import React, { Component } from 'react';
import {
  Tabs, Icon, Input, message,
} from 'antd';
import { Meteor } from 'meteor/meteor';
import { FormattedMessage } from 'react-intl';
import Offers from './Offers';

class renderOffersTabs extends Component {
  constructor(props) {
    super(props);
    this.state = { searchValues: [] };
    this.handleSearch = this.handleSearch.bind(this);
    this.renderSearchInput = this.renderSearchInput.bind(this);
  }

  handleSearch(searchItemText) {
    if (searchItemText) {
      setTimeout(() => {
        Meteor.call('categories.methods.search', { keyword: searchItemText.trim() }, (err, res) => {
          const searchValues = res && res.map(item => item._id);
          this.setState({ searchValues: searchValues || [] });
          if (searchValues.length === 0) {
            message.info('Sorry no offers found with this item name', 7);
          }
        });
      }, 500);
    } else {
      this.setState({ searchValues: [] });
      message.error('Enter item name first');
    }
  }

  renderSearchInput() {
    return (
      <div dir="auto" style={{ margin: '1rem 0' }}>
        <p>
          <FormattedMessage id="offers.searchItems" defaultMessage="Search offers with item name :" />
        </p>
        <div dir="ltr" style={{ width: 300 }}>
          <Input.Search
            enterButton
            allowClear
            onChange={({ target: { value } }) => !value && this.setState({ searchValues: [] })}
            placeholder="Item name here..."
            onSearch={value => this.handleSearch(value)}
            style={{ width: 300 }}
          />
        </div>
      </div>
    );
  }

  render() {
    const { searchValues } = this.state;
    return (
      <div className="dons" id="donations">
        <div className="dons__table ">
          <div className="dons__header">
            <h1>
              <Icon type="shop" />
              {' '}
              <FormattedMessage id="offers.title" defaultMessage="Offers" />
            </h1>
          </div>
          {this.renderSearchInput()}
          <Tabs defaultActiveKey="1">
            <Tabs.TabPane
              tab={(
                <span>
                  <Icon type="gift" />
                  <FormattedMessage id="offers.orgOffers" defaultMessage="Organization offers" />
                </span>
            )}
              key="1"
            >
              <Offers isOrgOffer searchValues={searchValues} />
            </Tabs.TabPane>
            <Tabs.TabPane
              tab={(
                <span>
                  <Icon type="global" />
                  <FormattedMessage id="offers.globalOffers" defaultMessage="Global offers" />
                </span>
          )}
              key="2"
            >
              <Offers isOrgOffer={false} searchValues={searchValues} />
            </Tabs.TabPane>
          </Tabs>
        </div>
      </div>
    );
  }
}


export default renderOffersTabs;
