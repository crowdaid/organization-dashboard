import React, { Component } from 'react';
import { Meteor } from 'meteor/meteor';
import { withRouter } from 'react-router-dom';
import { ReactiveVar } from 'meteor/reactive-var';
import { withTracker } from 'meteor/react-meteor-data';
import { FormattedMessage } from 'react-intl';
import PropTypes from 'prop-types';
import {
  Table, Button, Tag, message,
} from 'antd';
import { CollectionsMeta } from '../../api/CollectionsMeta';
import DonationsCollection from '../../api/Donations';
import ItemsCat from '../../api/Categories';
// table filters and sorting prams
import { searchTables } from '../../components/tablesFilters';


const GetItem = withTracker(({ itemId }) => {
  const itemHandle = Meteor.subscribe('categories.single', itemId);
  if (!itemHandle.ready()) {
    return {
      item: {},
    };
  }
  const item = ItemsCat.findOne({ _id: itemId });
  return {
    item,
  };
})(({ item }) => (<b style={{ color: '#2ecc71' }}>{item.name}</b>));


const PerPage = 10;
const PageNumber = new ReactiveVar(1);

// mark the donation as collected
class Offers extends Component {
  constructor(props) {
    super(props);
    // set the title of the page
    document.title = 'Offers | CrowdAid ';
    this.state = { searchText: '' };
    // bind search tables with 'this' class
    this.getColumnSearchProps = searchTables.getColumnSearchProps.bind(this);
    this.handleSearch = searchTables.handleSearch.bind(this);
    this.handleReset = searchTables.handleReset.bind(this);
    this.acceptOffer = this.acceptOffer.bind(this);
  }

  acceptOffer(donation) {
    const { history } = this.props;
    Meteor.call('donations.methods.accept_offer', { donation }, (err, res) => {
      if (err) {
        message.info(err.reason);
      } else {
        message.success('Donation collected successfully');
        history.push('/donations');
      }
    });
  }

  render() {
    const {
      dons, pagination, loading,
    } = this.props;
    const dataSource = dons.map(don => ({
      _id: don._id,
      quantity: don.quantity,
      status: don.status,
      createdAt: don.createdAt,
      isAnonymous: don.isAnonymous,
      itemId: don.itemId,
      userId: don.userId || 'فاعل خير',
      isAutomated: don.isAutomated,
      key: don._id,
    }));

    const columns = [
      {
        title: <FormattedMessage id="donations.table.user" defaultMessage="User" />,
        dataIndex: 'userId',
        key: 'userId',
        ...this.getColumnSearchProps('userId', 'users', this),
      },
      {
        title: <FormattedMessage id="donations.table.quantity" defaultMessage="Quantity" />,
        dataIndex: 'quantity',
        key: 'quantity',
        sorter: (a, b) => a.quantity - b.quantity,
        sortDirections: ['descend', 'ascend'],
      },
      {
        title: <FormattedMessage id="offers.table.item" defaultMessage="Item" />,
        dataIndex: 'itemId',
        key: 'item',
        render: (text, record) => (<GetItem itemId={record.itemId} />),
      },
      {
        title: <FormattedMessage id="offers.table.actions" defaultMessage="Actions" />,
        key: 'actions',
        render: (text, record) => {
          if (record.isRemoved) {
            return (<Tag color="blue"> Removed </Tag>);
          }
          return (
            <span>
              {record.status === 'Offer' && (
              <Button
                className="button"
                type="default"
                style={{ backgroundColor: '#786fa6', color: '#fff' }}
                shape="circle-outline"
                title="Mark as collected"
                icon="shopping-cart"
                onClick={this.acceptOffer}
              />
              )}
            </span>
          );
        },
      },
    ];

    return (
      <Table
        pagination={pagination}
        loading={loading}
        dataSource={dataSource}
        columns={columns}
      />
    );
  }
}

Offers.propTypes = {
  loading: PropTypes.bool.isRequired,
  dons: PropTypes.array.isRequired,
  pagination: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
};

export default withTracker((props) => {
  const { isOrgOffer, searchValues } = props;
  const userHandle = Meteor.subscribe('loggedUser');
  const donsHandle = Meteor.subscribe('DonationsTabular', {
    filter: { status: 'Offer' },
    limit: PerPage,
    skip: (PageNumber.get() - 1) * PerPage,
    setOrgId: false,
  });
  if (!donsHandle.ready() && !userHandle.ready()) {
    return ({
      loading: true,
      dons: [],
      pagination: {},
    });
  }
  const { organizationId } = Meteor.user();

  const qry = {};
  if (isOrgOffer) {
    qry.orgId = organizationId;
  } else {
    qry.orgId = null;
  }
  if (searchValues && searchValues.length !== 0) {
    qry.itemId = { $in: searchValues };
  }

  const donsMeta = CollectionsMeta.findOne({ _id: 'Donations' });

  const dons = DonationsCollection.find({ ...qry, status: 'Offer' }, {
    sort: { createdAt: -1 },
  });
  const totalDons = donsMeta && donsMeta.count;

  const donsExist = totalDons > 0;

  return {
    loading: false,
    dons: donsExist ? dons.fetch() : [],
    total: totalDons,
    pagination: {
      position: 'bottom',
      total: totalDons,
      defaultCurrent: 1,
      pageSize: PerPage,
      onChange(page, perPage) {
        PageNumber.set(page);
      },
    },
  };
})(withRouter(Offers));
