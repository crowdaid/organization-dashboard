import React, { Component } from 'react';
import {
  Input, Form as AntForm, Button, message,
} from 'antd';
import { Meteor } from 'meteor/meteor';
import { withRouter } from 'react-router-dom';
import { FormattedMessage } from 'react-intl';
import PropTypes from 'prop-types';
import { validateNationalNum, validateMobileNum } from '../../../components/utils';

class Form extends Component {
  constructor(props) {
    super(props);
    this.state = {
      firstName: '', lastName: '', mobile: '', idNumber: '', email: '',
    };
    // bind functions to class
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleNationalNumChange = this.handleNationalNumChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }


  componentDidMount() {
    const { user: { emails, profile } } = this.props;
    if (profile) {
      // set the title of the page
      const {
        firstName, lastName, mobile, idNumber,
      } = profile;
      document.title = `${firstName} | CrowdAid`;
      this.setState({
        firstName,
        lastName,
        mobile,
        idNumber,
      });
    } else {
      document.title = `${emails[0].address} | CrowdAid`;
    }

    this.setState({ email: emails[0].address });
  }

  // general input changes with name
  handleInputChange(e) {
    const { name } = e.currentTarget;
    const { value } = e.currentTarget;
    this.setState({ [name]: value });
  }

  handleNationalNumChange(e) {
    const idNumber = e.currentTarget.value;
    if (String(idNumber).length < 15) {
      this.setState({ idNumber });
    }
  }

  handleSubmit(e) {
    e.preventDefault();
    const {
      history, user,
    } = this.props;
    const {
      firstName, lastName, mobile, idNumber,
    } = this.state;
    // check if all the fields is filled
    if (!firstName || !lastName || !mobile || !idNumber) {
      message.error('Please fill all the fields below to edit information');
      return;
    }
    // validate the National ID number
    if (!validateNationalNum(idNumber)) {
      message.error('Please enter a valid ID number');
      return;
    }
    if (mobile && !validateMobileNum(mobile)) {
      message.error('Please enter a valid mobile number');
      return;
    }

    // edit the beneficiary
    Meteor.call('users.methods.update_profile', {
      userId: user._id, firstName, lastName, mobile, idNumber,
    }, (err, res) => {
      if (err) {
        message.error(err.reason);
      } else {
        // redirect with message
        history.push('/staff');
        message.success('Employee updated successfully');
      }
    });
  }

  render() {
    const {
      firstName, lastName, mobile, idNumber, email,
    } = this.state;
    const { history } = this.props;
    // show spinner until the subscription is ready
    return (
      <div className="container">
        <div className="form__with-back">
          { !firstName || !lastName ? <h1>{`${email}`}</h1> : <h1>{`${firstName} ${lastName}`}</h1> }
          <Button icon="arrow-left" onClick={() => history.replace('/staff')}>
            <FormattedMessage defaultMessage="Back" id="global.form.backButton" />
          </Button>
        </div>
        <form dir="auto" onSubmit={this.handleSubmit}>
          <div className="flex column">
            <div className="flex row">
              <div className="form-group fill">
                <AntForm.Item
                  label={(
                    <FormattedMessage
                      id="staff.form.firstName"
                      defaultMessage="First Name"
                    />)}
                  required
                >
                  <Input name="firstName" value={firstName} onChange={this.handleInputChange} />
                </AntForm.Item>
              </div>
              <div className="form-group fill">
                <AntForm.Item
                  label={(
                    <FormattedMessage
                      id="staff.form.lastName"
                      defaultMessage="Last Name"
                    />)}
                  required
                >
                  <Input name="lastName" value={lastName} onChange={this.handleInputChange} />
                </AntForm.Item>
              </div>
            </div>
            <div className="flex column">
              <div className="form-group">
                <AntForm.Item
                  label={(
                    <FormattedMessage
                      id="staff.form.idNumber"
                      defaultMessage="ID Number"
                    />)}
                  required
                >
                  <Input
                    name="idNumber"
                    value={idNumber}
                    onChange={this.handleNationalNumChange}
                  />
                </AntForm.Item>
              </div>
              <div className="form-group">
                <AntForm.Item
                  label={(
                    <FormattedMessage
                      id="staff.form.mobile"
                      defaultMessage="Mobile number"
                    />)}
                  required
                >
                  <Input name="mobile" value={mobile} onChange={this.handleInputChange} />
                </AntForm.Item>
              </div>
            </div>
            <div className="form-group">
              <Button block type="primary" htmlType="submit" onClick={this.handleSubmit}>
                <FormattedMessage
                  id="staff.form.saveButton"
                  defaultMessage="Save"
                />
              </Button>
            </div>
          </div>
        </form>
      </div>
    );
  }
}

Form.propTypes = {
  history: PropTypes.instanceOf(Object).isRequired,
  user: PropTypes.instanceOf(Object).isRequired,
};
export default withRouter(Form);
