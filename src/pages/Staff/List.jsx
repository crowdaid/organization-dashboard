import React, { Component } from 'react';
import { Meteor } from 'meteor/meteor';
import { ReactiveVar } from 'meteor/reactive-var';
import { withTracker } from 'meteor/react-meteor-data';
import PropTypes from 'prop-types';
import {
  Table, Modal, message, Spin, Button, Input, Icon,
} from 'antd';
import { FormattedMessage } from 'react-intl';
import { CollectionsMeta } from '../../api/CollectionsMeta';
import { Employees } from '../../api/Employees';
import { validateEmail, isOwner } from '../../components/utils';
// table filters and sorting prams
import { searchTables } from '../../components/tablesFilters';
import UserAvatar from '../../components/UserAvatar';

const PerPage = 5;
const PageNumber = new ReactiveVar(1);

const removeEmployee = (user) => {
  Modal.confirm({
    icon: 'cross',
    okText: 'Remove',
    okType: 'danger',
    title: `Remove ${user.profile.firstName || user.emails[0].address} 
    from the organization?`,
    content: 'This user will be removed permanently, and no longer have access to this organization',
    onOk() {
      Meteor.call('users.methods.remove', {
        userId: user._id,
        role: 'employee',
      }, (err, res) => {
        if (err) {
          message.error('something went wrong');
        } else {
          message.success('Employee removed successfully');
        }
      });
    },
    onCancel() {

    },
  });
};

class StaffList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      sending: false,
      searchText: '',
    };
    // create reference to the invite email input
    this.inviteEmailRef = React.createRef();
    this.inviteEmployee = this.inviteEmployee.bind(this);
    // set the title of the page
    document.title = 'Staff | CrowdAid';
    // bind search tables with 'this' class
    this.getColumnSearchProps = searchTables.getColumnSearchProps.bind(this);
    this.handleSearch = searchTables.handleSearch.bind(this);
    this.handleReset = searchTables.handleReset.bind(this);
  }


  inviteEmployee(e) {
    e.preventDefault();
    this.inviteEmailRef.current.focus();
    const inviteEmail = this.inviteEmailRef.current.input.value;
    // validate email
    if (!validateEmail(inviteEmail)) {
      message.warning('The Email must be valid, please check it!');
      return;
    }

    this.setState({ sending: true });
    Meteor.call('users.methods.send_invitation', { email: inviteEmail, role: 'employee' }, (err, res) => {
      this.setState({ sending: false });
      if (err) message.error('Some thing went wrong, check the email address and try again!');
      else message.info(res);
    });
  }


  render() {
    const {
      employees, pagination, loading, history, owner,
    } = this.props;

    const { sending } = this.state;
    const dataSource = employees.map((user) => {
      if (!user.profile) user.profile = [];
      return {
        ...user,
        key: user._id,
        first_name: user.profile.firstName,
        last_name: user.profile.lastName,
        mobile: user.profile.mobile,
        email: user.emails[0].address,
        identification: user.profile.idNumber,
      };
    });

    const columns = [
      {
        title: <FormattedMessage defaultMessage="Avatar" id="safe.table.avtar" />,
        key: 'avatar',
        render: (t, record) => (
          <UserAvatar userId={record._id} />
        ),
      },
      {
        title: <FormattedMessage defaultMessage="First Name" id="safe.table.firstName" />,
        dataIndex: 'first_name',
        key: 'first_name',
      },
      {
        title: <FormattedMessage defaultMessage="Last Name" id="safe.table.lastName" />,
        dataIndex: 'last_name',
        key: 'last_name',
      },
      {
        title: <FormattedMessage defaultMessage="Mobile" id="safe.table.mobile" />,
        dataIndex: 'mobile',
        key: 'mobile',
      },
      {
        title: <FormattedMessage defaultMessage="ID" id="safe.table.idNumber" />,
        dataIndex: 'identification',
        key: 'identification',
      },
      {
        title: <FormattedMessage defaultMessage="Email Address" id="safe.table.email" />,
        dataIndex: 'email',
        key: 'email',
        ...this.getColumnSearchProps('email', 'emails', this),
      },
      {
        title: <FormattedMessage defaultMessage="Actions" id="safe.table.actions" />,
        key: 'actions',
        render: (text, record) => (
          <span>
            <Button
              className="staff__button"
              type="primary"
              shape="circle-outline"
              icon="edit"
              title="Edit employee"
              onClick={() => history.replace(`staff/${record.key}/edit`)}
            />
            <Button
              className="staff__button"
              type="danger"
              shape="circle-outline"
              title="Delete employee"
              icon="delete"
              onClick={() => removeEmployee(record)}
            />
          </span>
        ),
      },
    ];

    return (
      <div className="staff">
        <form className="staff__form" onSubmit={this.inviteEmployee}>
          {
          !sending
            ? (
              owner
              && (
              <Input
                prefix={<Icon type="mail" style={{ color: 'rgba(0,0,0,.25)' }} />}
                title="Enter the employee email address, To send him invitation"
                ref={this.inviteEmailRef}
                type="email"
                suffix={(
                  <Button type="ghost" style={{ borderRight: 'none' }} htmlType="submit">
                    <FormattedMessage
                      id="staff.inviteInput.button.text"
                      defaultMessage="Invite New Employee"
                    />
                  </Button>)}
                placeholder="email@example.com"
              />
              )
            )
            : <Spin size="large" />
          }
        </form>
        <div className="staff__table ">
          <Table
            pagination={pagination}
            loading={loading}
            dataSource={dataSource}
            columns={columns}
          />
        </div>
      </div>);
  }
}
StaffList.propTypes = {
  loading: PropTypes.bool.isRequired,
  employees: PropTypes.array.isRequired,
  pagination: PropTypes.object.isRequired,
};

export default withTracker(({ history }) => {
  const employeesHandle = Meteor.subscribe('StaffTabular', {
    filter: { _id: { $ne: Meteor.userId() } },
    limit: PerPage,
    skip: (PageNumber.get() - 1) * PerPage,
  });
  if (!employeesHandle.ready()) {
    return ({
      loading: true,
      employees: [],
      pagination: {},
      owner: false,
    });
  }
  const employeesMeta = CollectionsMeta.findOne({ _id: 'Employees' });

  const employees = Employees.find({}, {
    sort: { createdAt: -1 },
  });

  const totalEmployees = employeesMeta && employeesMeta.count;

  const EmployeesExist = totalEmployees > 0;
  const { isReady, orgOwner } = isOwner(Meteor.userId());
  if (isReady && !orgOwner) {
    history.replace('/404');
  }
  return {
    loading: false,
    employeesReady: employeesHandle.ready(),
    employees: EmployeesExist ? employees.fetch() : [],
    total: totalEmployees,
    pagination: {
      position: 'bottom',
      total: totalEmployees,
      defaultCurrent: 1,
      pageSize: PerPage,
      onChange(page, perPage) {
        PageNumber.set(page);
      },
    },
    owner: orgOwner,
  };
})(StaffList);
