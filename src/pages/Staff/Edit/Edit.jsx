import React from 'react';
import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';
import PropTypes from 'prop-types';
import { Spin } from 'antd';
import { isOwner } from '../../../components/utils';
import Form from '../Form';

const Edit = (props) => {
  const { user, ready } = props;
  if (ready) return <Form user={user} formType="edit" />;
  return <Spin size="large" className="form__spinner" />;
};

Edit.propTypes = {
  ready: PropTypes.bool.isRequired,
  user: PropTypes.instanceOf(Object).isRequired,
};

export default withTracker((props) => {
  const { match: { params: { id } }, history } = props;

  const userHandle = Meteor.subscribe('users.single', id);
  const user = Meteor.users.findOne({ _id: id });
  if (!userHandle.ready()) {
    return {
      ready: false,
      user: {},
    };
  }
  const { isReady, orgOwner } = isOwner(Meteor.userId());
  if (isReady && !orgOwner) {
    history.replace('/404');
  }
  return {
    ready: true,
    user,
  };
})(Edit);
