/* eslint-disable import/no-extraneous-dependencies */
import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { Meteor } from 'meteor/meteor';
import { FormattedMessage, injectIntl, intlShape } from 'react-intl';
import { validateEmail, loginAs } from '../../components/utils';

class Login extends Component {
  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.state = { error: '' };
    // eslint-disable-next-line react/prop-types
    const { history } = this.props;
    // set the title of the page
    document.title = 'Login | CrowdAid';
    // redirect if the user already logged in
    if (Meteor.userId()) {
      history.replace('/');
    }
  }

  // handle login request
  handleSubmit(e) {
    e.preventDefault();
    const email = e.target.user.value.trim();
    const password = e.target.password.value;

    // validate email
    if (!email || !validateEmail(email)) {
      this.setState({ error: 'The Email must be vaild, please check it!' });
      return;
    }
    // eslint-disable-next-line react/prop-types
    const { history } = this.props;
    // login the user if he is an admin
    loginAs(email, password, ['owner', 'employee'], (error) => {
      if (!error) {
        history.replace('/');
      } else {
        this.setState({ error: error.reason });
      }
    });
  }

  render() {
    const { error } = this.state;
    const { intl } = this.props;
    const emailPlaceHolder = intl
      .formatMessage({ id: 'login.emailPlaceholder', defaultMessage: 'Enter your email' });
    const passPlaceHolder = intl
      .formatMessage({ id: 'login.passPlaceHolder', defaultMessage: 'Enter your password' });
    return (
      <div dir="auto" className="register">
        <div className="register__wrapper">
          <div className="register__title">
            <FormattedMessage id="login.title" defaultMessage="Login" />
          </div>
          {error && <p className="register__error">{error}</p>}
          <form className="register__form" onSubmit={this.handleSubmit}>
            <input required type="email" name="user" placeholder={emailPlaceHolder} />
            <input required type="password" name="password" placeholder={passPlaceHolder} />
            <button type="submit" className="register__btn">
              <FormattedMessage id="login.submitButton" defaultMessage="Login" />
            </button>
            <p className="register__info">
              <FormattedMessage
                id="login.note"
                defaultMessage="If you having a problem with your account please contact the administrator"
              />
            </p>
          </form>
        </div>
      </div>
    );
  }
}
Login.propTypes = {
  intl: intlShape.isRequired,
};
export default withRouter(injectIntl(Login));
