import React from 'react';
import {
  Card, Row, Col, Icon, Timeline,
} from 'antd';
import Statistic from 'antd/lib/statistic';
import { Pie, Doughnut, Polar } from 'react-chartjs-2';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import CasesCalendar from '../../components/CasesCalendar';

const Dashboard = ({
  totalstats,
  totalstats: {
    cases, needs, bens, donations,
  }, ready,
}) => (
  <div className="dashboard">
    <h1>
      <Icon type="dashboard" />
      {' '}
      <FormattedMessage id="dashboard.title" defaultMessage="Dashboard" />
    </h1>
    <div className="dashboard__totals">
      { ready && Object.keys(totalstats).map(stat => (
        <div key={totalstats[stat].total.lable} className="dashboard__totals__card">
          <Statistic
            title={(
              <div>
                <Icon type="area-chart" />
                {'  '}
                <FormattedMessage
                  id={`dashboard.totals.${totalstats[stat].total.lable}`}
                  defaultMessage={totalstats[stat].total.lable}
                />
              </div>
            )}
            valueStyle={{ fontWeight: 'bold', fontSize: 30 }}
            value={totalstats[stat].total.value}
          />
        </div>
      ))}
    </div>
    <Row gutter={8} type="flex">
      <Col span={8} className="dashboard__col">
        <Card
          loading={!ready}
          title={(
            <div className="dashboard__card-title">
              <img src="/image/cases.png" width="35" height="35" alt="crowd aid" />
              <b>
                <Link to="/cases">
                  <FormattedMessage
                    id="dashboard.cards.cases.lable"
                    defaultMessage="Cases"
                  />
                </Link>
              </b>
            </div>
          )}
          className="dashboard__card"
        >
          <div className="dashboard__card__info">
            <div className="dashboard__card__info__number">
              {cases && (
              <Pie
                height={200}
                options={{ title: { display: true, text: 'Urgency', position: 'top' } }}
                data={cases.urgencyData}
                legend={{ position: 'left', fullWidth: false }}
              />
              )}
              {cases && (
              <Doughnut
                height={200}
                legend={{ position: 'left', fullWidth: false }}
                options={{ title: { display: true, text: 'Status', position: 'top' } }}
                data={cases.statusData}
              />
              )}
            </div>
          </div>
        </Card>
      </Col>
      <Col span={8} className="dashboard__col">
        <Card
          loading={!ready}
          title={(
            <div className="dashboard__card-title">
              <img src="/image/needs.png" width="25" height="25" alt="crowd aid" />
              <b>
                <FormattedMessage
                  id="dashboard.cards.needs.lable"
                  defaultMessage="Needs"
                />
              </b>
            </div>
          )}
          className="dashboard__card"
        >
          <div className="dashboard__card__info">
            <div className="dashboard__card__info__number">
              {needs && (
              <Pie
                height={200}
                options={{ title: { display: true, text: 'Urgency', position: 'top' } }}
                data={needs.urgencyData}
                legend={{ position: 'left', fullWidth: false }}
              />
              )}
              {needs && (
              <Doughnut
                height={200}
                legend={{ position: 'left', fullWidth: false }}
                options={{ title: { display: true, text: 'Status', position: 'top' } }}
                data={needs.statusData}
              />
              )}
            </div>
          </div>
        </Card>
      </Col>
      <Col span={8} className="dashboard__col">
        <Card
          loading={!ready}
          className="dashboard__card"
          title={(
            <div className="dashboard__card-title">
              <img src="/image/hand.png" width="25" height="25" alt="crowd aid" />
              <b>
                <Link to="/beneficiaries">
                  <FormattedMessage
                    id="dashboard.cards.beneficiaries.lable"
                    defaultMessage="Beneficiaries"
                  />
                </Link>
              </b>
            </div>
          )}
        >
          <div className="dashboard__card__info">
            <div className="dashboard__card__info__number">
              {bens && (
              <Pie
                height={200}
                options={{ title: { display: true, text: 'Type', position: 'top' } }}
                data={bens.typesData}
                legend={{ position: 'left', fullWidth: false }}
              />
              )}
              {bens && (
              <Doughnut
                height={200}
                legend={{ position: 'left', fullWidth: false }}
                options={{ title: { display: true, text: 'Status', position: 'top' } }}
                data={bens.statusData}
              />
              )}
            </div>
          </div>
        </Card>
      </Col>
    </Row>
    <Row gutter={8} type="flex">
      <Col span={16} className="dashboard__col">
        <Card
          loading={!ready}
          className="dashboard__card"
          title={(
            <div className="dashboard__card-title">
              <img src="/image/donations.png" width="30" height="30" alt="crowd aid" />
              <b>
                <Link to="/donations">
                  <FormattedMessage
                    id="dashboard.cards.donations.lable"
                    defaultMessage="Donations"
                  />
                </Link>
              </b>
            </div>
          )}
        >
          <div className="dashboard__card__info">
            <div className="flex row">
              <div>
                {donations && (
                <Pie
                  height={200}
                  options={{ title: { display: true, text: 'Type', position: 'top' } }}
                  data={donations.typesData}
                  legend={{ position: 'left', fullWidth: false }}
                />
                )}
                {donations && (
                <Polar
                  height={200}
                  options={{ title: { display: true, text: 'Pay Methods ', position: 'top' } }}
                  data={donations.methodsData}
                  legend={{ position: 'left', fullWidth: false }}
                />
                )}
              </div>
              <div className="flex column">
                {donations && (
                <Doughnut
                  height={200}
                  legend={{ position: 'left', fullWidth: false }}
                  options={{ title: { display: true, text: 'Status', position: 'top' } }}
                  data={donations.statusData}
                />
                )}
                <div className="dashboard__last-donations">
                  <h3 style={{ padding: '1rem 0' }}>
                    <FormattedMessage
                      id="dashboard.cards.donations.last"
                      defaultMessage="Last 10 donations"
                    />
                  </h3>
                  {donations
                  && (
                  <Timeline>
                    {donations.lastDonations.map(don => (
                      <Link key={don._id} to={`/donations/${don._id}`}>
                        <Timeline.Item style={{ fontSize: 13, color: '#5F5F5F' }}>
                          <b>Donation type: </b>
                          {don.type}
                          <b>, donation method: </b>
                          {don.donationMethod}
                          <b>, quantity: </b>
                          {don.quantity}
                          <b>, money value of: </b>
                          {don.moneyValue}
                        </Timeline.Item>
                      </Link>
                    ))}
                  </Timeline>
                  )
                  }
                </div>
              </div>
            </div>
          </div>
        </Card>
      </Col>
      <div className="flex dashboard__card calender">
        <CasesCalendar />
      </div>
    </Row>
  </div>
);


Dashboard.propTypes = {
  ready: PropTypes.bool.isRequired,
  totalstats: PropTypes.instanceOf(Object).isRequired,
};

export default Dashboard;
