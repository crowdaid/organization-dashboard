import './Dashboard.css';
import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';
import Dashboard from './Dashboard';
import {
  generateCasesData, generateDonationsData,
  generateNeedsData, generateBeneficiariesData,
} from './stats';

export default withTracker((props) => {
  document.title = 'Dashboard | CrowdAid';
  const orgStatsHandle = Meteor.subscribe('org.dashboard.stats');
  const donsHandle = Meteor.subscribe('DonationsTabular', {
    filter: {},
    limit: 0,
  });
  Meteor.subscribe('loggedUser');
  if (!orgStatsHandle.ready() || !Meteor.user() || !donsHandle) {
    return {
      ready: false,
      totalstats: {},
    };
  }
  const organizationId = (Meteor.user() && Meteor.user().organizationId) || '';
  // cases statistics
  const cases = generateCasesData(organizationId);
  const needs = generateNeedsData(organizationId);
  const bens = generateBeneficiariesData(organizationId);
  const donations = generateDonationsData(organizationId);
  const staff = {
    total: {
      lable: 'Total Staff',
      value: Meteor.users.find({
        organizationId,
      }).count(),
    },
  };
  const totalstats = {
    cases,
    needs,
    bens,
    staff,
    donations,
  };
  return {
    ready: true,
    totalstats,
  };
})(Dashboard);
