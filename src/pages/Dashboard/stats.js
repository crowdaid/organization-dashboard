import Cases from '../../api/Cases';
import Needs from '../../api/Needs';
import Beneficiaries from '../../api/Beneficiaries';
import Donations from '../../api/Donations';

const needStatuses = ['untouched', 'touched', 'fulfilled'];
const caseStatuses = ['draft', 'approved', 'pending', 'rejected', 'fulfilled', 'terminated'];
const urgencyFlags = ['immediate', 'today', 'thisWeek', 'thisMonth', 'thisYear', 'N/A'];
const verificationStatuses = ['pending', 'verified', 'moreInfoNeeded', 'rejected'];
const beneficiaryTypes = ['individual', 'animal', 'group', 'family', 'organization', 'place'];
const donationMethods = ['Cash on door', 'Credit card', 'NoMoney'];
const donationStatuses = ['Pending', 'Collected', 'Failed', 'Cancelled'];
const donationType = ['money', 'items', 'noquantity'];

// calculate cases statistics
export const generateCasesData = organizationId => ({
  total: {
    lable: 'Total Cases',
    value: Cases.find({ organizationId }).count(),
  },
  urgencyData: {
    datasets: [{
      data: urgencyFlags.map(urgency => Cases.find({ organizationId, urgency }).count()),
      backgroundColor: [
        '#4b6584', '#2ecc71', '#3498db', '#9b59b6', '#e74c3c', '#f1c40f',
      ],
    }],
    labels: urgencyFlags,
  },
  statusData: {
    datasets: [{
      data: caseStatuses.map(status => Cases.find({ organizationId, status }).count()),
      backgroundColor: [
        '#4b6584', '#2ecc71', '#3498db', '#9b59b6', '#e74c3c', '#f1c40f',
      ],
    }],
    labels: caseStatuses,
  },
});


// calculate needs statistics
export const generateNeedsData = organizationId => ({
  total: {
    lable: 'Total Needs',
    value: Needs.find({ organizationId }).count(),
  },
  urgencyData: {
    datasets: [{
      data: urgencyFlags.map(urgency => Needs.find({ organizationId, urgency }).count()),
      backgroundColor: [
        '#4b6584', '#2ecc71', '#3498db', '#9b59b6', '#e74c3c', '#f1c40f',
      ],
    }],
    labels: urgencyFlags,
  },
  statusData: {
    datasets: [{
      data: needStatuses.map(status => Needs.find({ organizationId, status }).count()),
      backgroundColor: [
        '#4b6584', '#2ecc71', '#3498db',
      ],
    }],
    labels: needStatuses,
  },
});

// calculate Beneficiaries statistics
export const generateBeneficiariesData = organizationId => ({
  total: {
    lable: 'Total Beneficiaries',
    value: Beneficiaries.find({ orgId: organizationId }).count(),
  },
  typesData: {
    datasets: [{
      data: beneficiaryTypes
        .map(type => Beneficiaries
          .find({ orgId: organizationId, type }).count()),
      backgroundColor: [
        '#4b6584', '#2ecc71', '#3498db', '#9b59b6', '#e74c3c', '#f1c40f',
      ],
    }],
    labels: beneficiaryTypes,
  },
  statusData: {
    datasets: [{
      data: verificationStatuses
        .map(verificationStatus => Beneficiaries
          .find({ orgId: organizationId, verificationStatus }).count()),
      backgroundColor: [
        '#4b6584', '#2ecc71', '#3498db', '#e74c3c',
      ],
    }],
    labels: verificationStatuses,
  },
});

// calculate Donations statistics
export const generateDonationsData = orgId => ({
  total: {
    lable: 'Total Donations',
    value: Donations.find({ orgId }).count(),
  },
  methodsData: {
    datasets: [{
      data: donationMethods
        .map(donationMethod => Donations
          .find({ orgId, donationMethod }).count()),
      backgroundColor: [
        '#4b6584', '#2ecc71', '#3498db',
      ],
    }],
    labels: donationMethods,
  },
  statusData: {
    datasets: [{
      data: donationStatuses.map(status => Donations.find({ orgId, status }).count()),
      backgroundColor: [
        '#4b6584', '#2ecc71', '#3498db', '#9b59b6',
      ],
    }],
    labels: donationStatuses,
  },
  typesData: {
    datasets: [{
      data: donationType.map(type => Donations.find({ orgId, type }).count()),
      backgroundColor: [
        '#4b6584', '#2ecc71', '#3498db',
      ],
    }],
    labels: donationType,
  },
  lastDonations: Donations.find({ orgId, status: 'Pending' }, {
    sort: { createdAt: -1 },
    limit: 10,
  }).fetch(),
});
