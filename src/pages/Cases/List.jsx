import React, { Component } from 'react';
import { Meteor } from 'meteor/meteor';
import { ReactiveVar } from 'meteor/reactive-var';
import { withTracker } from 'meteor/react-meteor-data';
import PropTypes from 'prop-types';
import {
  Table, Modal, message, Button, Icon, Drawer, Tooltip,
} from 'antd';
import { FormattedMessage } from 'react-intl';
import { CollectionsMeta } from '../../api/CollectionsMeta';
import CasesCollection from '../../api/Cases';
import CasesNotes from '../CasesNotes';
// table filters and sorting prams
import { casesFilters, searchTables } from '../../components/tablesFilters';

const PerPage = 10;
const PageNumber = new ReactiveVar(1);

const removeCase = (Case) => {
  Modal.confirm({
    icon: 'cross',
    okText: 'Remove',
    okType: 'danger',
    title: `Remove Case: ${Case.title} ?`,
    content: `
      This case will be marked as removed.
      Please note that all the donations 
      and needs related to this case will be removed as well 
    `,
    onOk() {
      Meteor.call('cases.methods.remove', { caseId: Case.id }, (err, res) => {
        if (err) {
          message.error('something went wrong');
        } else {
          message.success(`${Case.title} removed successfully`);
        }
      });
    },
    onCancel() { Modal.destroyAll(); },
  });
};
class CasesList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchText: '',
      caseNotesVisible: false,
      caseNoteId: '',
    };
    // set the title of the page
    document.title = 'Cases | CrowdAid';
    // bind search tables with 'this' class
    this.getColumnSearchProps = searchTables.getColumnSearchProps.bind(this);
    this.handleSearch = searchTables.handleSearch.bind(this);
    this.handleReset = searchTables.handleReset.bind(this);
    this.onCloseDrawer = this.onCloseDrawer.bind(this);
  }

  onCloseDrawer() {
    this.setState({
      caseNotesVisible: false,
    });
  }

  render() {
    const {
      cases, pagination, loading, history,
    } = this.props;

    const dataSource = cases.map(c => ({
      id: c._id,
      key: c._id,
      title: c.title,
      orgId: c.organizationId,
      status: c.status,
      urgency: c.urgency,
      caseCode: c.caseCode || '-',
      dueDate: (new Date(c.dueDate)).toDateString(),
    }));

    const columns = [
      {
        title: <FormattedMessage id="cases.table.caseCode" defaultMessage="Case Number" />,
        dataIndex: 'caseCode',
        key: 'caseCode',
        ...this.getColumnSearchProps('caseCode', 'cases', this),
      },
      {
        title: <FormattedMessage id="cases.table.caseTitle" defaultMessage="Title" />,
        dataIndex: 'title',
        key: 'title',
        ...this.getColumnSearchProps('title', 'cases', this),
      },
      {
        title: <FormattedMessage id="cases.table.status" defaultMessage="Status" />,
        dataIndex: 'status',
        key: 'status',
        filters: casesFilters.status,
        onFilter: (value, record) => (record.status === value),
      },
      {
        title: <FormattedMessage id="cases.table.urgency" defaultMessage="Urgency" />,
        dataIndex: 'urgency',
        key: 'urgency',
        filters: casesFilters.urgency,
        onFilter: (value, record) => (record.urgency === value),
      },
      {
        title: <FormattedMessage id="cases.table.dueDate" defaultMessage="Due Date" />,
        dataIndex: 'dueDate',
        key: 'dueDate',
      },
      {
        title: <FormattedMessage id="cases.table.actions" defaultMessage="Actions" />,
        key: 'actions',
        width: 300,
        render: (text, record) => (
          <span className="cases_actions">
            <div>
              <Button
                className="button"
                type="default"
                shape="circle-outline"
                icon="eye"
                title="View case"
                onClick={() => history.push(`/cases/${record.id}`)}
              />
              <Button
                className="button"
                type="primary"
                shape="circle-outline"
                icon="edit"
                title="Edit case"
                onClick={() => history.push(`/cases/${record.id}/edit`)}
              />
              <Tooltip title="Donate to this case needs">
                <Button
                  className="button"
                  type="default"
                  style={{ backgroundColor: '#20bf6b', color: '#fff' }}
                  shape="circle-outline"
                  icon="dollar"
                  onClick={() => history.push(`/donate/${record.id}`)}
                />
              </Tooltip>
              <Button
                className="button"
                type="danger"
                shape="circle-outline"
                icon="delete"
                title="Delete case"
                onClick={() => removeCase(record)}
              />
            </div>

            <div style={{ marginTop: '1rem' }}>
              <Button
                className="button"
                type="dashed"
                onClick={() => history.push(`/cases/${record.id}/needs`)}
                title="Edit case needs"
              >
                <FormattedMessage id="needs.title" defaultMessage="Needs" />
              </Button>
              {/* cases notes open button */}
              <Button
                className="button"
                type="default"
                title="View case staff notes"
                icon="form"
                onClick={() => this.setState({
                  caseNoteId: record.id,
                  caseNotesVisible: true,
                })}
              >
                <FormattedMessage
                  defaultMessage="Staff Notes"
                  id="cases.table.casesNotes"
                />
              </Button>
            </div>
          </span>
        ),
      },
    ];
    const {
      caseNotesVisible, caseNoteId,
    } = this.state;

    return (
      <div className="cases">
        <div className="cases__table">
          <div className="cases__header">
            <h1>
              <Icon type="solution" />
              {'  '}
              <FormattedMessage id="cases.title" defaultMessage="Cases" />
            </h1>
            <Button
              type="primary"
              icon="plus"
              onClick={() => history.push('/cases/create')}
            >
              <FormattedMessage id="cases.buttons.add" defaultMessage="Add New Case" />
            </Button>
          </div>
          <Table
            pagination={pagination}
            loading={loading}
            dataSource={dataSource}
            columns={columns}
          />
          {/* case staff Notes */}
          <Drawer
            title={(
              <FormattedMessage id="cases.staffnotes.title" defaultMessage="Case staff notes" />
            )}
            placement="left"
            closable
            destroyOnClose
            width={400}
            onClose={this.onCloseDrawer}
            visible={caseNotesVisible}
          >
            <CasesNotes caseId={caseNoteId} />
          </Drawer>
        </div>
      </div>);
  }
}

CasesList.propTypes = {
  loading: PropTypes.bool.isRequired,
  cases: PropTypes.array.isRequired,
  pagination: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
};

export default withTracker((props) => {
  const casesHandle = Meteor.subscribe('CasesTabular', {
    filter: {},
    limit: PerPage,
    skip: (PageNumber.get() - 1) * PerPage,
  });
  if (!casesHandle.ready()) {
    return ({
      loading: true,
      cases: [],
      pagination: {},
    });
  }
  const casesMeta = CollectionsMeta.findOne({ _id: 'Cases' });

  const cases = CasesCollection.find({}, {
    sort: { createdAt: -1 },
  });

  const totalCases = casesMeta && casesMeta.count;

  const casesExist = totalCases > 0;

  return {
    loading: false,
    cases: casesExist ? cases.fetch() : [],
    total: totalCases,
    pagination: {
      position: 'bottom',
      total: totalCases,
      defaultCurrent: 1,
      pageSize: PerPage,
      onChange(page, perPage) {
        PageNumber.set(page);
      },
    },
  };
})(CasesList);
