import React from 'react';
import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';
import PropTypes from 'prop-types';
import {
  message, Modal, Icon, Collapse,
} from 'antd';
import { FormattedMessage } from 'react-intl';
import { CollectionsMeta } from '../../../api/CollectionsMeta';
import FilesCollection from '../../../api/Files';

const removeFile = (file) => {
  Modal.confirm({
    title: `Remove ${file.name}?`,
    content: 'This file will be removed permanently.',
    onOk: () => {
      Meteor.call('files.remove', { _id: file._id }, (err, res) => {
        if (err) {
          message.error(`something went wrong, ${err}`);
        } else {
          message.success('File removed successfully');
        }
      });
    },
    onCancel: () => {
    },
  });
};

const FileItem = ({ file }) => (
  <li className="files-displayer__list__item">
    <div>{file.name}</div>
    <div className="files-displayer__list__item-actions">
      <a
        title={`open ${file.name}`}
        href={FilesCollection.findOne({ _id: file._id }).link()}
        rel="noopener noreferrer"
        target="_blank"
      >
        <Icon type="eye" color="#00b894" />
      </a>
      <span title={`remove ${file.name}`}>
        <Icon type="delete" style={{ color: '#d63031' }} onClick={() => removeFile(file)} />
      </span>
    </div>
  </li>
);

const Files = ({ files, total }) => {
  if (total) {
    return (
      <Collapse className="files-displayer" bordered={false}>
        <Collapse.Panel
          header={(
            <p>
              <Icon type="file" />
              {'  '}
              <FormattedMessage
                id="fileDisplayer.cases.viewAttatchs"
                defaultMessage="View case attachments"
              />
            </p>
          )}
          key="1"
        >
          <div className="files-displayer__filesc">
            <ul className="files-displayer__files__list">
              <h4 style={{ color: 'gray' }}>
                <FormattedMessage
                  id="fileDisplayer.cases.totalAttatchs"
                  defaultMessage={` Number of existing files: ${total}`}
                  values={{ total }}
                />
              </h4>
              {files.map(file => (
                <FileItem key={file._id} file={file} />
              ))}
            </ul>
          </div>
        </Collapse.Panel>
      </Collapse>
    );
  }
  return (
    <h4 style={{ color: 'gray' }}>
      <FormattedMessage
        id="fileDisplayer.cases.noAttatchs"
        defaultMessage=" No files for this case yet."
      />
    </h4>
  );
};

FileItem.propTypes = {
  file: PropTypes.instanceOf(Object).isRequired,
};

Files.propTypes = {
  files: PropTypes.array.isRequired,
  total: PropTypes.number.isRequired,
};

export default withTracker((props) => {
  if (!props.caseId) {
    return {
      loading: true,
      files: [],
      total: 0,
    };
  }
  const filesHandle = Meteor.subscribe('files.cases.attachments', {
    filter: {},
    caseId: props.caseId,
  });
  if (!filesHandle.ready()) {
    return ({
      loading: true,
      files: [],
      total: 0,
    });
  }
  const filesMeta = CollectionsMeta.findOne({ _id: 'Files' });

  const files = FilesCollection.find({ 'meta.caseId': props.caseId }, {
    sort: { _id: 'DESC' },
  });
  const totalFiles = filesMeta && filesMeta.count;

  const filesExist = totalFiles > 0;

  return {
    loading: false,
    files: filesExist ? files.fetch() : [],
    total: totalFiles,
  };
})(Files);
