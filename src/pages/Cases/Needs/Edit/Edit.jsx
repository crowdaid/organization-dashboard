import React from 'react';
import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';
import PropTypes from 'prop-types';
import { Spin } from 'antd';
import Form from '../Form';
import NeedsCollection from '../../../../api/Needs';
import CategoriesCollection from '../../../../api/Categories';

const EditCase = ({
  need, ready, needTypes, itemTypes,
}) => {
  if (ready) return <Form formType="edit" need={need} needTypes={needTypes} itemTypes={itemTypes} />;
  return <Spin size="large" className="form__spinner" />;
};

EditCase.propTypes = {
  need: PropTypes.instanceOf(Object).isRequired,
  ready: PropTypes.bool.isRequired,
  needTypes: PropTypes.array.isRequired,
  itemTypes: PropTypes.array.isRequired,
};

// subscribe to need information
export default withTracker((props) => {
  const { match: { params: { id } } } = props;

  const needHandle = Meteor.subscribe('needs.single', id);
  const needTypesHandle = Meteor.subscribe('categories.all');

  if (!needHandle.ready() || !needTypesHandle.ready()) {
    return {
      ready: false,
      need: {},
      needTypes: [],
      itemTypes: [],
    };
  }

  const needTypes = CategoriesCollection.find({ categoryType: 'needType' }).fetch();
  const itemTypes = CategoriesCollection.find({ categoryType: 'item' }).fetch();

  const need = NeedsCollection.findOne({ _id: id });

  return {
    ready: true,
    need,
    needTypes,
    itemTypes,
  };
})(EditCase);
