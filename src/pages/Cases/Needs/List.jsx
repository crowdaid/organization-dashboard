import React, { Component } from 'react';
import { Meteor } from 'meteor/meteor';
import { ReactiveVar } from 'meteor/reactive-var';
import { withTracker } from 'meteor/react-meteor-data';
import PropTypes from 'prop-types';
import {
  Table, Modal, message, Button, Icon, Tooltip,
} from 'antd';
import { FormattedMessage } from 'react-intl';
import { CollectionsMeta } from '../../../api/CollectionsMeta';
import NeedsCollection from '../../../api/Needs';
// table filters and sorting prams
import { needsFilters, searchTables } from '../../../components/tablesFilters';

const PerPage = 10;
const PageNumber = new ReactiveVar(1);

const removeNeed = (Need) => {
  Modal.confirm({
    icon: 'cross',
    okText: 'Remove',
    okType: 'danger',
    title: `Remove Need: ${Need.title} ?`,
    content: `
      This need will removed from this case.
      Please note that all related donations will be removed as well
      `,
    onOk() {
      Meteor.call('needs.methods.remove', { _id: Need.id }, (err, res) => {
        if (err) {
          message.error('something went wrong');
        } else {
          message.success(`${Need.title} removed successfully`);
        }
      });
    },
    onCancel() { Modal.destroyAll(); },
  });
};

class CaseNeeds extends Component {
  constructor(props) {
    super(props);
    // set the title of the page
    document.title = 'Needs | CrowdAid';
    this.state = { searchText: '' };
    // bind search tables with 'this' class
    this.getColumnSearchProps = searchTables.getColumnSearchProps.bind(this);
    this.handleSearch = searchTables.handleSearch.bind(this);
    this.handleReset = searchTables.handleReset.bind(this);
  }

  render() {
    const {
      needs, pagination, loading, history, match: { params: { id } },
    } = this.props;

    const dataSource = needs.map(c => ({
      id: c._id,
      key: c._id,
      title: c.title,
      status: c.status,
      urgency: c.urgency,
      unit: c.unitOfMeasure,
      frequency: c.frequency,
      caseId: c.caseId,
      orgId: c.organizationId,
      dueDate: (new Date(c.dueDate)).toDateString(),
    }));

    const columns = [
      {
        title: <FormattedMessage id="needs.table.title" defaultMessage="Title" />,
        dataIndex: 'title',
        key: 'title',
        ...this.getColumnSearchProps('title', 'needs', this),
      },
      {
        title: <FormattedMessage id="needs.table.status" defaultMessage="Status" />,
        dataIndex: 'status',
        key: 'status',
        filters: needsFilters.status,
        onFilter: (value, record) => (record.status === value),
      },
      {
        title: <FormattedMessage id="needs.table.urgency" defaultMessage="Urgency" />,
        dataIndex: 'urgency',
        key: 'urgency',
        filters: needsFilters.urgency,
        onFilter: (value, record) => (record.urgency === value),
      },
      {
        title: <FormattedMessage id="needs.table.frequency" defaultMessage="Frequency" />,
        dataIndex: 'frequency',
        key: 'frequency',
        filters: needsFilters.frequency,
        onFilter: (value, record) => (record.frequency === value),
      },
      {
        title: <FormattedMessage id="needs.table.dueDate" defaultMessage="Due Date" />,
        dataIndex: 'dueDate',
        key: 'dueDate',
      },
      {
        title: <FormattedMessage id="needs.table.actions" defaultMessage="Actions" />,
        key: 'actions',
        render: (text, record) => (
          <span>
            <Button
              className="button"
              type="default"
              shape="circle-outline"
              icon="eye"
              title="View need"
              onClick={() => history.push(`/needs/${record.id}`)}
            />
            <Button
              className="button"
              type="primary"
              shape="circle-outline"
              icon="edit"
              title="Edit need"
              onClick={() => history.push(`/needs/${record.id}/edit`)}
            />
            {record.status !== 'fulfilled'
          && (
            <Tooltip title="Donate to this need">
              <Button
                className="button"
                type="default"
                style={{ backgroundColor: '#20bf6b', color: '#fff' }}
                shape="circle-outline"
                icon="dollar"
                onClick={() => history.push(`/donate/${record.caseId}?needId=${record.id}`)}
              />
            </Tooltip>

          )}
            <Button
              className="button"
              type="danger"
              title="Remove need"
              shape="circle-outline"
              icon="delete"
              onClick={() => removeNeed(record)}
            />
          </span>
        ),
      },
    ];

    return (
      <div className="container">
        <div className="needs__no-top">
          <div className="needs__table">
            <div className="needs__header">
              <h1>
                <Icon type="home" />
                {'  '}
                <FormattedMessage id="needs.title" defaultMessage="Case needs" />
              </h1>
              <div className="view__actions">
                <Button icon="arrow-left" onClick={() => history.goBack()}>
                  <FormattedMessage defaultMessage="Back" id="global.form.backButton" />
                </Button>
                <Button
                  type="primary"
                  icon="plus"
                  onClick={() => history.push(`/cases/${id}/needs/create`)}
                >
                  <FormattedMessage id="needs.buttons.add" defaultMessage=" Add New Need" />
                </Button>
              </div>
            </div>
            <Table
              pagination={pagination}
              loading={loading}
              dataSource={dataSource}
              columns={columns}
            />
            <div className="form-group">
              <Button
                block
                type="primary"
                icon="arrow-left"
                htmlType="submit"
                onClick={() => history.replace('/cases')}
              >
                <FormattedMessage id="needs.buttons.submit" defaultMessage=" Done and back" />
              </Button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

CaseNeeds.propTypes = {
  loading: PropTypes.bool.isRequired,
  needs: PropTypes.array.isRequired,
  pagination: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
};

export default withTracker((props) => {
  const { match: { params: { id } } } = props;
  const needsHandle = Meteor.subscribe('NeedsTabular', {
    filter: {},
    limit: PerPage,
    skip: (PageNumber.get() - 1) * PerPage,
    caseId: id,
  });
  if (!needsHandle.ready()) {
    return ({
      loading: true,
      needs: [],
      caseId: id,
      pagination: {},
    });
  }
  const needsMeta = CollectionsMeta.findOne({ _id: 'Needs' });

  const needs = NeedsCollection.find({ caseId: id }, {
    sort: { createdAt: -1 },
  });

  const totalNeeds = needsMeta && needsMeta.count;

  const needsExist = totalNeeds > 0;

  return {
    loading: false,
    needs: needsExist ? needs.fetch() : [],
    caseId: id,
    total: totalNeeds,
    pagination: {
      position: 'bottom',
      total: totalNeeds,
      defaultCurrent: 1,
      pageSize: PerPage,
      onChange(page, perPage) {
        PageNumber.set(page);
      },
    },
  };
})(CaseNeeds);
