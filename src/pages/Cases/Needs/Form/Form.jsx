import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import { Meteor } from 'meteor/meteor';
import moment from 'moment';
import {
  Input, Button, Form as AntForm,
  Switch, InputNumber, Select, Card, Divider,
  Icon, DatePicker, message, Tooltip,
} from 'antd';
import { FormattedMessage } from 'react-intl';
import FilesDisplayer from '../FilesDisplayer';
import Files from '../../../../api/Files';
import ItemModal from '../ItemModal';

// fixed values
const Units = ['EGP', 'Item'];
const needFrequencies = ['Once', 'Monthly', 'Yearly'];
const urgencyFlags = ['immediate', 'today', 'thisWeek', 'thisMonth', 'thisYear', 'N/A'];

// form for edit and create
class Form extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: '',
      description: '',
      dueDate: new Date(),
      urgency: '',
      needTypeId: '',
      itemId: '',
      attachments: [],
      filesCompleted: 0,
      filesFailed: 0,
      isQuantifiable: true,
      itemPrice: 0,
      frequency: '',
      addItemModal: false,
      unitOfMeasure: '',
      quantityNeeded: 0,
    };
    document.title = 'Needs-create | CrowdAid';
    // bind functions to class
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleDateChange = this.handleDateChange.bind(this);
    this.handleFilesChange = this.handleFilesChange.bind(this);
    this.handleItemPriceChange = this.handleItemPriceChange.bind(this);
    this.handleNeedTypeChange = this.handleNeedTypeChange.bind(this);
    this.handleItemTypesChange = this.handleItemTypesChange.bind(this);
    this.addNewItem = this.addNewItem.bind(this);
    this.handleQuantityNeededChange = this.handleQuantityNeededChange.bind(this);
    this.handleSelectChanges = this.handleSelectChanges.bind(this);
    this.handleIsQuantifiableChange = this.handleIsQuantifiableChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount() {
    // load the need data if edit form
    const { formType, need } = this.props;
    if (formType === 'edit' && need) {
      this.loadNeed(need);
    }
  }

  handleNeedTypeChange(needTypeId) {
    this.setState({
      needTypeId,
    });
  }

  handleItemTypesChange(itemId) {
    this.setState({
      itemId,
    });
  }

  loadNeed(need) {
    // set the page title same as need title
    document.title = `Needs-${need.title} | CrowdAid`;
    this.setState({
      title: need.title,
      description: need.description,
      unitOfMeasure: need.unitOfMeasure,
      quantityNeeded: need.quantityNeeded,
      needTypeId: need.needTypeId,
      itemId: need.itemId,
      dueDate: need.dueDate,
      isQuantifiable: need.isQuantifiable,
      itemPrice: need.itemPrice,
      urgency: need.urgency,
      frequency: need.frequency,
    });
  }

  /* ---------------------- handle components changes ---------------*/

  handleDateChange(dueDate) {
    this.setState({ dueDate: dueDate.toDate() });
  }


  handleInputChange(e) {
    const { name, value } = e.currentTarget;
    this.setState({ [name]: value });
  }

  handleIsQuantifiableChange(checked) {
    this.setState({ isQuantifiable: checked });
  }

  handleSelectChanges(val) {
    if (needFrequencies.includes(val)) this.setState({ frequency: val });
    else if (urgencyFlags.includes(val)) this.setState({ urgency: val });
    else this.setState({ unitOfMeasure: val });
  }


  handleItemPriceChange(value) {
    this.setState({
      itemPrice: value,
    });
  }

  handleQuantityNeededChange(value) {
    this.setState({
      quantityNeeded: value,
    });
  }

  filterItems(input, option) {
    this.setState();
    const { aliases } = option.props;
    const filteredA = aliases
      .concat([option.props.children])
      .filter(a => a.toLowerCase().indexOf(input.toLowerCase()) >= 0);
    if (filteredA.length !== 0) return true;
    return false;
  }

  addNewItem(itemId) {
    this.setState({ itemId });
  }

  handleFilesChange(e) {
    const { files } = e.currentTarget;
    const formattedFiles = [];

    Object.keys(files).forEach((k, i) => {
      formattedFiles.push(files[k]);
    });

    this.setState({
      attachments: formattedFiles,
    });
  }

  // handle upload files action
  uploadFiles(needId) {
    const { attachments } = this.state;
    attachments.forEach((file) => {
      Files.insert({
        file,
        meta: {
          namespace: 'needs',
          needId,
          type: 'attachments',
        },
        onUploaded: (err, f) => {
          if (!err) {
            Meteor.call('needs.methods.addAttachment', { needId, fileId: f._id });
            const { filesCompleted } = this.state;
            this.setState({
              filesCompleted: filesCompleted + 1,
            });
          }
        },
        onError: (err, f) => {
          const { filesFailed } = this.state;
          message.error(`Error on uploading ${f.name}, ${err.reason}`);
          this.setState({
            filesFailed: filesFailed + 1,
          });
        },
      });
    });
  }

  isFormValid() {
    const {
      title, description, quantityNeeded, isQuantifiable,
      itemId, unitOfMeasure, itemPrice, needTypeId,
    } = this.state;

    if (!title.trim()) {
      message.error('Title is required.');
      return false;
    }

    if (!description.trim()) {
      message.error('Description is required.');
      return false;
    }
    if (!needTypeId) {
      message.error('Please choose need type');
      return false;
    }

    if (isQuantifiable && unitOfMeasure !== 'Item' && quantityNeeded <= 1) {
      message.error('Please enter the quantity needed > 1');
      return false;
    }
    if (unitOfMeasure === 'Item' && !itemPrice) {
      message.error('Please enter the item price ');
      return false;
    }
    if (unitOfMeasure === 'Item' && !itemId) {
      message.error('Please choose the item needed ');
      return false;
    }

    return true;
  }


  handleSubmit(e) {
    e.preventDefault();

    if (!this.isFormValid()) return;

    const {
      history, need, formType, caseId,
    } = this.props;

    const {
      title,
      description,
      unitOfMeasure,
      quantityNeeded,
      dueDate,
      isQuantifiable,
      itemPrice,
      needTypeId,
      itemId,
      urgency,
      frequency,
    } = this.state;

    // configure data object
    const data = {
      title,
      description,
      unitOfMeasure,
      quantityNeeded,
      dueDate,
      isQuantifiable,
      needTypeId,
      itemId,
      itemPrice: itemPrice || 1,
      urgency,
      frequency,
    };

    if (formType === 'edit') {
    // edit the need
      Meteor.call('needs.methods.update', { _id: need._id, ...data }, (err) => {
        if (err) {
          message.error(err.reason);
          return;
        }
        // redirect with message
        this.uploadFiles(need._id);
        this.setState({ attachments: [] });
        const { filesFailed } = this.state;

        message.success('need updated successfully');
        if (!filesFailed) {
          history.goBack();
        }
      });
    } else {
      // create the need
      Meteor.call('needs.methods.create', { ...data, caseId }, (error, result) => {
        if (error) {
          message.error(error.reason);
          return;
        }
        this.uploadFiles(result);
        this.setState({ attachments: [] });
        const { filesFailed } = this.state;

        message.success('Need created and added to case successfully');
        if (!filesFailed) {
          history.goBack();
        }
      });
    }
  }


  render() {
    const {
      title, attachments, description, unitOfMeasure, quantityNeeded,
      dueDate, isQuantifiable, needTypeId, addItemModal,
      itemPrice, urgency, frequency, itemId,
    } = this.state;
    const {
      history, formType, need, needTypes, itemTypes,
    } = this.props;

    return (
      <div className="container">
        <div className="form__with-back">
          {formType === 'edit'
            ? (
              <h1>
                <FormattedMessage
                  id="needs.form.title.edit"
                  defaultMessage={`Edit ${title}`}
                  values={{ title }}
                />
              </h1>
            )
            : (
              <h1>
                <FormattedMessage
                  id="needs.form.title.new"
                  defaultMessage="Create new need"
                />
              </h1>
            )
            }
          <Button icon="arrow-left" onClick={() => history.goBack()}>
            <FormattedMessage defaultMessage="Back" id="global.form.backButton" />
          </Button>
        </div>
        <form dir="auto" onSubmit={this.handleSubmit}>
          <div className="flex column">
            <div className="form-group">
              <AntForm.Item
                label={(<FormattedMessage id="needs.form.title" defaultMessage="Title" />)}
                required
              >
                <Input name="title" value={title} onChange={this.handleInputChange} />
              </AntForm.Item>
            </div>
            <div className="form-group">
              <AntForm.Item
                label={(<FormattedMessage id="needs.form.description" defaultMessage="Description" />)}
                required
              >
                <Input.TextArea cols={100} name="description" value={description} onChange={this.handleInputChange} />
              </AntForm.Item>
            </div>
            <div className="flex row">
              <div className="form-group form__select">
                <AntForm.Item
                  label={(<FormattedMessage id="needs.form.needType" defaultMessage="Need Type" />)}
                  required
                >
                  <Select
                    name="needTypeId"
                    value={needTypeId}
                    onChange={this.handleNeedTypeChange}
                  >
                    {needTypes.map(flag => (
                      <Select.Option
                        key={flag._id}
                        value={flag._id}
                      >
                        {flag.name}
                      </Select.Option>
                    ))}
                  </Select>
                </AntForm.Item>
              </div>
              <div className="form-group form__select">
                <AntForm.Item
                  label={(<FormattedMessage id="needs.form.urgency" defaultMessage="Urgency" />)}
                >
                  <Select
                    defaultValue={urgencyFlags[0]}
                    name="urgency"
                    value={urgency || urgencyFlags[0]}
                    onChange={this.handleSelectChanges}
                  >
                    {urgencyFlags.map(flag => (
                      <Select.Option
                        key={flag}
                        value={flag}
                      >
                        {flag}
                      </Select.Option>
                    ))}
                  </Select>
                </AntForm.Item>
              </div>
              <div className="form-group form__select">
                <AntForm.Item
                  label={(<FormattedMessage id="needs.form.frequency" defaultMessage="Frequency" />)}
                >
                  <Select
                    defaultValue={needFrequencies[0]}
                    name="frequency"
                    value={frequency || needFrequencies[0]}
                    onChange={this.handleSelectChanges}
                  >
                    {needFrequencies.map(flag => (
                      <Select.Option
                        key={flag}
                        value={flag}
                      >
                        {flag}
                      </Select.Option>
                    ))}
                  </Select>
                </AntForm.Item>
              </div>
              <div dir="ltr" className="form-group">
                <AntForm.Item
                  label={(<FormattedMessage id="needs.form.dueDate" defaultMessage="Due Date" />)}
                >
                  <DatePicker
                    name="dueDate"
                    allowClear={false}
                    value={moment(dueDate)}
                    onChange={this.handleDateChange}
                    format="YYYY/MM/DD"
                  />
                </AntForm.Item>
              </div>
            </div>
            <Card
              title={(
                <div className="form-group">
                  <FormattedMessage id="needs.form.isQuantifiable" defaultMessage="Is Quantifiable" />
                  {' '}
                  <Switch
                    checked={isQuantifiable}
                    onChange={this.handleIsQuantifiableChange}
                  />
                </div>
               )}
              type="inner"
            >
              <div className="flex row">
                <div className="flex row">
                  <div className="form-group">
                    <AntForm.Item
                      label={(
                        <FormattedMessage
                          id="needs.form.unitOfMeasure"
                          defaultMessage="Unit Of Measure"
                        />)}
                    >
                      <Tooltip
                        title={(
                          <FormattedMessage
                            id="needs.form.unitOfMeasure.toolTip"
                            defaultMessage="Type of quantifiable item (EGP for money and Item eg. bed)"
                          />)}
                        placement="top"
                      >
                        <Select
                          defaultValue={Units[0]}
                          name="unitOfMeasure"
                          disabled={!isQuantifiable}
                          value={unitOfMeasure || Units[0]}
                          onChange={this.handleSelectChanges}
                        >
                          {Units.map(flag => (
                            <Select.Option
                              key={flag}
                              value={flag}
                            >
                              {flag}
                            </Select.Option>
                          ))}
                        </Select>
                      </Tooltip>
                    </AntForm.Item>
                  </div>
                  <div className="form-group">
                    <AntForm.Item
                      required
                      label={(
                        <FormattedMessage
                          id="needs.form.quantityNeeded"
                          defaultMessage="Quantity Needed"
                        />)}
                    >
                      <Tooltip
                        title={(
                          <FormattedMessage
                            id="needs.form.quantityNeeded.toolTip"
                            defaultMessage="Quantity required to mark this need as fulfilled"
                          />)}
                        placement="top"
                      >
                        <InputNumber
                          name="quantityNeeded"
                          disabled={!isQuantifiable}
                          value={quantityNeeded}
                          onChange={this.handleQuantityNeededChange}
                        />
                      </Tooltip>
                    </AntForm.Item>
                  </div>
                </div>
                <div className="flex row">
                  {unitOfMeasure === 'Item' && (
                  <div className="form-group">
                    <AntForm.Item
                      required
                      label={(<FormattedMessage id="needs.form.itemPrice" defaultMessage="Item Price" />)}
                    >
                      <Tooltip
                        title={(
                          <FormattedMessage
                            id="needs.form.itemPrice.toolTip"
                            defaultMessage="The total required price to fully have the
                                  requested item (Total money to buy a single bed or Total
                                    money to pay monthly bells for EGP type)"
                          />)}
                        placement="top"
                      >

                        <InputNumber
                          min={1}
                          value={itemPrice}
                          disabled={!isQuantifiable}
                          name="itemPrice"
                          onChange={this.handleItemPriceChange}
                        />
                      </Tooltip>
                    </AntForm.Item>
                  </div>
                  )}
                  {/* the item type needed */}
                  {unitOfMeasure === 'Item' && (
                  <div dir="ltr" className="form-group">
                    <AntForm.Item
                      required
                      label={(<FormattedMessage id="needs.form.itemType" defaultMessage="Choose the item needed" />)}
                    >
                      <Select
                        showSearch
                        style={{ width: 200 }}
                        className="fill"
                        value={itemId}
                        optionFilterProp="aliases"
                        onChange={this.handleItemTypesChange}
                        filterOption={this.filterItems}
                        dropdownRender={menu => (
                          <div>
                            {menu}
                            <Divider style={{ margin: '4px 0' }} />
                            <div style={{ marginBottom: '0.5rem', textAlign: 'center' }}>
                              <Button
                                type="ghost"
                                style={{ border: 'none', width: '100%' }}
                                icon="plus"
                                onClick={() => this.setState({ addItemModal: true })}
                              >
                                <FormattedMessage id="needs.form.addItem" defaultMessage=" Add new item" />
                              </Button>
                            </div>
                          </div>
                        )}
                      >
                        {itemTypes.map(item => (
                          <Select.Option aliases={item.aliases || []} value={item._id} key={item._id}>
                            {item.name}
                          </Select.Option>
                        ))}
                      </Select>
                    </AntForm.Item>
                  </div>
                  )}
                </div>
              </div>
            </Card>
            <div className="form-group">
              <AntForm.Item label=" Attachments">
                <div className="need__upload-btn-wrapper">
                  <Button>
                    <Icon type="upload" />
                    <FormattedMessage
                      id="needs.form.Attachments"
                      defaultMessage="Upload need attachments"
                    />
                  </Button>
                  <input
                    multiple
                    type="file"
                    name="need-attachments"
                    size="20"
                    onChange={this.handleFilesChange}
                  />
                </div>
                <ul className="need__files">
                  {attachments.map(file => <li key={file.name}>{file.name}</li>)}
                </ul>
                {need && <FilesDisplayer needId={need._id} />}
              </AntForm.Item>
            </div>
            <div className="form-group">
              <Button block type="primary" htmlType="submit" onClick={this.handleSubmit}>
                <FormattedMessage
                  id="needs.form.submitButton"
                  defaultMessage={formType === 'edit' ? 'Save' : 'Create'}
                  values={{ value: formType === 'edit' ? 'Save' : 'Create' }}
                />
              </Button>
              <ItemModal
                visible={addItemModal}
                handleOk={this.addNewItem}
                handleCancel={() => this.setState({ addItemModal: false })}
              />
            </div>
          </div>
        </form>
      </div>
    );
  }
}

Form.defaultProps = {
  caseId: '',
};

Form.propTypes = {
  history: PropTypes.instanceOf(Object).isRequired,
  formType: PropTypes.string.isRequired,
  caseId: PropTypes.string,
  needTypes: PropTypes.array.isRequired,
  itemTypes: PropTypes.array.isRequired,
};
export default withRouter(Form);
