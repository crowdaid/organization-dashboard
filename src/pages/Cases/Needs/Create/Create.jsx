import React from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import { Meteor } from 'meteor/meteor';
import CategoriesCollection from '../../../../api/Categories';
import Form from '../Form';

const CreateCaseNeed = (props) => {
  const { match: { params: { id } } } = props;
  return <Form formType="create" {...props} caseId={id} ready={1} />;
};

export default withTracker((props) => {
  const needTypesHandle = Meteor.subscribe('categories.all');

  if (!needTypesHandle.ready()) {
    return {
      ready: false,
      needTypes: [],
      itemTypes: [],
    };
  }

  const needTypes = CategoriesCollection.find({ categoryType: 'needType' }).fetch();
  const itemTypes = CategoriesCollection.find({ categoryType: 'item' }).fetch();

  return {
    ready: true,
    needTypes,
    itemTypes,
  };
})(CreateCaseNeed);
