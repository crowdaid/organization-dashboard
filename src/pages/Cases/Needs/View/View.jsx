import React, { Component, Fragment } from 'react';
import { Button, Spin } from 'antd';
import { Meteor } from 'meteor/meteor';
import { FormattedMessage } from 'react-intl';
import { withTracker } from 'meteor/react-meteor-data';
import PropTypes from 'prop-types';
import NeedsCollection from '../../../../api/Needs';
import Donations from '../../../Donations';
import FilesDisplayer from '../FilesDisplayer';

class ViewNeed extends Component {
    state = {
      title: '',
      description: '',
      dueDate: new Date(),
      urgency: '',
      isQuantifiable: true,
      itemPrice: 0,
      quantityFulfilled: 0,
      frequency: '',
      unitOfMeasure: '',
      quantityNeeded: 0,
    };

    componentWillReceiveProps({ need, ready, history }) {
      if (ready && need) {
        this.loadNeed(need);
      }
    }

    loadNeed(need) {
    // set the title of the page
      document.title = `Needs-${need.title} | CrowdAid`;
      this.setState({
        title: need.title,
        description: need.description,
        unitOfMeasure: need.unitOfMeasure,
        quantityNeeded: need.quantityNeeded,
        dueDate: need.dueDate,
        isQuantifiable: need.isQuantifiable,
        itemPrice: need.itemPrice,
        quantityFulfilled: need.quantityFulfilled,
        urgency: need.urgency,
        frequency: need.frequency,
        status: need.status,
      });
    }

    render() {
      const {
        title, description, unitOfMeasure, quantityNeeded,
        dueDate, isQuantifiable, status,
        itemPrice, quantityFulfilled, urgency, frequency,
      } = this.state;
      const { ready, history, need } = this.props;
      if (!ready) return <Spin size="large" className="form__spinner" />;
      return (
        <div className="container">
          <div className="view__with-back">
            <h1>{`${title}`}</h1>
            <Button icon="arrow-left" onClick={() => history.goBack()}>
              <FormattedMessage defaultMessage="Back" id="global.form.backButton" />
            </Button>
          </div>
          <hr />
          <div dir="auto">
            <table className="view__table">
              <tbody className="view__table__body">
                <tr className="view__table__body__row">
                  <td>
                    <b>
                      <FormattedMessage defaultMessage="Urgency" id="needs.view.urgency" />
                    </b>
                  </td>
                  <td>{urgency}</td>
                </tr>
                <tr>
                  <td>
                    <b>
                      <FormattedMessage defaultMessage="Due Date" id="needs.view.dueDate" />
                    </b>
                  </td>
                  <td>{dueDate.toDateString()}</td>
                </tr>
                <tr>
                  <td>
                    <b>
                      <FormattedMessage defaultMessage="Status" id="needs.view.status" />
                    </b>
                  </td>
                  <td>{status}</td>
                </tr>
                <tr>
                  <td>
                    <b>
                      <FormattedMessage defaultMessage="Description" id="needs.view.description" />
                    </b>
                  </td>
                  <td>{description}</td>
                </tr>
                <tr>
                  <td>
                    <b>
                      <FormattedMessage defaultMessage="Is Quantifiable" id="needs.view.isQuantifiable" />
                    </b>
                  </td>
                  <td>{isQuantifiable ? 'Yes' : 'No'}</td>
                </tr>
                <tr>
                  <td>
                    <b>
                      <FormattedMessage defaultMessage="Frequency" id="needs.view.frequency" />
                    </b>
                  </td>
                  <td>{frequency}</td>
                </tr>
                {isQuantifiable
                && (
                <Fragment>
                  <tr>
                    <td>
                      <b>
                        <FormattedMessage defaultMessage="Total Price" id="needs.view.totalPrice" />
                      </b>
                    </td>
                    <td>{`${itemPrice * quantityNeeded} EGP`}</td>
                  </tr>
                  {status !== 'fulfilled' && (
                  <tr>
                    <td>
                      <b>
                        <FormattedMessage defaultMessage="Total Price" id="needs.view.tobefull" />
                      </b>
                    </td>
                    <td>{`${(quantityNeeded - quantityFulfilled) * itemPrice} EGP`}</td>
                  </tr>
                  )}
                  <tr>
                    <td>
                      <b>
                        <FormattedMessage defaultMessage="Quantity Needed" id="needs.view.quantityNeeded" />
                      </b>
                    </td>
                    <td>{`${unitOfMeasure} ${quantityNeeded}`}</td>
                  </tr>
                  <tr>
                    <td>
                      <b>
                        <FormattedMessage defaultMessage=" Item price" id="needs.view.itemPrice" />
                      </b>
                    </td>
                    <td>{`${itemPrice} EGP`}</td>
                  </tr>
                  <tr>
                    <td>
                      <b>
                        <FormattedMessage defaultMessage=" Quantity fulfilled" id="needs.view.quantityFulfilled" />
                      </b>
                    </td>
                    <td>{`${unitOfMeasure} ${quantityFulfilled}`}</td>
                  </tr>
                </Fragment>
                )
                }
              </tbody>
            </table>
          </div>
          {need && <FilesDisplayer needId={need._id} />}
          {need && <Donations needId={need._id} />}
        </div>
      );
    }
}

ViewNeed.propTypes = {
  ready: PropTypes.bool.isRequired,
  history: PropTypes.instanceOf(Object).isRequired,
  need: PropTypes.instanceOf(Object).isRequired,
};

export default withTracker((props) => {
  const { match: { params: { id } } } = props;

  const needHandle = Meteor.subscribe('needs.single', id);
  const need = NeedsCollection.findOne({ _id: id });
  if (!needHandle.ready()) {
    return {
      ready: false,
      need: {},
    };
  }

  return {
    ready: true,
    need,
  };
})(ViewNeed);
