import React, { Component } from 'react';
import {
  Modal, message, Input, Divider,
} from 'antd';
import { Meteor } from 'meteor/meteor';
import { FormattedMessage } from 'react-intl';

class ItemModal extends Component {
  constructor(props) {
    super(props);
    this.state = { itemName: '', matchNames: [] };
    this.handleAddItem = this.handleAddItem.bind(this);
    this.renderMatches = this.renderMatches.bind(this);
    this.resetFeilds = this.resetFeilds.bind(this);
    this.callAddItemMethod = this.callAddItemMethod.bind(this);
    this.handleItemNameChange = this.handleItemNameChange.bind(this);
  }

  handleItemNameChange(e) {
    const itemName = e.target.value;
    this.setState({ itemName });
    if (itemName.length >= 2) {
      setTimeout(() => {
        Meteor.call('categories.methods.search', { keyword: itemName.trim() }, (err, matchNames) => {
          this.setState({ matchNames: matchNames || [] });
        });
      }, 500);
    } else {
      this.setState({ matchNames: [] });
    }
  }

  callAddItemMethod() {
    const { handleCancel, handleOk } = this.props;
    const { itemName } = this.state;
    Meteor.call('categories.methods.create', {
      parentCategory: null,
      categoryType: 'item',
      name: itemName.trim(),
    }, (error, id) => {
      if (error) {
        message.error(error.reason);
      } else {
        handleCancel();
        this.setState({ matchNames: [], itemName: '' });
        handleOk(id);
        message.success('Item added successfully');
      }
    });
  }

  handleAddItem() {
    const { matchNames } = this.state;

    if (matchNames.length !== 0) {
      Modal.confirm({
        title: 'Do you want ignore the items match ?',
        content: 'This item name may already exists ',
        onOk: () => {
          this.callAddItemMethod();
        },
        onCancel() {},
      });
    } else {
      this.callAddItemMethod();
    }
  }

  resetFeilds() {
    const { handleCancel } = this.props;
    this.setState({ matchNames: [], itemName: '' });
    handleCancel();
  }


  renderMatches() {
    const { matchNames } = this.state;
    if (matchNames.length !== 0) {
      return (
        <div dir="auto">
          <Divider />
          <h2 className="categories__matches-header">
            <FormattedMessage id="itemModal.matsh" defaultMessage="These items may partially match the entered name" />
          </h2>
          <ul className="matches-ul">
            {matchNames.map(match => (
              <li key={match._id}>
                <b>{match.name}</b>
              </li>
            ))}
          </ul>
        </div>
      );
    }
    return null;
  }

  render() {
    const { itemName } = this.state;
    const { visible } = this.props;
    return (
      <Modal
        title={(<FormattedMessage id="needs.form.addItem" defaultMessage=" Add new item" />)}
        visible={visible}
        destroyOnClose
        onOk={this.handleAddItem}
        onCancel={this.resetFeilds}
      >
        <div dir="auto">
          <b>
            <FormattedMessage id="itemModal.text" defaultMessage="Enter the new item name" />
          </b>
          <Input style={{ marginTop: '1rem' }} name="itemName" onChange={this.handleItemNameChange} value={itemName} />
        </div>
        {this.renderMatches()}
      </Modal>
    );
  }
}


export default ItemModal;
