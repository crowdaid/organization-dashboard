import React from 'react';
import { withRouter } from 'react-router-dom';
import Form from '../Form';

const CreateCase = props => <Form formType="create" {...props} ready={1} />;

export default withRouter(CreateCase);
