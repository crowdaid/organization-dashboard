import React from 'react';
import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';
import PropTypes from 'prop-types';
import { Spin } from 'antd';
import CasesCollection from '../../../api/Cases';
import Form from '../Form';

const EditCase = ({
  ready, orgCase, history, matchId,
}) => {
  if (ready) return <Form formType="edit" orgCase={orgCase} history={history} matchId={matchId} />;
  return <Spin size="large" className="form__spinner" />;
};


EditCase.propTypes = {
  orgCase: PropTypes.instanceOf(Object).isRequired,
  ready: PropTypes.bool.isRequired,
  history: PropTypes.instanceOf(Object).isRequired,
  matchId: PropTypes.string.isRequired,
};

// subscribe to beneficiary information
export default withTracker((props) => {
  const { match: { params: { id } } } = props;

  const caseHandle = Meteor.subscribe('cases.single', id);
  const orgCase = CasesCollection.findOne({ _id: id });
  if (!caseHandle.ready()) {
    return {
      ready: false,
      matchId: id,
      orgCase: {},
    };
  }
  return {
    ready: true,
    matchId: id,
    orgCase,
  };
})(EditCase);
