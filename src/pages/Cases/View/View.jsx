import React, { Component } from 'react';
import { Button, Spin, message } from 'antd';
import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import CasesCollection from '../../../api/Cases';
import CategoriesCollection from '../../../api/Categories';
import BeneficiariesCollection from '../../../api/Beneficiaries';
import Donations from '../../Donations';
import FilesDisplayer from '../FilesDisplayer';


const copyToClipboard = (str) => {
  const el = document.createElement('textarea');
  el.value = str;
  document.body.appendChild(el);
  el.select();
  document.execCommand('copy');
  document.body.removeChild(el);
  message.success('Copied to clipboard');
};

class ViewCase extends Component {
  state = {
    id: '',
    title: '',
    description: '',
    dueDate: new Date(),
    urgency: '',
    causes: [],
    status: '',
    ben: {},
    address: null,
  };

  componentWillReceiveProps({
    c, ready, allCauses, history, ben,
  }) {
    if (ready && c) this.loadCase(c, allCauses, ben);
  }

  loadCase(c, allCauses, ben) {
    // set the title of the page
    document.title = `Case-${c.title} | CrowdAid`;
    this.setState({
      id: c._id,
      title: c.title,
      status: c.status,
      description: c.description,
      urgency: c.urgency,
      address: c.caseLocation.address,
      caseCode: c.caseCode,
      ben,
      dueDate: c.dueDate,
      causes: allCauses.filter(cause => c.categories.indexOf(cause._id) !== -1)
        .map(cause => cause.name),
    });
  }

  render() {
    const {
      title, urgency, description, dueDate, status,
      address, causes, ben, id, caseCode,
    } = this.state;
    const { ready, history, c } = this.props;
    // show spinner until the subscription is ready
    if (!ready) return <Spin size="large" className="form__spinner" />;
    return (
      <div className="">
        <div className="view__with-back">
          <h1>{`${title}`}</h1>
          <div className="view__actions">
            <Button icon="arrow-left" onClick={() => history.goBack()}>
              <FormattedMessage defaultMessage="Back" id="global.form.backButton" />
            </Button>
            <Button
              className="button"
              type="dashed"
              title="View related needs"
              onClick={() => history.push(`/cases/${id}/needs`)}
            >
              <FormattedMessage id="cases.view.buttons.viewNeeds" defaultMessage="View Needs" />
            </Button>
            <Button
              className="button"
              type="dashed"
              title="View related beneficiary"
              onClick={() => history.push(`/beneficiaries/${ben._id}`)}
            >
              <FormattedMessage id="cases.view.buttons.viewBeneficiary" defaultMessage="View beneficiary" />
            </Button>
          </div>
        </div>
        <hr />
        <div dir="auto">
          <table className="view__table">
            <tbody className="view__table__body">
              <tr className="view__table__body__row">
                <td>
                  <b>
                    <FormattedMessage id="cases.table.caseCode" defaultMessage="Case code number" />
                  </b>
                </td>
                <td>
                  {caseCode}
                  {' '}
                  <Button
                    title="Copy"
                    icon="copy"
                    type="ghost"
                    style={{ border: 'none' }}
                    onClick={() => copyToClipboard(caseCode)}
                  />
                </td>
              </tr>
              <tr className="view__table__body__row">
                <td>
                  <b>
                    <FormattedMessage id="cases.view.urgency" defaultMessage="Urgency" />
                  </b>
                </td>
                <td>{urgency}</td>
              </tr>
              <tr>
                <td>
                  <b>
                    <FormattedMessage id="cases.view.dueDate" defaultMessage=" Due Date" />
                  </b>
                </td>
                <td>{dueDate.toDateString()}</td>
              </tr>
              <tr>
                <td>
                  <b>
                    <FormattedMessage id="cases.view.status" defaultMessage="Status" />
                  </b>
                </td>
                <td>{status}</td>
              </tr>
              <tr>
                <td>
                  <b>
                    <FormattedMessage id="cases.view.description" defaultMessage="Description" />
                  </b>
                </td>
                <td>{description}</td>
              </tr>
              <tr>
                <td>
                  <b>
                    <FormattedMessage id="cases.view.address" defaultMessage="Case address" />
                  </b>
                </td>
                <td>{address}</td>
              </tr>
              <tr>
                <td>
                  <b>
                    <FormattedMessage id="cases.view.beneficiary" defaultMessage="Beneficiary" />
                  </b>
                </td>
                <td>{`${ben.name.ar} (${ben.name.en})`}</td>
              </tr>
              <tr>
                <td>
                  <b>
                    <FormattedMessage id="cases.view.causes" defaultMessage="Case causes" />
                  </b>
                </td>
                <td>{causes.map(cc => <p key={cc}>{cc}</p>)}</td>
              </tr>
            </tbody>
          </table>
        </div>
        {c && <FilesDisplayer caseId={c._id} />}
        {c && <Donations caseId={c._id} />}

      </div>
    );
  }
}

ViewCase.propTypes = {
  ready: PropTypes.bool.isRequired,
  history: PropTypes.instanceOf(Object).isRequired,
  c: PropTypes.instanceOf(Object).isRequired,
  allCauses: PropTypes.array.isRequired,
  ben: PropTypes.instanceOf(Object).isRequired,
};

export default withTracker((props) => {
  const { match: { params: { id } } } = props;

  const casesHandle = Meteor.subscribe('cases.single', id);
  const causesHandle = Meteor.subscribe('categories.all');
  const c = CasesCollection.findOne({ _id: id });

  if (!causesHandle.ready() || !casesHandle.ready()) {
    return {
      ready: false,
      allCauses: [],
      c: {},
      ben: {},
    };
  }

  const allCauses = CategoriesCollection.find({ categoryType: 'cause' }).fetch();
  const ben = BeneficiariesCollection.findOne({ _id: c.beneficiary });

  return {
    ready: true,
    allCauses,
    c,
    ben,
  };
})(ViewCase);
