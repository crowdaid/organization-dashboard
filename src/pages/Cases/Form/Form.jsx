import React, { Component } from 'react';
import {
  Button, Input, Select, DatePicker, Form as AntForm, Icon, message,
} from 'antd';
import moment from 'moment';
import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import GoogleMap from '../../../components/GoogleMap';
import CategoriesCollection from '../../../api/Categories';
import BeneficiariesCollection from '../../../api/Beneficiaries';
import FilesDisplayer from '../FilesDisplayer';
import Files from '../../../api/Files';

export const urgencyFlags = ['N/A', 'today', 'immediate', 'thisWeek', 'thisMonth', 'thisYear'];
const caseStatuses = ['draft', 'approved', 'pending', 'rejected', 'terminated'];

class Form extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: '',
      description: '',
      dueDate: new Date(),
      urgency: urgencyFlags[0],
      status: caseStatuses[0],
      causes: [],
      beneficiary: null,
      attachments: [],
      filesCompleted: 0,
      filesFailed: 0,
      caseLocation: null,
    };
    // set the title of the page
    const { formType } = this.props;
    document.title = `Cases-${formType} | CrowdAid`;
    // bind functions to class
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleDateChange = this.handleDateChange.bind(this);
    this.handleMapChange = this.handleMapChange.bind(this);
    this.handleFilesChange = this.handleFilesChange.bind(this);
    this.handleCausesChange = this.handleCausesChange.bind(this);
    this.handleSelectChanges = this.handleSelectChanges.bind(this);
    this.handleBeneficiaryChange = this.handleBeneficiaryChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentWillReceiveProps(props) {
    const { formType } = props;
    if (formType === 'edit') this.loadCase(props);
  }

  // load case info if edit
  loadCase({
    orgCase,
  }) {
    document.title = `Cases-${orgCase.title} | CrowdAid`;
    this.setState({
      title: orgCase.title,
      description: orgCase.description,
      urgency: orgCase.urgency,
      status: orgCase.status,
      caseLocation: orgCase.caseLocation || null,
      causes: orgCase.categories,
      beneficiary: orgCase.beneficiary,
      dueDate: orgCase.dueDate,
    });
  }

  handleSubmit(e) {
    e.preventDefault();

    if (!this.isFormValid()) return;

    const {
      history, matchId, formType,
    } = this.props;

    const {
      title, description, urgency, causes, dueDate, caseLocation, status, beneficiary,
    } = this.state;

    // configure data object
    const data = {
      title,
      description,
      urgency,
      beneficiary,
      status,
      caseLocation,
      categories: causes,
      dueDate,
    };
    if (formType === 'edit') {
    // edit the cases
      Meteor.call('cases.methods.update', { _id: matchId, ...data }, (err) => {
        if (err) {
          message.error(err.reason);
          return;
        }
        // redirect with message
        this.uploadFiles(matchId);
        this.setState({ attachments: [] });
        const { filesFailed } = this.state;

        message.success('cases updated successfully');
        if (!filesFailed) {
          history.replace('/cases');
        }
      });
    } else {
      // create the cases
      Meteor.call('cases.methods.create', { ...data }, (error, result) => {
        if (error) {
          message.error(error.reason);
          return;
        }
        this.uploadFiles(result);
        this.setState({ attachments: [] });
        const { filesFailed } = this.state;

        message.success('Case created successfully');

        if (!filesFailed) {
          history.replace(`/cases/${result}/needs`);
        }
      });
    }
  }

  isFormValid() {
    const {
      title, description, caseLocation, beneficiary,
    } = this.state;

    if (!title.trim()) {
      message.error('Title is required.');
      return false;
    }

    if (!description.trim()) {
      message.error('Description is required.');
      return false;
    }
    if (!beneficiary) {
      message.error('Each case must have a beneficiary');
      return false;
    }
    if (!caseLocation) {
      message.error('The case location is required.');
      return false;
    }
    if (caseLocation && caseLocation.location.coordinates && !caseLocation.address) {
      message.error("The case should't have a location without address");
      return false;
    }

    return true;
  }

  uploadFiles(caseId) {
    const { attachments } = this.state;
    attachments.forEach((file) => {
      Files.insert({
        file,
        meta: {
          namespace: 'cases',
          caseId,
          type: 'attachments',
        },
        onUploaded: (err, f) => {
          if (!err) {
            Meteor.call('cases.methods.addAttachment', { caseId, fileId: f._id });
          }
        },
        onError: (err, f) => {
          const { filesFailed } = this.state;
          message.error(`Error on uploading ${f.name}, ${err.reason}`);
          this.setState({
            filesFailed: filesFailed + 1,
          });
        },
      });
    });
  }

  handleMapChange(point) {
    this.setState({ caseLocation: point });
  }

  handleDateChange(dueDate) {
    this.setState({ dueDate: dueDate.toDate() });
  }

  handleInputChange(e) {
    const { name, value } = e.currentTarget;
    this.setState({ [name]: value });
  }

  handleFilesChange(e) {
    const { files } = e.currentTarget;
    const formattedFiles = [];

    Object.keys(files).forEach((k, i) => {
      formattedFiles.push(files[k]);
    });

    this.setState({
      attachments: formattedFiles,
    });
  }

  handleCausesChange(causes) {
    this.setState({ causes });
  }

  handleSelectChanges(val) {
    if (caseStatuses.includes(val)) this.setState({ status: val });
    else this.setState({ urgency: val });
  }

  handleBeneficiaryChange(beneficiary, opt) {
    this.setState({ beneficiary });
  }


  render() {
    const {
      title, attachments, urgency, description, dueDate, status,
      caseLocation, causes, beneficiary,
    } = this.state;
    const {
      history, formType, allCauses, allBens, orgCase,
    } = this.props;
    return (
      <div className="container">
        <div className="form__with-back">
          {formType === 'edit'
            ? (
              <h1>
                <FormattedMessage
                  id="cases.form.title.edit"
                  defaultMessage={`Edit ${title}`}
                  values={{ title }}
                />
              </h1>
            )
            : (
              <h1>
                <FormattedMessage
                  id="cases.form.title.new"
                  defaultMessage="Create new case"
                />
              </h1>
            )
            }
          <Button icon="arrow-left" onClick={() => history.goBack()}>
            <FormattedMessage defaultMessage="Back" id="global.form.backButton" />
          </Button>
        </div>
        <AntForm dir="auto" onSubmit={this.handleSubmit}>
          <div className="case__form-flex column">
            <div className="case__form-group">
              <AntForm.Item
                label={(<FormattedMessage id="cases.form.title" defaultMessage="Title" />)}
                required
              >
                <Input name="title" value={title} onChange={this.handleInputChange} />
              </AntForm.Item>
            </div>
            <div className="case__form-group">
              <AntForm.Item
                label={(<FormattedMessage id="cases.form.description" defaultMessage="Description" />)}
                required
              >
                <Input.TextArea
                  cols={100}
                  name="description"
                  value={description}
                  onChange={this.handleInputChange}
                />
              </AntForm.Item>
            </div>
            <div className="case__form-flex row">
              <div className="case__form-group">
                <AntForm.Item
                  label={(<FormattedMessage id="cases.form.causes" defaultMessage="Causes" />)}
                >
                  <Select
                    mode="multiple"
                    style={{ width: 310 }}
                    value={causes}
                    placeholder="Select causes.."
                    onChange={this.handleCausesChange}
                  >
                    {allCauses.map(c => (
                      <Select.Option
                        key={c._id}
                      >
                        {c.name}
                      </Select.Option>
                    ))}
                  </Select>
                </AntForm.Item>
              </div>
              <div className="case__form-group">
                <AntForm.Item
                  label={(
                    <FormattedMessage
                      id="cases.form.beneficiary"
                      defaultMessage=" Select Beneficiary"
                    />)}
                  required
                >
                  <Select
                    showSearch
                    style={{ width: 400 }}
                    placeholder="Select a beneficiary"
                    defaultActiveFirstOption
                    value={beneficiary}
                    optionFilterProp="children"
                    onChange={this.handleBeneficiaryChange}
                    filterOption={(input, option) => option.props.children
                      .toLowerCase().indexOf(input.toLowerCase()) >= 0}
                  >
                    {allBens.map(b => (
                      <Select.Option
                        key={b._id}
                      >
                        <div>
                          <b style={{ marginRight: '1rem' }}>{`${b.name.ar}`}</b>
                          <span>{b.name.en}</span>
                        </div>
                      </Select.Option>
                    ))}
                  </Select>
                </AntForm.Item>
              </div>
            </div>
            <div className="case__form-flex row">
              <div className="case__form-group form__select">
                <AntForm.Item
                  label={(
                    <FormattedMessage
                      id="cases.form.urgency"
                      defaultMessage="Urgency"
                    />)}
                >
                  <Select
                    defaultValue={urgencyFlags[0]}
                    name="urgency"
                    value={urgency}
                    onChange={this.handleSelectChanges}
                  >
                    {urgencyFlags.map(flag => (
                      <Select.Option
                        key={flag}
                        value={flag}
                      >
                        {flag}
                      </Select.Option>
                    ))}
                  </Select>
                </AntForm.Item>

              </div>
              {status !== 'fulfilled' && (
              <div className="case__form-group form__select">
                <AntForm.Item
                  label={(
                    <FormattedMessage
                      id="cases.form.status"
                      defaultMessage="Status"
                    />)}
                >
                  <Select
                    defaultValue={caseStatuses[0]}
                    name="status"
                    value={status}
                    onChange={this.handleSelectChanges}
                  >
                    {caseStatuses.map(stat => (
                      <Select.Option
                        key={stat}
                        value={stat}
                      >
                        {stat}
                      </Select.Option>
                    ))}
                  </Select>
                </AntForm.Item>
              </div>
              )}
              <div dir="ltr" className="case__form-group">
                <AntForm.Item
                  label={(
                    <FormattedMessage
                      id="cases.form.dueDate"
                      defaultMessage="Due Date"
                    />)}
                >
                  <DatePicker
                    name="dueDate"
                    allowClear={false}
                    value={moment(dueDate)}
                    onChange={this.handleDateChange}
                    format="YYYY/MM/DD"
                  />
                </AntForm.Item>
              </div>
            </div>
            <div className="case__form-group">
              <AntForm.Item
                label={(
                  <FormattedMessage
                    id="cases.form.attachments"
                    defaultMessage="Attachments"
                  />)}
              >
                <div className="case__upload-btn-wrapper">
                  <Button>
                    <Icon type="upload" />
                    <FormattedMessage
                      id="cases.form.buttons.upload"
                      defaultMessage=" Upload case attachments"
                    />
                  </Button>
                  <input
                    multiple
                    type="file"
                    name="cases_attachments"
                    size="20"
                    onChange={this.handleFilesChange}
                  />
                </div>
                <ul className="case__files">
                  {attachments.map(file => <li key={file.name}>{file.name}</li>)}
                </ul>

                {orgCase && <FilesDisplayer caseId={orgCase._id} />}
              </AntForm.Item>
            </div>
            <hr style={{ margin: '1rem 0' }} />
            <div className="form__map">
              <AntForm.Item
                label={(
                  <FormattedMessage
                    id="cases.form.caseLocation"
                    defaultMessage="The case location and address"
                  />
              )}
                required
              >
                <GoogleMap
                  point={caseLocation || null}
                  onChange={this.handleMapChange}
                />
              </AntForm.Item>
            </div>
            <div className="">
              {formType === 'edit'
                ? (
                  <Button
                    block
                    type="primary"
                    htmlType="submit"
                    onClick={this.handleSubmit}
                  >
                    <FormattedMessage
                      id="cases.form.saveButton"
                      defaultMessage="Save"
                    />
                  </Button>
                )
                : (
                  <Button
                    block
                    type="primary"
                    htmlType="submit"
                    onClick={this.handleSubmit}
                    icon="arrow-right"
                  >
                    <FormattedMessage
                      id="cases.form.addNeedsButton"
                      defaultMessage="Add Needs"
                    />
                  </Button>
                )}
            </div>
          </div>
        </AntForm>
      </div>
    );
  }
}


Form.defaultProps = {
  matchId: '',
};

Form.propTypes = {
  history: PropTypes.instanceOf(Object).isRequired,
  allCauses: PropTypes.instanceOf(Object).isRequired,
  allBens: PropTypes.instanceOf(Object).isRequired,
  matchId: PropTypes.string,
  formType: PropTypes.string.isRequired,
};
export default withTracker((props) => {
  const causesHandle = Meteor.subscribe('categories.org');
  const bensHandle = Meteor.subscribe('org.beneficiaries');

  if (!causesHandle.ready() && !bensHandle.ready()) {
    return {
      loading: true,
      allCauses: [],
      allBens: [],
      ...props,
    };
  }

  const allCauses = CategoriesCollection.find({ categoryType: 'cause' }).fetch();
  const allBens = BeneficiariesCollection.find({}).fetch();

  return {
    loading: false,
    allCauses,
    allBens,
    ...props,
  };
})(Form);
