import React from 'react';
import { FormattedMessage } from 'react-intl';

document.title = 'NotFound | CrowdAid';
const NotFound = props => (
  <div className="notfound">
    <h1 className="notfound__h1">
      <FormattedMessage id="notFound.mainMsg" defaultMessage="YOU JUST GOT 404:(" />
    </h1>
    <h3 className="notfound__h3">
      <FormattedMessage
        id="notFound.subMsg"
        defaultMessage="THE PAGE YOU ARE LOOKING FOR DOES NOT EXISTS, SORRY "
      />
    </h3>
  </div>
);

export default NotFound;
