import React, { Component, Fragment } from 'react';
import {
  Card, Avatar, Select, Button, Form, Input, Popconfirm, Icon, Divider, message,
} from 'antd';
import { FormattedMessage } from 'react-intl';
import { Meteor } from 'meteor/meteor';
import { validateEmail, validateMobileNum } from '../../../../components/utils';

class SelectDonor extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedUser: undefined,
      showUserForm: false,
      noUserInfo: false,
      matchUsers: [],
      userAccountInfo: {
        username: '', email: '', address: '', phone: '',
      },
    };
    // props.goNextStep.bind(this);
    this.handleSubmitUser = this.handleSubmitUser.bind(this);
    this.handleUserChange = this.handleUserChange.bind(this);
    this.handleUsersSearch = this.handleUsersSearch.bind(this);
    this.renderSelectUser = this.renderSelectUser.bind(this);
    this.handleInputChage = this.handleInputChage.bind(this);
    this.showAddUserForm = this.showAddUserForm.bind(this);
  }

  handleUserChange(selectedUser) {
    this.setState({ selectedUser });
  }

  showAddUserForm() {
    this.setState(prev => ({
      showUserForm: !prev.showUserForm,
      userAccountInfo: {
        username: '', email: '', address: '', phone: '',
      },
      selectedUser: undefined,
    }));
  }

  handleInputChage(e) {
    const { userAccountInfo } = this.state;
    const { name, value } = e.target;
    this.setState({ userAccountInfo: { ...userAccountInfo, [name]: value } });
  }

  handleSubmitUser() {
    const { goNextStep } = this.props;
    const { userAccountInfo, selectedUser } = this.state;
    if (selectedUser) {
      goNextStep(selectedUser);
    } else if (userAccountInfo.username && userAccountInfo.email) {
      const data = {
        fromOrg: true,
        username: userAccountInfo.username,
        email: userAccountInfo.email,
        profile: {
          address: userAccountInfo.address,
          phone: userAccountInfo.phone,
        },
      };
      if (!validateEmail(userAccountInfo.email)) {
        message.error('Please enter a valid email address');
        return;
      }
      if (userAccountInfo.phone && !validateMobileNum(userAccountInfo.phone)) {
        message.error('Please enter a valid mobile number ');
        return;
      }
      Meteor.call('users.methods.register_donor_user', data, (err, userId) => {
        if (err) message.error(err.reason);
        else goNextStep(userId);
      });
    } else {
      goNextStep(selectedUser);
    }
  }

  handleUsersSearch(keyword) {
    if (keyword.length >= 2) {
      setTimeout(() => {
        Meteor.call('users.methods.search_donors', { keyword }, (err, matchUsers) => {
          if (!err) this.setState({ matchUsers });
        });
      }, 1000);
    }
  }

  renderUserInfo= userInfo => (
    <div>
      <h3 style={{ marginTop: 16 }}>The selected user information</h3>
      <Card style={{ marginTop: 16 }}>
        <Card.Meta
          avatar={<Avatar icon="user" size="large" shape="square" src={userInfo.userImage} />}
          title={userInfo.username}
          description={userInfo.emails[0].address}
        />
        <table className="userInfo__table">
          <tbody>
            <tr>
              <td>Mobile number</td>
              <td>
                {userInfo.profile ? userInfo.profile.phone : `No mobile number for ${userInfo.username}`}
              </td>
            </tr>
            <tr>
              <td>Address</td>
              <td>
                {userInfo.profile ? userInfo.profile.address : `No address for ${userInfo.username}`}
              </td>
            </tr>
          </tbody>
        </table>
      </Card>
    </div>

  )

  renderSelectUser() {
    const { selectedUser, matchUsers } = this.state;
    const userInfo = matchUsers.filter(user => user._id === selectedUser)[0];
    return (
      <div style={{ marginTop: '4%' }}>
        <p>
          <FormattedMessage
            defaultMessage=" Search in all donors users with username, email or mobile number"
            id="donations.form.select.search"
          />
        </p>
        <Select
          showSearch
          placeholder="Search and select user ..."
          value={selectedUser}
          className="donation__searsh"
          defaultActiveFirstOption={false}
          showArrow={false}
          filterOption={false}
          onSearch={this.handleUsersSearch}
          onChange={this.handleUserChange}
          notFoundContent={null}
        >
          {matchUsers.map(user => (
            <Select.Option key={user._id} value={user._id}>
              {user.username || user.email}
            </Select.Option>
          ))}
        </Select>
        <Button onClick={this.showAddUserForm} type="dashed" icon="plus">
          <FormattedMessage defaultMessage="Add new user" id="donations.form.select.addUser" />
        </Button>
        { userInfo && this.renderUserInfo(userInfo)}

      </div>
    );
  }

  renderUserForm() {
    const { userAccountInfo } = this.state;
    return (
      <div dir="auto" style={{ width: 500 }}>
        <Divider>
          <FormattedMessage
            defaultMessage="Create new donor account"
            id="donations.form.select.add.title"
          />
        </Divider>
        <form>
          <Button title="go back" onClick={this.showAddUserForm} icon="arrow-left" type="ghost" style={{ float: 'right', border: 'none' }} />
          <div className="flex column">
            <Form.Item
              required
              label={(
                <FormattedMessage
                  defaultMessage="Username"
                  id="donations.form.select.add.username"
                />
            )}
            >
              <Input value={userAccountInfo.username} required name="username" onChange={this.handleInputChage} />
            </Form.Item>
            <Form.Item
              required
              label={(
                <FormattedMessage
                  defaultMessage="Email"
                  id="donations.form.select.add.email"
                />
            )}
            >
              <Input value={userAccountInfo.email} required type="email" name="email" onChange={this.handleInputChage} />
            </Form.Item>
            <Form.Item label={(
              <FormattedMessage
                defaultMessage="Home address"
                id="donations.form.select.add.address"
              />
            )}
            >
              <Input value={userAccountInfo.address} name="address" onChange={this.handleInputChage} />
            </Form.Item>
            <Form.Item label={(
              <FormattedMessage
                defaultMessage="Mobile number"
                id="donations.form.select.add.phone"
              />)}
            >
              <Input value={userAccountInfo.phone} name="phone" onChange={this.handleInputChage} />
            </Form.Item>
          </div>
        </form>
      </div>
    );
  }

  render() {
    const { showUserForm, selectedUser, userAccountInfo } = this.state;

    return (
      <Fragment>
        <div className="donation__selectuser__container">
          {!showUserForm && this.renderSelectUser()}
          {showUserForm && this.renderUserForm()}
        </div>
        <div style={{ marginTop: '5%' }}>
          <Popconfirm
            placement="topLeft"
            style={{ cursor: 'pointer' }}
            title={selectedUser || userAccountInfo.email
              ? (
                <FormattedMessage
                  defaultMessage="Continue with this donor ?"
                  id="donations.form.select.pop.donor"
                />
              )
              : (
                <FormattedMessage
                  defaultMessage="Anonymous donation ?"
                  id="donations.form.select.pop.anonymous"
                />
              )
              }
            onConfirm={this.handleSubmitUser}
          >
            <Button type="ghost" style={{ border: 'none' }}>
              <FormattedMessage defaultMessage="Next step" id="donations.form.next" />
              <Icon type="arrow-right" />
            </Button>
          </Popconfirm>
          <p style={{ margin: '16px 0 0 16px', color: 'darkgray' }}>
            <FormattedMessage
              defaultMessage=" Note: you can skip this step if the donor want to donate anonymously"
              id="donations.form.select.note"
            />
          </p>
        </div>

      </Fragment>
    );
  }
}


export default SelectDonor;
