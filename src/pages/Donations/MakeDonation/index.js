import React, { Component, Fragment } from 'react';
import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';
import {
  Steps, Spin, Icon, Modal, Button,
} from 'antd';
import { FormattedMessage } from 'react-intl';
import CasesCollection from '../../../api/Cases';
import Needs from '../../../api/Needs';
import Form from './Form';
import SelectDonor from './SelectDonor';


class MakeDonation extends Component {
  constructor({ props }) {
    super(props);
    document.title = 'Donate';
    this.state = { current: 0 };
    this.steps = [
      {
        title: <FormattedMessage defaultMessage="Select donor" id="donations.form.select.step1Title" />,
        key: 1,
        icon: <Icon type="user" />,
        content: () => (<SelectDonor goNextStep={this.NextStep} />),
      },
      {
        title: <FormattedMessage defaultMessage="Donation details" id="donations.form.select.step2Title" />,
        key: 2,
        icon: <Icon type="dollar" />,
        content: (donatedCase, caseNeeds, selectedUserId, oneNeed) => (
          <Form
            donatedCase={donatedCase}
            selectedUserId={selectedUserId}
            caseNeeds={caseNeeds}
            goPrevStep={this.PrevStep}
            oneNeed={oneNeed}
          />
        ),
      },
    ];
    this.NextStep = this.NextStep.bind(this);
    this.PrevStep = this.PrevStep.bind(this);
  }

  NextStep(selectedUserId) {
    this.setState(prev => ({ current: prev.current + 1, selectedUserId }));
  }

  PrevStep() {
    this.setState(prev => ({ current: prev.current - 1 }));
  }

  render() {
    const { current, selectedUserId } = this.state;
    const {
      donatedCase, caseNeeds, loading, history, oneNeed,
    } = this.props;
    if (loading) return <Spin size="large" style={{ margin: '20% 50%' }} />;
    return (
      <Fragment>
        <div className="view__with-back">
          <h2>
            <FormattedMessage
              values={{ value: donatedCase.title }}
              defaultMessage={`Donate to ${donatedCase.title}`}
              id="donations.form.title"
            />
          </h2>
          <Button icon="arrow-left" onClick={() => history.goBack()}>
            <FormattedMessage defaultMessage="Back" id="global.form.backButton" />
          </Button>
        </div>
        <Steps current={current} style={{ marginTop: '1rem' }}>
          {this.steps.map(item => (
            <Steps.Step
              icon={item.icon}
              key={item.key}
              title={item.title}
            />
          ))}
        </Steps>
        <div>
          {this.steps[current].content(donatedCase, caseNeeds, selectedUserId, oneNeed)}
        </div>
      </Fragment>
    );
  }
}


export default withTracker((props) => {
  const { match: { params: { id } }, history, location } = props;
  const needId = new URLSearchParams(location.search).get('needId');

  const casesHandle = Meteor.subscribe('cases.single', id);
  const needsHandle = Meteor.subscribe('cases.needs', { limit: 0, caseId: id });


  if (!casesHandle.ready() && !needsHandle.ready()) {
    return {
      loading: true,
      donatedCase: {},
    };
  }

  const qry = {};
  if (needId) {
    qry._id = needId;
  }
  const donatedCase = CasesCollection.findOne({ _id: id });

  const caseNeeds = Needs.find({
    ...qry,
    isRemoved: { $ne: true },
    isQuantifiable: true,
    status: { $ne: 'fulfilled' },
    caseId: id,
  }).fetch();


  if (caseNeeds.length === 0 && needsHandle.ready()) {
    history.replace('/cases');
    Modal.info({
      centered: true,
      keyboard: false,
      autoFocusButton: 'ok',
      title: `No needs in ${donatedCase.title}`,
      content: (
        <div>
          <ul>
            <li>This case have no needs.</li>
            <li>{'Or all it\'s needs are fulfilled.'}</li>
            <li>Or their is no quantifiable needs to donate.</li>
            <li>Or the need not found.</li>
          </ul>
          <b>{'You can\'t donate to it.'}</b>

        </div>

      ),
      onOk() {},
    });
  }
  let oneNeed = false;
  if (caseNeeds.length === 1) {
    oneNeed = true;
  }

  return {
    loading: false,
    caseNeeds,
    oneNeed,
    donatedCase,
  };
})(MakeDonation);
