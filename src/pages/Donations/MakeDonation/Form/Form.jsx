import React from 'react';
import {
  Card, message, Input, Form as AntForm, InputNumber, Select, Divider, Button, Popconfirm, Alert, Modal,
} from 'antd';
import PropTypes from 'prop-types';
import { withTracker } from 'meteor/react-meteor-data';
import { FormattedMessage } from 'react-intl';
import { withRouter } from 'react-router-dom';
import { Meteor } from 'meteor/meteor';
import ItemsCat from '../../../../api/Categories';

const donationMethods = ['Cash on door', 'Credit card'];
const donationTypes = ['money', 'items'];

const GetItem = withTracker(({ itemId }) => {
  const itemHandle = Meteor.subscribe('categories.single', itemId);
  if (!itemHandle.ready()) {
    return {
      item: {},
    };
  }
  const item = ItemsCat.findOne({ _id: itemId });
  return {
    item,
  };
})(({ item }) => (<b>{item.name}</b>));

class DonationsForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isMoneyNeedType: false,
      isItemType: false,
      need: null,
      error: undefined,
      needId: '',
      quantity: 0,
      type: '',
      notes: '',
      donationMethod: '',
      caseNeeds: [],
    };
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleQuantityChange = this.handleQuantityChange.bind(this);
    this.handleSelectChanges = this.handleSelectChanges.bind(this);
    this.handleNeedChange = this.handleNeedChange.bind(this);
    this.renderSelectedNeed = this.renderSelectedNeed.bind(this);
    this.confirmDonation = this.confirmDonation.bind(this);
    this.handlesubmit = this.handlesubmit.bind(this);
  }

  componentDidMount() {
    const {
      caseNeeds, selectedUserId, oneNeed,
    } = this.props;
    const needId = caseNeeds[0]._id;

    this.setState({
      userId: selectedUserId,
      needId: oneNeed ? needId : '',
      quantity: 0,
      notes: '',
      caseNeeds,
      donationMethod: donationMethods[0],
      type: donationTypes[0],
    });
    if (oneNeed && needId) {
      this.handleNeedChange(needId);
    }
  }


  handlesubmit = (e) => {
    const { donatedCase, history } = this.props;
    const {
      needId, quantity, caseNeeds,
      type, notes, donationMethod, userId,
    } = this.state;

    if (caseNeeds.length === 0) {
      message.error("Their is no needs in the case can't donate to nothing ");
      return;
    }
    if (!type || !donationMethod || !needId || !quantity) {
      message.error('Please fill all required fields before donate');
      return;
    }
    if (type === 'items' && donationMethod === donationMethods[1]) {
      this.setState({ error: 'Credit card donation method not allowed if the donation is items' });
      window.scrollTo(0, 0);
      return;
    }

    const donation = {
      userId,
      needId,
      quantity,
      type,
      fromOrganization: true,
      notes,
      donationMethod,
      collectionTime: {
        date: new Date(),
      },
      orgId: donatedCase.organizationId,
      isAnonymous: !userId,
    };

    Meteor.call('donations.methods.need_donate', donation, (err, res) => {
      if (err) {
        this.setState({ error: err.reason });
        // scroll to top of the page so the user can see the error
        window.scrollTo(0, 0);
      } else {
        message.success('Donation created successfully');
        history.replace(`/needs/${needId}`);
      }
    });
  }


  confirmDonation() {
    const {
      quantity, need, type, donationMethod, userId,
    } = this.state;
    const { donatedCase } = this.props;
    const content = (
      <table className="donation__table--confirm">
        <tbody>
          <tr>
            <td>Quantity</td>
            <td>
              <b style={{ color: '#2ecc71' }}>
                {quantity}
                {' '}
                {type === 'money' ? 'EGP' : 'Item'}
              </b>
            </td>
          </tr>
          <tr>
            <td>Need</td>
            <td>
              <b>
                {need.title}
              </b>
            </td>
          </tr>
          <tr>
            <td>Case</td>
            <td>
              <b>
                {donatedCase.title}
              </b>
            </td>
          </tr>
          <tr>
            <td>Donation method</td>
            <td>
              <b>
                {donationMethod}
              </b>
            </td>
          </tr>
          <tr>
            <td>Anonymous donation</td>
            <td>
              <b style={{ color: '#f39c12' }}>
                {userId ? 'No' : 'Yes'}
              </b>
            </td>
          </tr>
        </tbody>
      </table>
    );
    Modal.confirm({
      title: 'Confirm donation',
      content,
      onOk: () => (this.handlesubmit()),
      onCancel: () => {},
    });
  }

  handleInputChange(e) {
    const { name, value } = e.currentTarget;
    this.setState({ [name]: value });
  }

  handleNeedChange(needId, opt) {
    const { caseNeeds } = this.props;
    const need = caseNeeds.filter(n => (n._id === needId))[0];
    if (need.unitOfMeasure === 'EGP') {
      this.setState({ isMoneyNeedType: true });
    } else {
      this.setState({ isMoneyNeedType: false });
    }
    let defaultQuantity = 0;
    let isItemType = false;
    let type = '';
    if (need.unitOfMeasure === 'EGP') {
      defaultQuantity = need.quantityNeeded - need.quantityFulfilled;
      type = 'money';
    } else {
      defaultQuantity = need.quantityNeeded - need.quantityFulfilled;
      type = 'items';
      isItemType = true;
    }
    this.setState({
      needId, need, quantity: defaultQuantity, type, isItemType,
    });
  }

  handleQuantityChange(value) {
    this.setState({
      quantity: value,
    });
  }


  handleSelectChanges(val) {
    const { need } = this.state;
    if (!need) {
      this.setState({ error: 'Please select the need first' });
      return;
    }
    if (donationTypes.includes(val)) {
      let defaultQuantity = 0;
      let isItemType = false;
      if (val === 'money') {
        defaultQuantity = (need.quantityNeeded - need.quantityFulfilled) * need.itemPrice;
      } else {
        defaultQuantity = need.quantityNeeded;
        isItemType = true;
      }
      this.setState({
        type: val,
        quantity: defaultQuantity,
        isItemType,
        donationMethod: isItemType && 'Cash on door',
      });
    } else if (donationMethods.includes(val)) this.setState({ donationMethod: val });
  }

  renderSelectedNeed() {
    const { caseNeeds } = this.props;
    const { needId } = this.state;
    const need = caseNeeds.filter(n => (n._id === needId))[0];
    return (
      <div style={{ marginBottom: '2rem' }}>
        <Card style={{ marginTop: 16, borderRadius: 5 }}>
          <Card.Meta
            title={need.title}
            description={need.description}
          />
          <Divider>
            <FormattedMessage
              id="donations.form.details.need.title"
              defaultMessage="Need information"
            />
          </Divider>
          <table dir="auto" className="userInfo__table">
            <tbody>
              <tr>
                <td>
                  <FormattedMessage
                    id="donations.form.details.need.type"
                    defaultMessage=" The type needed"
                  />
                </td>
                <td>
                  <b>{need.unitOfMeasure === 'EGP' ? 'Money' : 'Items'}</b>
                </td>
              </tr>
              {need.unitOfMeasure === 'Item'
            && (
            <tr>
              <td>
                <FormattedMessage
                  id="donations.form.details.need.item"
                  defaultMessage=" Item needed"
                />
              </td>
              <td>
                <GetItem itemId={need.itemId} />
              </td>
            </tr>
            )
            }
              {need.unitOfMeasure === 'Item'
            && (
            <tr>
              <td>
                <FormattedMessage
                  id="donations.form.details.need.itemQuantity"
                  defaultMessage="Quantity of items needed to be fulfilled"
                />
              </td>
              <td>
                <b style={{ color: '#f39c12' }}>
                  {`${need.quantityNeeded - need.quantityFulfilled} 
                ${(need.quantityNeeded - need.quantityFulfilled) === 1 ? 'Item' : 'Items'}`}
                </b>
              </td>
            </tr>
            )
            }
              <tr>
                <td>
                  <FormattedMessage
                    id="donations.form.details.need.moneyQuantity"
                    defaultMessage=" Money needed to be fulfilled"
                  />
                </td>
                <td>
                  <b style={{ color: '#2ecc71' }}>
                    {`${(need.quantityNeeded - need.quantityFulfilled) * need.itemPrice} EGP`}
                  </b>
                </td>
              </tr>
              {need.unitOfMeasure === 'Item'
            && (
            <tr>
              <td>
                <FormattedMessage
                  id="donations.form.details.need.itemPrice"
                  defaultMessage="Item price"
                />
              </td>
              <td>
                <b>
                  {`${need.itemPrice} EGP`}
                </b>
              </td>
            </tr>
            )
            }
              <tr>
                <td>
                  <FormattedMessage
                    id="donations.form.details.need.Qfulfilled"
                    defaultMessage="Quantity fulfilled"
                  />
                </td>
                <td>
                  <b>
                    {need.unitOfMeasure === 'Item' ? `${need.quantityFulfilled} Item` : `${need.quantityFulfilled} EGP`}
                  </b>
                </td>
              </tr>
            </tbody>
          </table>
        </Card>
      </div>
    );
  }

  render() {
    const {
      needId, quantity, caseNeeds, isMoneyNeedType,
      type, notes, donationMethod, error, isItemType,
    } = this.state;
    const { goPrevStep, oneNeed } = this.props;
    return (
      <div className="donation__form">
        {error && (
        <Alert
          message={error}
          afterClose={() => this.setState({ error: '' })}
          showIcon
          banner
          closable
          type="info"
          style={{ marginBottom: '1rem' }}
        />
        )}
        <form className="flex column">
          <div className="flex row donatetion__com" style={{ margin: oneNeed && 'auto' }}>
            {/* needs */}
            {!oneNeed && (
            <AntForm.Item
              label={(
                <FormattedMessage
                  id="donations.form.needId"
                  defaultMessage=" Select a Need to donate to "
                />)}
              required
            >
              <Select
                showSearch
                style={{ width: 300 }}
                placeholder="Select a need..."
                defaultActiveFirstOption
                value={needId}
                optionFilterProp="children"
                onChange={this.handleNeedChange}
                filterOption={(input, option) => option.props.children
                  .toLowerCase().indexOf(input.toLowerCase()) >= 0}
              >
                {caseNeeds.map(n => (
                  <Select.Option
                    key={n._id}
                  >
                    {n.title}
                  </Select.Option>
                ))}
              </Select>
            </AntForm.Item>
            )}
            {needId && this.renderSelectedNeed()}
          </div>
          <div className="flex row donatetion__com">
            {/* type of donation */}
            <AntForm.Item
              label={(
                <FormattedMessage
                  id="donations.form.type"
                  defaultMessage="Donation Type"
                />)}
            >
              <Select
                style={{ width: '21rem' }}
                defaultValue={donationTypes[0]}
                name="type"
                disabled={isMoneyNeedType}
                value={isMoneyNeedType ? 'money' : (type || donationTypes[0])}
                onChange={this.handleSelectChanges}
              >
                {donationTypes.map(t => (
                  <Select.Option
                    key={t}
                    value={t}
                  >
                    {t}
                  </Select.Option>
                ))}
              </Select>
            </AntForm.Item>
            {/* quantity */}
            <AntForm.Item
              required
              label={(
                <FormattedMessage
                  id="donations.form.quantity"
                  defaultMessage="Donation Quantity"
                />)}
            >
              <InputNumber
                className="donatetion__com-size"
                value={quantity}
                onChange={this.handleQuantityChange}
              />
            </AntForm.Item>
            {/*  donation  methods */}
            <AntForm.Item
              label={(
                <FormattedMessage
                  id="donations.form.donationMethod"
                  defaultMessage="Donation Method"
                />)}
            >
              <Select
                style={{ width: '21rem' }}
                defaultValue={donationMethods[0]}
                name="donationMethod"
                disabled={isItemType}
                value={isItemType ? donationMethods[0] : donationMethod || donationMethods[0]}
                onChange={this.handleSelectChanges}
              >
                {donationMethods.map(dm => (
                  <Select.Option
                    key={dm}
                    value={dm}
                  >
                    {dm}
                  </Select.Option>
                ))}
              </Select>
            </AntForm.Item>
          </div>
          <AntForm.Item
            label={(
              <FormattedMessage
                id="donations.form.notes"
                defaultMessage="Notes"
              />)}
          >
            <Input.TextArea
              name="notes"
              value={notes}
              onChange={this.handleInputChange}
            />
          </AntForm.Item>
          <div className="flex row">
            <Popconfirm
              title={(
                <FormattedMessage
                  id="donations.form.details.popBack"
                  defaultMessage="Back the previous step ?"
                />
            )}
              onConfirm={goPrevStep}
            >
              <Button
                htmlType="button"
                icon="arrow-left"
                type="ghost"
                style={{ border: 'none', marginRight: '2rem' }}
              >
                <FormattedMessage
                  id="donations.form.details.goback"
                  defaultMessage="Go Back"
                />
              </Button>
            </Popconfirm>
            <Button
              htmlType="button"
              onClick={this.confirmDonation}
              type="primary"
              block
              icon="check"
            >
              <FormattedMessage
                id="donations.form.details.donate"
                defaultMessage="Donate"
              />
            </Button>

          </div>
        </form>
      </div>
    );
  }
}

DonationsForm.defaultProps = {
  selectedUserId: '',
  oneNeed: false,
};
DonationsForm.propTypes = {
  caseNeeds: PropTypes.array.isRequired,
  selectedUserId: PropTypes.string,
  donatedCase: PropTypes.instanceOf(Object).isRequired,
  goPrevStep: PropTypes.func.isRequired,
  oneNeed: PropTypes.bool,
  history: PropTypes.instanceOf(Object).isRequired,
};

export default withRouter(DonationsForm);
