import React, { Component, Fragment } from 'react';
import { Meteor } from 'meteor/meteor';
import { withRouter } from 'react-router-dom';
import { ReactiveVar } from 'meteor/reactive-var';
import { withTracker } from 'meteor/react-meteor-data';
import { FormattedMessage } from 'react-intl';
import PropTypes from 'prop-types';
import {
  Table, Button, Tag, Icon, message, Popconfirm,
} from 'antd';
import { CollectionsMeta } from '../../api/CollectionsMeta';
import DonationsCollection from '../../api/Donations';
// table filters and sorting prams
import { donationsFilters, searchTables } from '../../components/tablesFilters';

const PerPage = 10;
const PageNumber = new ReactiveVar(1);

// mark the donation as collected
const markAsCollected = (donation) => {
  Meteor.call('donations.methods.mark_as_collected', { donation }, (err, res) => {
    if (err) {
      message.info(err.reason);
    } else {
      message.success('Donation collected successfully');
    }
  });
};


class Donations extends Component {
  constructor(props) {
    super(props);
    // set the title of the page
    document.title = 'Donations | CrowdAid ';
    this.state = { searchText: '' };
    // bind search tables with 'this' class
    this.getColumnSearchProps = searchTables.getColumnSearchProps.bind(this);
    this.handleSearch = searchTables.handleSearch.bind(this);
    this.handleReset = searchTables.handleReset.bind(this);
  }

  render() {
    const {
      dons, pagination, loading, history, inRelatedView,
    } = this.props;
    const dataSource = dons.map(don => ({
      _id: don._id,
      caseId: don.caseId,
      needId: don.needId,
      quantity: don.quantity,
      type: don.type,
      status: don.status,
      donationMethod: don.donationMethod,
      collectionAddress: don.collectionAddress,
      collectionTime: don.collectionTime,
      createdAt: don.createdAt,
      userId: don.userId || 'فاعل خير',
      isAutomated: don.isAutomated,
      key: don._id,
    }));

    const columns = [
      {
        title: <FormattedMessage id="donations.table.user" defaultMessage="User" />,
        dataIndex: 'userId',
        key: 'userId',
        ...this.getColumnSearchProps('userId', 'users', this),
      },
      {
        title: <FormattedMessage id="donations.table.type" defaultMessage="Type" />,
        dataIndex: 'type',
        key: 'type',
        filters: donationsFilters.type,
        onFilter: (value, record) => (record.type === value),
      },
      {
        title: <FormattedMessage id="donations.table.donationMethod" defaultMessage="Donation Method" />,
        dataIndex: 'donationMethod',
        key: 'donationMethod',
        filters: donationsFilters.donationMethods,
        onFilter: (value, record) => (record.donationMethod === value),
      },
      {
        title: <FormattedMessage id="donations.table.quantity" defaultMessage="Quantity" />,
        dataIndex: 'quantity',
        key: 'quantity',
        sorter: (a, b) => a.quantity - b.quantity,
        sortDirections: ['descend', 'ascend'],
      },
      {
        title: <FormattedMessage id="donations.table.status" defaultMessage="Status" />,
        dataIndex: 'status',
        key: 'status',
        filters: donationsFilters.donationStatuses,
        onFilter: (value, record) => (record.status === value),
      },
      {
        title: <FormattedMessage id="donations.table.actions" defaultMessage="Related Cases & Needs" />,
        key: 'actions',
        render: (text, record) => {
          if (record.isRemoved) {
            return (<Tag color="blue"> Removed </Tag>);
          }
          return (
            <span>
              <Button
                className="button"
                type="default"
                shape="circle-outline"
                title="View this donation"
                icon="eye"
                onClick={() => history.push(`/donations/${record._id}`)}
              />
              {!inRelatedView
              && (
              <Fragment>
                <Button
                  className="button"
                  type="dashed"
                  title="View related case"
                  onClick={() => history.push(`/cases/${record.caseId}`)}
                >
                  <FormattedMessage id="donations.buttons.case" defaultMessage="Case" />
                </Button>
                <Button
                  className="button"
                  type="dashed"
                  title="View related need"
                  onClick={() => history.push(`/needs/${record.needId}`)}
                >
                  <FormattedMessage id="donations.buttons.need" defaultMessage="Need" />
                </Button>
              </Fragment>
              )
               }
              {record.status === 'Pending' && (
              <Popconfirm
                title={(
                  <FormattedMessage
                    id="donation.pop.collect"
                    defaultMessage="Do really want to collect this donation ?"
                  />)}
                onConfirm={() => markAsCollected(record)}
              >
                <Button
                  className="button"
                  type="default"
                  style={{ backgroundColor: '#45aaf2', color: '#fff' }}
                  shape="circle-outline"
                  title="Mark as collected"
                  icon="check"
                />
              </Popconfirm>)}
            </span>
          );
        },
      },
    ];

    return (
      <div className="dons" id="donations">
        <div className="dons__table ">
          <div className="dons__header">
            <h1>
              <Icon type="dollar" />
              {' '}
              <FormattedMessage id="donations.title" defaultMessage="Users Donations" />
            </h1>
          </div>
          <Table
            pagination={pagination}
            loading={loading}
            dataSource={dataSource}
            columns={columns}
          />
        </div>
      </div>);
  }
}
Donations.defaultProps = {
  inRelatedView: false,
};
Donations.propTypes = {
  loading: PropTypes.bool.isRequired,
  dons: PropTypes.array.isRequired,
  pagination: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
  inRelatedView: PropTypes.bool,
};

export default withTracker((props) => {
  const { needId, caseId } = props;
  const qry = {};
  // view needs donations and case donations all in one view
  if (needId) qry.needId = needId;
  if (caseId) qry.caseId = caseId;
  const donsHandle = Meteor.subscribe('DonationsTabular', {
    filter: { ...qry, status: { $nin: ['Offer', 'Unallocated'] } },
    limit: PerPage,
    skip: (PageNumber.get() - 1) * PerPage,
  });
  if (!donsHandle.ready()) {
    return ({
      loading: true,
      dons: [],
      pagination: {},
    });
  }
  const donsMeta = CollectionsMeta.findOne({ _id: 'Donations' });

  const dons = DonationsCollection.find({}, {
    sort: { status: -1, createdAt: -1 },
  });

  const totalDons = donsMeta && donsMeta.count;

  const donsExist = totalDons > 0;

  return {
    loading: false,
    dons: donsExist ? dons.fetch() : [],
    inRelatedView: !!((needId || caseId)),
    total: totalDons,
    pagination: {
      position: 'bottom',
      total: totalDons,
      defaultCurrent: 1,
      pageSize: PerPage,
      onChange(page, perPage) {
        PageNumber.set(page);
      },
    },
  };
})(withRouter(Donations));
