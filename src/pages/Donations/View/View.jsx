import React, { Component } from 'react';
import { Button, Spin } from 'antd';
import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import DonationsCollection from '../../../api/Donations';
import NeedsCollection from '../../../api/Needs';
import CasesCollection from '../../../api/Cases';

class View extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: '',
      donCase: '',
      need: '',
      user: '',
      quantity: '',
      type: '',
      status: '',
      donationMethod: '',
      collectionAddress: '',
      collectionTime: '',
      createdAt: '',
      isAutomated: '',
    };
    // set the title of the page
    document.title = 'Donations-View | CrowdAid';
  }

  componentWillReceiveProps({ ready, don, history }) {
    if (ready && don) {
      this.loadDonation(don);
    }
  }

  loadDonation(don) {
    const { date } = don.collectionTime;
    this.setState({
      id: don._id,
      donCase: don.donCase,
      need: don.need,
      user: don.user,
      quantity: don.quantity,
      type: don.type,
      status: don.status,
      donationMethod: don.donationMethod,
      collectionAddress: don.collectionAddress,
      collectionTime: `${date.toLocaleDateString()}`,
      createdAt: don.createdAt.toLocaleString(),
      isAutomated: don.isAutomated,
    });
  }

  render() {
    const {
      donCase, need, user, quantity, type,
      status, donationMethod, isAutomated,
      collectionAddress, collectionTime, createdAt,
    } = this.state;
    const { ready, history } = this.props;
    // show spinner until the subscription is ready
    if (!ready) {
      return (
        <div className="container">
          <Spin size="large" className="form__spinner" />
        </div>
      );
    }
    return (
      <div className="container">
        <div className="view__with-back">
          <h1>
            <FormattedMessage
              id="donations.view.title"
              defaultMessage="Donation view "
            />
          </h1>
          <div className="view__actions">
            <Button icon="arrow-left" onClick={() => history.goBack()}>Back</Button>
            <Button
              className="button"
              type="dashed"
              title="View related case"
              onClick={() => history.push(`/cases/${donCase._id}`)}
            >
              <FormattedMessage
                id="donations.view.buttons.viewCase"
                defaultMessage="View case"
              />
            </Button>
            <Button
              className="button"
              type="dashed"
              title="View related need"
              onClick={() => history.push(`/needs/${need._id}`)}
            >
              <FormattedMessage id="donations.view.buttons.viewNeed" defaultMessage="View need" />
            </Button>
          </div>
        </div>
        <hr />
        <div>
          <table className="view__table">
            <tbody className="view__table__body">
              <tr className="view__table__body__row">
                <td>
                  <b>
                    <FormattedMessage id="donations.view.donorEmail" defaultMessage="Donor email" />
                  </b>
                </td>
                <td>{user}</td>
              </tr>
              <tr>
                <td>
                  <b>
                    <FormattedMessage id="donations.view.case" defaultMessage="Case" />
                  </b>
                </td>
                <td>{donCase.title}</td>
              </tr>
              <tr>
                <td>
                  <b>
                    <FormattedMessage id="donations.view.need" defaultMessage="Need" />
                  </b>
                </td>
                <td>{need.title}</td>
              </tr>
              <tr>
                <td>
                  <b>
                    <FormattedMessage id="donations.view.type" defaultMessage="Type" />
                  </b>
                </td>
                <td>{type}</td>
              </tr>
              <tr>
                <td>
                  <b>
                    <FormattedMessage id="donations.view.status" defaultMessage="Status" />
                  </b>
                </td>
                <td>{status}</td>
              </tr>
              <tr>
                <td>
                  <b>
                    <FormattedMessage id="donations.view.donationMethod" defaultMessage="Donation Method" />
                  </b>
                </td>
                <td>{donationMethod}</td>
              </tr>
              <tr>
                <td>
                  <b>
                    <FormattedMessage id="donations.view.quantity" defaultMessage="Quantity" />
                  </b>
                </td>
                <td>{type === 'money' ? `${quantity} EGP` : `${quantity} Item` }</td>
              </tr>
              <tr>
                <td>
                  <b>
                    <FormattedMessage id="donations.view.collectionAddress" defaultMessage="Collection Address" />
                  </b>
                </td>
                <td>{collectionAddress}</td>
              </tr>
              <tr>
                <td>
                  <b>
                    <FormattedMessage
                      id="donations.view.collectionTime"
                      defaultMessage="Collection date and Time"
                    />
                  </b>
                </td>
                <td>{collectionTime}</td>
              </tr>
              <tr>
                <td>
                  <b>
                    <FormattedMessage id="donations.view.isAutomated" defaultMessage="Is automated donation" />
                  </b>
                </td>
                <td>{`${isAutomated ? 'Yes' : 'No'}`}</td>
              </tr>
              <tr>
                <td>
                  <b>
                    <FormattedMessage id="donations.view.createdAt" defaultMessage=" Donation date and time" />
                  </b>
                </td>
                <td>{createdAt}</td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}

View.defaultProps = {
  don: null,
};

View.propTypes = {
  ready: PropTypes.bool.isRequired,
  history: PropTypes.instanceOf(Object).isRequired,
  don: PropTypes.instanceOf(Object),
};


export default withTracker((props) => {
  const { match: { params: { id } } } = props;

  const donHandle = Meteor.subscribe('donations.single', id);
  if (!donHandle.ready()) {
    return {
      ready: false,
      don: null,
    };
  }
  const don = DonationsCollection.findOne({ _id: id });
  if (don) {
    const donCase = CasesCollection.findOne({ _id: don.caseId });
    const need = NeedsCollection.findOne({ _id: don.needId });
    const user = Meteor.users.findOne({ _id: don.userId });
    don.user = user ? user.emails[0].address : 'فاعل خير';
    don.donCase = donCase;
    don.need = need;
  }
  return {
    ready: true,
    don,
  };
})(View);
