import React, { Component } from 'react';
import {
  Layout, Menu, Icon,
} from 'antd';
import { Link, Switch, withRouter } from 'react-router-dom';
import { FormattedMessage } from 'react-intl';
import AuthRoute from '../../layout/AuthRoute';
import BasicInfo from './BasicInfo';
import Emails from './Emails';
import ChangePassword from './ChangePassword';

const {
  Content, Sider,
} = Layout;

class Profile extends Component {
  constructor(props) {
    super(props);
    // set the title of the page
    document.title = 'CrowdAid-Profile';
  }

  render() {
    const { location: { pathname } } = this.props;
    return (
      <div className="container">
        <Layout style={{ padding: '24px 0', background: '#fff' }}>
          <Sider width={200} style={{ background: '#fff' }}>
            <Menu
              mode="inline"
              defaultSelectedKeys={[pathname]}
              style={{ height: '100%' }}
            >
              <Menu.Item key="/profile">
                <Link
                  to="/profile"
                >
                  <Icon type="user" />
                  <FormattedMessage
                    id="profile.taps.basicInto"
                    defaultMessage="Profile"
                  />
                </Link>
              </Menu.Item>
              <Menu.Item key="/profile/emails">
                <Link
                  to="/profile/emails"
                >
                  <Icon type="yahoo" />
                  <FormattedMessage
                    id="profile.taps.emails"
                    defaultMessage="Emails"
                  />
                </Link>
              </Menu.Item>
              <Menu.Item key="/profile/change-password">
                <Link
                  to="/profile/change-password"
                >
                  <Icon type="unlock" />
                  <FormattedMessage
                    id="profile.taps.changePassword"
                    defaultMessage="changePassword"
                  />
                </Link>
              </Menu.Item>
            </Menu>
          </Sider>
          <Content style={{ padding: '0 24px', minHeight: 280 }}>
            <Switch>
              <AuthRoute exact path="/profile" component={BasicInfo} />
              <AuthRoute path="/profile/emails" component={Emails} />
              <AuthRoute path="/profile/change-password" component={ChangePassword} />
            </Switch>
          </Content>
        </Layout>
      </div>
    );
  }
}

export default withRouter(Profile);
