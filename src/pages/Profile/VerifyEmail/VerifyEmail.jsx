import React, { Component } from 'react';
import { Accounts } from 'meteor/accounts-base';
import { Link } from 'react-router-dom';
import { FormattedMessage } from 'react-intl';
import { Spin, Icon } from 'antd';

class VerifyEmail extends Component {
  constructor(props) {
    super(props);
    this.state = { done: false, err: '' };
    // set the title of the page
    document.title = 'Emails | CrowdAid';
  }

  componentDidMount(e) {
    // eslint-disable-next-line react/prop-types
    const { match: { params: { token } } } = this.props;
    Accounts.verifyEmail(token, ((err) => {
      if (err) this.setState(err);
      else this.setState({ done: true });
    }));
  }

  render() {
    const { done, err } = this.state;
    if (err) {
      return (
        <div className="verifyemail__verified--error">
          <FormattedMessage
            id="profile.verified.emails.error"
            defaultMessage="Something went wrong please try again"
          />
        </div>
      );
    }
    return (
      <div className="verifyemail">
        {done ? (
          <div className="verifyemail__verified">
            <Icon type="check" className="verifyemail__verified--icon" size="large" />
            {' '}
            <FormattedMessage
              id="profile.verified.emails.success"
              defaultMessage="Email verified successfully"
            />
            <Link className="verifyEmail__link" to="/profile">Back to profile</Link>
          </div>

        ) : (
          <div className="verifyemail__verifying">
            <Spin size="large" />
            {' '}
            <FormattedMessage
              id="profile.verified.emails.verifying"
              defaultMessage="verifying email..."
            />
          </div>
        )}
      </div>
    );
  }
}

export default VerifyEmail;
