import React, { Component, Fragment } from 'react';
import { Meteor } from 'meteor/meteor';
import {
  message, Spin, Modal, Input, Button, Icon, Form as AntForm, List,
} from 'antd';
import { withTracker } from 'meteor/react-meteor-data';
import { FormattedMessage } from 'react-intl';
import { validateEmail } from '../../../components/utils';


const handleRemoveEmail = (email) => {
  Modal.confirm({
    title: `Are you sure you want to remove this email ${email}`,
    onOk() {
      Meteor.call('users.methods.remove_email', { email }, (err) => {
        if (err) message.error(err.reason);
        else message.success('Email removed successfully');
      });
    },
    onCancel() {},
  });
};

class Emails extends Component {
  constructor(props) {
    super(props);
    this.state = { ready: false, emails: [], sending: { email: '', loading: false } };
    // set the title of the page
    document.title = 'Emails | CrowdAid';
    this.emailRef = React.createRef();
    this.handleAddEmail = this.handleAddEmail.bind(this);
    this.handleResendEmail = this.handleResendEmail.bind(this);
  }

  componentWillReceiveProps(props) {
    // eslint-disable-next-line react/prop-types
    const { emails, ready } = props;
    this.setState({
      emails,
      ready,
    });
  }


  handleResendEmail(email) {
    this.setState({ sending: { email, loading: true } });
    Meteor.call('users.methods.send_verification', { email }, (err) => {
      if (err) {
        this.setState({ sending: { email, loading: false } });
        message.error(err.reason);
      } else {
        this.setState({ sending: { email, loading: false } });
        message.success('Verification email resent successfully');
      }
    });
  }


  handleAddEmail(e, k) {
    e.preventDefault();
    const { emails } = this.state;
    const email = this.emailRef.current.input.value;
    if (!validateEmail(email)) {
      message.error('Please enter a valid email');
      return;
    }
    // check if he have this email already
    if (emails.filter(em => em.address === email).length !== 0) {
      message.warning('You already have this email');
      return;
    }

    Meteor.call('users.methods.add_email', { email }, (err) => {
      if (err) message.error(err.reason);
      else {
        this.emailRef.current.input.value = '';
        message.success('Email added successfully, Please verify your new email', 10);
      }
    });
  }

  render() {
    const { ready, emails, sending } = this.state;
    return (
      <div className="profile">
        <h3>
          <FormattedMessage
            id="profile.emails.title"
            defaultMessage="Emails"
          />
        </h3>
        {ready ? (
          <List
            size="small"
            bordered
            dataSource={emails}
            renderItem={email => (
              <List.Item className="emails__card">
                <table className="profile__table__emails">
                  <tbody>
                    <tr>
                      <td>{email.address}</td>
                      {email.verified
                        ? (
                          <td className="emails__true">
                            <FormattedMessage
                              id="profile.emails.verified"
                              defaultMessage="Verified"
                            />
                          </td>
                        )
                        : (
                          <td className="emails__false">
                            <Fragment>
                              <FormattedMessage
                                id="profile.emails.notVerified"
                                defaultMessage="Not verified"
                              />
                            </Fragment>
                            <Button
                              loading={email.address === sending.email && sending.loading}
                              title="Resend Email"
                              type="primary"
                              shape="circle"
                              onClick={e => this.handleResendEmail(email.address)}
                            >
                              {!(sending.loading && email.address === sending.email)
                                && <Icon type="reload" />}
                            </Button>
                          </td>
                        )
                        }
                      <td title="Remove Email">
                        <Icon
                          type="delete"
                          theme="filled"
                          className="emails__icon"
                          onClick={e => handleRemoveEmail(email.address)}
                        />
                      </td>
                    </tr>
                  </tbody>
                </table>

              </List.Item>
            )}
          />
        ) : <Spin size="large" className="form__spinner" />}
        <form className="emails__add" onSubmit={this.handleAddEmail}>
          <div className="form-group">
            <AntForm.Item
              label={(
                <FormattedMessage
                  id="profile.emails.addLable"
                  defaultMessage="Add New Email"
                />)}
              required
            >
              <Input
                prefix={<Icon type="mail" style={{ color: 'rgba(0,0,0,.25)' }} />}
                title="Enter the employee email address, To send him invitation"
                ref={this.emailRef}
                type="email"
                suffix={(
                  <Button
                    type="ghost"
                    icon="plus"
                    style={{ borderRight: 'none' }}
                    htmlType="submit"
                  >
                    <FormattedMessage
                      id="profile.emails.add"
                      defaultMessage="Add"
                    />
                  </Button>)}
                placeholder="email@example.com"
              />
            </AntForm.Item>
          </div>
        </form>
      </div>
    );
  }
}

Emails.defaultProps = {
  Emails: [],
};

export default withTracker((props) => {
  const userHandle = Meteor.subscribe('loggedUser');
  if (!userHandle.ready()) {
    return {
      ready: false,
      emails: [],
    };
  }
  return {
    ready: true,
    emails: (Meteor.user() && Meteor.user().emails) || [],
  };
})(Emails);
