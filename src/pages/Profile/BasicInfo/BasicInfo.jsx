import React, { Component } from 'react';
import { Meteor } from 'meteor/meteor';
import {
  message, Input, Form as AntForm, Spin, Button, Icon, Avatar,
} from 'antd';
import { withTracker } from 'meteor/react-meteor-data';
import { FormattedMessage } from 'react-intl';
import Files from '../../../api/Files';
import { validateNationalNum, validateMobileNum } from '../../../components/utils';

class BasicIngo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      firstName: '', lastName: '', mobile: '', idNumber: '', changed: false, avatar: '', load: false,
    };

    // set the title of the page
    document.title = 'My Profile | CrowdAid';
    // bind functions to class
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleNationalNumChange = this.handleNationalNumChange.bind(this);
    this.handleFileChange = this.handleFileChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount() {
    this.loadUser(this.props);
  }

  componentWillReceiveProps(props) {
    this.loadUser(props);
  }

  loadUser(props) {
    // eslint-disable-next-line react/prop-types
    const { user, avatar, ready } = props;
    if (ready && user) {
      this.setState({
        firstName: user.profile.firstName,
        lastName: user.profile.lastName,
        mobile: user.profile.mobile,
        idNumber: user.profile.idNumber,
        avatar,
        load: false,
      });
    } else {
      this.setState({ load: true });
    }
  }

  handleNationalNumChange(e) {
    const idNumber = e.currentTarget.value;
    if (String(idNumber).length < 15) {
      this.setState({ idNumber, changed: true });
    }
  }

  handleInputChange(e) {
    const { name } = e.currentTarget;
    const { value } = e.currentTarget;
    this.setState({ [name]: value, changed: true });
  }

  handleFileChange(e) {
    const file = e.currentTarget.files[0];
    this.uploadAvatar(file);
  }

  handleSubmit(e) {
    e.preventDefault();
    const {
      idNumber, mobile, firstName, lastName,
    } = this.state;
    // validate the National ID number
    if (!validateNationalNum(idNumber)) {
      message.error('Please enter a valid ID number');
      return;
    }
    if (mobile && !validateMobileNum(mobile)) {
      message.error('Please enter a valid mobile number');
      return;
    }
    if (firstName && firstName.length < 4) {
      message.warning('First name should be as lest 4 characters ');
      return;
    }
    if (lastName && lastName.length < 4) {
      message.warning('Last name should be as lest 4 characters ');
      return;
    }

    Meteor.call('users.methods.update_profile', { ...this.state }, (err, res) => {
      if (err) message.error(`Some this went wrong, ${err.reason}`);
      else {
        message.success('Your profile updated successfully', 10);
        this.setState({ changed: false });
      }
    });
  }

  uploadAvatar(file) {
    const { user } = this.props;
    Files.insert({
      file,
      meta: {
        namespace: 'users',
        slug: user._id,
        type: 'avatar',
      },
      onUploaded: (err, f) => {
        if (!err) {
          Meteor.call('users.methods.update_avatar', { userId: Meteor.userId(), fileId: f._id });
        }
      },
      onError: (err, f) => {
        message.error(`Error on uploading ${f.name}, ${err.reason}`);
      },
    });
  }

  render() {
    const {
      firstName, lastName, mobile, idNumber, changed, avatar, load,
    } = this.state;
    if (load) return <Spin size="large" className="form__spinner" />;
    return (
      <div className="profile">
        <h3>
          <FormattedMessage
            id="profile.basicInfo.title"
            defaultMessage="Public Profile"
          />
        </h3>
        <form dir="auto" className="flex column" onSubmit={this.handleSubmit}>
          <div className="flex row">
            <div className="flex column fill">
              <div className="form-group">
                <AntForm.Item
                  label={(
                    <FormattedMessage
                      id="profile.basicInfo.form.firstName"
                      defaultMessage="First name"
                    />)}
                >
                  <Input value={firstName} name="firstName" onChange={this.handleInputChange} />
                </AntForm.Item>
              </div>
              <div className="form-group">
                <AntForm.Item
                  label={(
                    <FormattedMessage
                      id="profile.basicInfo.form.lastName"
                      defaultMessage="Last name"
                    />)}
                >
                  <Input value={lastName} name="lastName" onChange={this.handleInputChange} />
                </AntForm.Item>
              </div>
            </div>
            <div className="profile__avatar">
              {avatar
                ? <Avatar src={avatar} alt="avatar" size={135} />
                : (
                  <Avatar
                    style={{ backgroundColor: '#6c5ce7', fontSize: '6rem', verticalAlign: 'middle' }}
                    alt="avatar"
                    size={120}
                  >
                    {firstName[0] && firstName.trim()[0].toUpperCase()}
                  </Avatar>
                )}
              <div className="profile__upload-btn-wrapper">
                <Button>
                  <Icon type="picture" />
                  <FormattedMessage
                    id="profile.basicInfo.form.avatarButtonText"
                    defaultMessage=" Upload New Avatar"
                  />
                </Button>
                <input type="file" name="files" size="20" onChange={this.handleFileChange} />
              </div>
            </div>
          </div>
          <div className="form-group">
            <AntForm.Item
              label={(
                <FormattedMessage
                  id="profile.basicInfo.form.mobile"
                  defaultMessage="Mobile number"
                />)}
            >
              <Input value={mobile} name="mobile" onChange={this.handleInputChange} />
            </AntForm.Item>
          </div>
          <div className="form-group">
            <AntForm.Item
              label={(
                <FormattedMessage
                  id="profile.basicInfo.form.idNumber"
                  defaultMessage="National ID number"
                />)}
            >
              <Input value={idNumber} name="idNumber" onChange={this.handleNationalNumChange} />
            </AntForm.Item>
          </div>
          <Button block disabled={!changed} htmlType="submit" type="primary">
            <FormattedMessage
              id="profile.basicInfo.form.saveButton"
              defaultMessage="Save"
            />
          </Button>
        </form>
      </div>
    );
  }
}

export default withTracker((props) => {
  const userHandle = Meteor.subscribe('loggedUser');
  const avatarHandle = Meteor.subscribe('files.avatar');
  if (!userHandle.ready() && !avatarHandle.ready()) {
    return {
      user: '',
    };
  }
  if (!(Meteor.user() && Meteor.user().profile)) {
    return {
      user: '',
    };
  }
  // load the user avatar
  const avatar = Files.findOne({ _id: Meteor.user().profile.avatar, 'meta.type': 'avatar' });
  return {
    ready: true,
    user: Meteor.user() || '',
    avatar: (avatar && avatar.link()),
  };
})(BasicIngo);
