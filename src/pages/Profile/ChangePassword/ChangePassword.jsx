import React, { Component } from 'react';
import { Accounts } from 'meteor/accounts-base';
import {
  Form as AntForm, Button, Input, message,
} from 'antd';
import { FormattedMessage } from 'react-intl';

class ChangePassword extends Component {
  constructor(props) {
    super(props);
    this.state = { changed: false };
    // set the title of the page
    document.title = 'Change password | CrowdAid';
    // bind functions to class
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleInputChange(e) {
    this.setState({ changed: true });
  }

  handleSubmit(e) {
    e.preventDefault();
    const oldPassword = e.target.oldPassword.value;
    const newPassword = e.target.newPassword.value;
    const newPassword2 = e.target.newPassword2.value;
    if (!oldPassword) {
      message.warning('Please enter your old password');
      return;
    }
    if (!newPassword || newPassword.length < 6) {
      message.warning('Please enter your new password at least 6 characters');
      return;
    }
    if (newPassword !== newPassword2) {
      message.error('Passwords does not match');
      return;
    }
    e.target.reset();
    Accounts.changePassword(oldPassword, newPassword, (err) => {
      if (err) message.error(err.reason);
      else message.success('Password changed successfully');
    });
    this.setState({ changed: false });
  }

  render() {
    const { changed } = this.state;
    return (
      <div className="profile">
        <h3>
          <FormattedMessage
            id="profile.changePass.title"
            defaultMessage="Change password"
          />
        </h3>
        <form dir="auto" className="flex column" onSubmit={this.handleSubmit}>
          <div className="form-group">
            <AntForm.Item
              required
              label={(
                <FormattedMessage
                  id="profile.changePass.form.oldPass"
                  defaultMessage="Old password"
                />)}
            >
              <Input required type="password" name="oldPassword" onChange={this.handleInputChange} />
            </AntForm.Item>
          </div>
          <div className="flex row">
            <div className="form-group fill">
              <AntForm.Item
                required
                label={(
                  <FormattedMessage
                    id="profile.changePass.form.newPass"
                    defaultMessage="New password"
                  />)}
              >
                <Input required type="password" name="newPassword" onChange={this.handleInputChange} />
              </AntForm.Item>
            </div>
            <div className="form-group fill">
              <AntForm.Item
                required
                label={(
                  <FormattedMessage
                    id="profile.changePass.form.newPassConfirm"
                    defaultMessage="Confirm new password"
                  />)}
              >
                <Input required type="password" name="newPassword2" onChange={this.handleInputChange} />
              </AntForm.Item>
            </div>
          </div>
          <Button block disabled={!changed} htmlType="submit" type="primary">
            <FormattedMessage
              id="profile.changePass.form.submitButton"
              defaultMessage="Update password"
            />
          </Button>
        </form>
      </div>
    );
  }
}

export default ChangePassword;
