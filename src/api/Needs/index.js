import { Mongo } from 'meteor/mongo';

const NeedsCollection = new Mongo.Collection('needs');

export default NeedsCollection;
