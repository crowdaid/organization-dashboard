import { Mongo } from 'meteor/mongo';

const Beneficiaries = new Mongo.Collection('beneficiaries');

export default Beneficiaries;
