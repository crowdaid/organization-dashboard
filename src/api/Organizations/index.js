import { Mongo } from 'meteor/mongo';

const organizations = new Mongo.Collection('organizations');

export default organizations;
