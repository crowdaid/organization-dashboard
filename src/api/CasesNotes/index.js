import { Mongo } from 'meteor/mongo';

const CasesCollection = new Mongo.Collection('casesNotes');

export default CasesCollection;
