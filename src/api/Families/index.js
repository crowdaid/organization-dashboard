import { Mongo } from 'meteor/mongo';

const Families = new Mongo.Collection('families');

export default Families;
