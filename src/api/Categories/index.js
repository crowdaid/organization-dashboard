import { Mongo } from 'meteor/mongo';

const CategoriesCollection = new Mongo.Collection('categories');

export default CategoriesCollection;
