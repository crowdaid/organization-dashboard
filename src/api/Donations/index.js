import { Mongo } from 'meteor/mongo';

const Donations = new Mongo.Collection('donations');

export default Donations;
