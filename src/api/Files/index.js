import { FilesCollection } from 'meteor/ostrio:files';
import { Meteor } from 'meteor/meteor';
import * as path from 'path';

const Files = new FilesCollection({
  collectionName: 'files',
  allowClientCode: true, // Disallow remove files from Client
  storagePath(file) {
    if (!file || !file._id) return path.join(Meteor.settings.storage.path);

    // if file is uploaded for a case
    if (file.meta.caseId) return path.join(Meteor.settings.storage.path, file.meta.namespace, file.meta.caseId, file.meta.type);

    // if file is uploaded for a case
    if (file.meta.needId) return path.join(Meteor.settings.storage.path, file.meta.namespace, file.meta.needId, file.meta.type);

    return path.join(Meteor.settings.storage.path, file.meta.namespace, file.meta.slug, file.meta.type);
  },
  onBeforeUpload(file) {
    // Allow upload files under 10MB, and only in png/jpg/jpeg formats
    if (file.size <= 1024 * 1024 * 10 && /png|jpg|jpeg|pdf|doc|docx/i.test(file.extension)) {
      return true;
    }
    return 'Please upload image, pdf, or word document, with size equal or less than 10MB';
  },
});
export default Files;
