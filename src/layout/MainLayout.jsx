import React from 'react';
import { Redirect } from 'react-router-dom';
import ProtoTypes from 'prop-types';
import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';
import { Layout, BackTop } from 'antd';
import HeaderLayout from './HeaderLayout';
import SiderLayout from './SiderLayout';
import Footer from '../components/Footer';

const { Content } = Layout;

/** Wrapper for Main routes with Navbar and main layout */
const MainLayout = ({ user, children }) => {
  if (!user) return <Redirect to="/login" />;
  return (
    <Layout style={{ minHeight: '100vh' }}>
      <SiderLayout />
      <Layout style={{ marginLeft: 200 }}>
        <HeaderLayout />
        <Content style={{ margin: '24px 16px 0', overflow: 'initial' }}>
          <div style={{ padding: 24, background: '#fff', minHeight: 360 }}>
            {children}
            <BackTop />
          </div>
        </Content>
        <Footer />
      </Layout>
    </Layout>
  );
};

MainLayout.defaultProps = {
  user: undefined,
};

MainLayout.propTypes = {
  children: ProtoTypes.oneOfType([
    ProtoTypes.arrayOf(ProtoTypes.node),
    ProtoTypes.node,
  ]).isRequired,
  user: ProtoTypes.string,
};
// redirect to login if user logout in other tab
export default withTracker(props => ({ user: Meteor.userId() }))(MainLayout);
