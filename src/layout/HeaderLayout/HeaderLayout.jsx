import React from 'react';
import PropTypes from 'prop-types';
import { withTracker } from 'meteor/react-meteor-data';
import { Meteor } from 'meteor/meteor';
import {
  Avatar, Layout, Menu, Icon, Badge, Button, Popconfirm,
} from 'antd';
import { FormattedMessage } from 'react-intl';
import ProfileMenu from '../../components/ProfileMenu';
import Files from '../../api/Files';

const { Header } = Layout;
const capitalizeUserName = name => name.replace(/\b\w/g, l => l.toUpperCase());

const LangIcon = () => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    x="0px"
    y="0px"
    width="19"
    height="19"
    viewBox="0 0 48 48"
    style={{ fill: '#000000' }}
  >
    <g id="surface1">
      <path style={{ fill: '#CFD8DC' }} d="M 15 13 L 40 13 C 41.105469 13 42 13.894531 42 15 L 42 40 C 42 41.105469 41.105469 42 40 42 L 26 42 Z " />
      <path style={{ fill: '#546E7A' }} d="M 26.832031 34.855469 L 25.914063 33.078125 L 26.804688 32.617188 C 26.867188 32.589844 32.90625 29.410156 35.847656 23.515625 L 36.292969 22.621094 L 38.085938 23.511719 L 37.636719 24.40625 C 34.394531 30.902344 27.992188 34.257813 27.722656 34.398438 Z " />
      <path style={{ fill: '#546E7A' }} d="M 38.019531 34 L 37.148438 33.511719 C 36.941406 33.394531 32.058594 30.609375 28.652344 25.84375 L 30.28125 24.679688 C 33.417969 29.074219 38.085938 31.742188 38.132813 31.769531 L 39 32.261719 Z " />
      <path style={{ fill: '#546E7A' }} d="M 26 22 L 40 22 L 40 24 L 26 24 Z " />
      <path style={{ fill: '#546E7A' }} d="M 32 20 L 34 20 L 34 24 L 32 24 Z " />
      <path style={{ fill: '#2196F3' }} d="M 33 35 L 8 35 C 6.894531 35 6 34.105469 6 33 L 6 8 C 6 6.894531 6.894531 6 8 6 L 22 6 Z " />
      <path style={{ fill: '#3F51B5' }} d="M 26 42 L 23 35 L 33 35 Z " />
      <path style={{ fill: '#FFFFFF' }} d="M 19.171875 24 L 14.8125 24 L 13.804688 27 L 11 27 L 15.765625 14 L 18.207031 14 L 23 27 L 20.195313 27 Z M 15.445313 22 L 18.546875 22 L 16.984375 17.285156 Z " />
    </g>

  </svg>
);


const changeLang = () => {
  if (localStorage.appLang === 'ar' || !localStorage.appLang) {
    localStorage.appLang = 'en';
  } else localStorage.appLang = 'ar';
  window.location.reload();
};
/** the app navbar */
const HeaderLayout = ({ userName, avatar }) => (
  <Header className="layout__header">
    <Menu
      mode="horizontal"
      theme="dark"
      style={{ lineHeight: '62px' }}
    >
      {/* change lang */}
      <Menu.SubMenu
        className="layout__header__item"
        title={(
          <Popconfirm
            icon={<Icon type="info-circle" />}
            placement="bottomRight"
            title={(
              <FormattedMessage
                id="changelange.popconfirm"
                defaultMessage="Do really want to change language ?"
              />)}
            onConfirm={changeLang}
          >
            <Button
              className="layout__header__lang-btn"
              type="ghost"
            >
              <Icon component={LangIcon} />
              <FormattedMessage id="change.lang.button" defaultMessage="Arabic" />
            </Button>
          </Popconfirm>
        )}
      />
      {/* notifications */}
      <Menu.SubMenu
        title={(
          <Badge count={1}>
            <Icon type="mail" theme="outlined" style={{ fontSize: 16 }} />
          </Badge>
        )}
        key="mail"
        className="layout__header__item"
      >
        <Menu.Item>
          <p>
            <FormattedMessage
              id="header.welcomMsg"
              defaultMessage={`Welcome! ${(userName && userName)} to crowd aid`}
              values={{ userName }}
            />
          </p>
        </Menu.Item>
      </Menu.SubMenu>
      {/* profile and organizations  */}
      <Menu.SubMenu
        className="layout__header__item"
        key="profile"
        title={(
          <span>
            {avatar
              ? <Avatar src={avatar} size="large" alt="avatar" />
              : (
                <Avatar
                  size="large"
                  style={{ backgroundColor: '#6c5ce7', fontSize: '2rem', verticalAlign: 'middle' }}
                  alt="avatar"
                >
                  {userName[0] && userName[0].toUpperCase()}
                </Avatar>
              )}
            <span style={{ fontSize: '1.5rem', marginLeft: '1rem' }}>
              {capitalizeUserName(userName)}
            </span>
            {'   '}
            <Icon type="caret-down" />
          </span>)}
      >
        <ProfileMenu />
      </Menu.SubMenu>
    </Menu>
  </Header>
);

HeaderLayout.defaultProps = {
  userName: 'Profile',
  avatar: '',
};

HeaderLayout.propTypes = {
  userName: PropTypes.string,
  avatar: PropTypes.string,
};

export default withTracker((props) => {
  const userHandle = Meteor.subscribe('loggedUser');
  const avatarHandle = Meteor.subscribe('files.avatar');
  if (!userHandle.ready() && !avatarHandle.ready()) {
    return {
      userName: '',
    };
  }
  if (!(Meteor.user() && Meteor.user().profile)) {
    return {
      userName: '',
    };
  }
  // load the user avatar
  const avatar = Files.findOne({ _id: Meteor.user().profile.avatar, 'meta.type': 'avatar' });
  const { firstName } = Meteor.user().profile;
  return {
    ready: true,
    userName: firstName.trim() || '',
    avatar: (avatar && avatar.link()) || '',
  };
})(HeaderLayout);
