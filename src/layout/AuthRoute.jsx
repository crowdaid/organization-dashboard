import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { Meteor } from 'meteor/meteor';

/** protect admin views -- only logged in users allowed */
const AuthRoute = (props) => {
  if (!Meteor.userId()) {
    return <Redirect to="/login" />;
  }
  return <Route {...props} />;
};

export default AuthRoute;
