import React from 'react';
import { Link, withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import { withTracker } from 'meteor/react-meteor-data';
import { Layout, Menu, Icon } from 'antd';
import { FormattedMessage } from 'react-intl';
import { Meteor } from 'meteor/meteor';
import { isOwner } from '../../components/utils';

const { Sider } = Layout;

/** the app navbar */
const SiderLayout = ({ location: { pathname }, owner }) => (
  <Sider
    className="layout__sider"
  >
    <div className="layout__logo">
      <Link to="/">
        <img
          src="/image/logo.png"
          style={{ borderRadius: '50%', margin: '0 .5rem' }}
          width="35"
          height="35"
          alt="crowd aid"
        />
        <FormattedMessage id="appName" defaultMessage="crowd aid" />
      </Link>
    </div>
    <Menu theme="dark" defaultSelectedKeys={[pathname]} mode="inline">
      <Menu.Item key="/">
        <Link className="layout__link" to="/">
          <Icon type="dashboard" />
          <FormattedMessage id="sideBar.dashboard" defaultMessage="Dashboard" />
        </Link>
      </Menu.Item>
      {owner
      && (
      <Menu.Item key="/staff">
        <Link className="layout__link" to="/staff">
          <Icon type="team" />
          <FormattedMessage id="sideBar.staff" defaultMessage="Staff" />
        </Link>
      </Menu.Item>
      )
      }
      <Menu.Item key="/beneficiaries">
        <Link className="layout__link" to="/beneficiaries">
          <Icon type="heart" />
          <FormattedMessage id="sideBar.beneficiaries" defaultMessage="Beneficiaries" />
        </Link>
      </Menu.Item>
      <Menu.Item key="/cases">
        <Link className="layout__link" to="/cases">
          <Icon type="solution" />
          <FormattedMessage id="sideBar.cases" defaultMessage="Cases" />
        </Link>
      </Menu.Item>
      <Menu.Item key="/donations">
        <Link className="layout__link" to="/donations">
          <Icon type="dollar" />
          <FormattedMessage id="sideBar.donations" defaultMessage="Donations" />
        </Link>
      </Menu.Item>
      <Menu.Item key="/offers">
        <Link className="layout__link" to="/offers">
          <Icon type="shop" />
          <FormattedMessage id="sideBar.offers" defaultMessage="Offers" />
        </Link>
      </Menu.Item>
    </Menu>
  </Sider>
);

SiderLayout.defaultProps = {
  owner: false,
};

SiderLayout.propTypes = {
  owner: PropTypes.bool,
  location: PropTypes.instanceOf(Object).isRequired,
};

export default
withTracker(props => ({
  owner: isOwner(Meteor.userId()).isReady && isOwner(Meteor.userId()).orgOwner,
}))(withRouter(SiderLayout));
