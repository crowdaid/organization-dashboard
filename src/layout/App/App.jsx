/* eslint-disable camelcase */
import React from 'react';
import {
  BrowserRouter, Switch, Route, Redirect,
} from 'react-router-dom';
// languages support
import { IntlProvider } from 'react-intl';
import { LocaleProvider } from 'antd';
import ar_EG from 'antd/lib/locale-provider/ar_EG';
import en_US from 'antd/lib/locale-provider/en_US';
import moment from 'moment';
import lang from '../../locales/lang';
// import for all routing components
import MainLayout from '../MainLayout';
import AuthRoute from '../AuthRoute';
import Dashboard from '../../pages/Dashboard';
import Beneficiaries from '../../pages/Beneficiaries';
import BeneficiariesCreate from '../../pages/Beneficiaries/Create';
import BeneficiariesEdit from '../../pages/Beneficiaries/Edit';
import BeneficiariesView from '../../pages/Beneficiaries/View';
import {
  CreateFamily, CreateHomeDetails,
  FamiliesTable, EditFamily, EditHomeDetails,
} from '../../pages/BeneficiariesWizard';
import CasesList from '../../pages/Cases';
import CreateCase from '../../pages/Cases/Create';
import EditCase from '../../pages/Cases/Edit';
import ViewCase from '../../pages/Cases/View';
import CaseNeeds from '../../pages/Cases/Needs';
import CaseNeedCreate from '../../pages/Cases/Needs/Create';
import EditNeed from '../../pages/Cases/Needs/Edit';
import ViewNeed from '../../pages/Cases/Needs/View';
import SignUp from '../../pages/SignUp';
import Donations from '../../pages/Donations';
import Offers from '../../pages/Offers';
import MakeDonation from '../../pages/Donations/MakeDonation';
import DonationsView from '../../pages/Donations/View';
import Login from '../../pages/Login';
import StaffList from '../../pages/Staff';
import EditEmployee from '../../pages/Staff/Edit';
import Profile from '../../pages/Profile';
import VerifyEmail from '../../pages/Profile/VerifyEmail';
import NotFound from '../../pages/NotFound';

// change app language
const appLang = localStorage.appLang || 'ar';
const antdtLang = appLang === 'ar' ? ar_EG : en_US;
moment.locale(appLang);

const App = props => (
  <IntlProvider locale={appLang} messages={lang[appLang]} defaultLocale="en">
    {/* antd provider for components languages support */}
    <LocaleProvider locale={antdtLang}>
      <BrowserRouter>
        <Switch>
          {/* authentication routes  (without navbar and footer) */}
          <Route exact path="/organizations-users/register/:token" component={SignUp} />
          <Route exact path="/login" component={Login} />
          {/* Routes for the main layout (with navbar and footer) */}
          <MainLayout>
            <Switch>
              <AuthRoute exact path="/" component={Dashboard} />
              <AuthRoute exact path="/staff" component={StaffList} />
              <AuthRoute exact path="/staff/:id([0-9a-zA-Z]{17})/edit" component={EditEmployee} />
              {/* beneficiaries routes */}
              <AuthRoute exact path="/beneficiaries/create" component={BeneficiariesCreate} />
              <AuthRoute exact path="/beneficiaries/:id([0-9a-zA-Z]{17})" component={BeneficiariesView} />
              <AuthRoute exact path="/beneficiaries" component={Beneficiaries} />
              <AuthRoute exact path="/beneficiaries/:id([0-9a-zA-Z]{17})/edit" component={BeneficiariesEdit} />
              {/* BeneficiariesWizard */}
              <AuthRoute exact path="/beneficiaries/:id([0-9a-zA-Z]{17})/family" component={FamiliesTable} />
              <AuthRoute
                exact
                path="/beneficiaries/:id([0-9a-zA-Z]{17})/family/create-family"
                component={CreateFamily}
              />
              <AuthRoute
                exact
                path="/family/:id([0-9a-zA-Z]{17})/edit"
                component={EditFamily}
              />
              <AuthRoute
                exact
                path="/beneficiaries/:id([0-9a-zA-Z]{17})/create-home-description"
                component={CreateHomeDetails}
              />
              <AuthRoute
                exact
                path="/beneficiaries/:id([0-9a-zA-Z]{17})/edit-home-description"
                component={EditHomeDetails}
              />
              {/* cases routes */}
              <AuthRoute exact path="/cases/create" component={CreateCase} />
              <AuthRoute exact path="/cases/:id([0-9a-zA-Z]{17})" component={ViewCase} />
              <AuthRoute exact path="/cases" component={CasesList} />
              <AuthRoute exact path="/cases/:id([0-9a-zA-Z]{17})/edit" component={EditCase} />
              <AuthRoute exact path="/cases/:id([0-9a-zA-Z]{17})/needs" component={CaseNeeds} />
              <AuthRoute exact path="/cases/:id([0-9a-zA-Z]{17})/needs/create" component={CaseNeedCreate} />
              {/* needs routes */}
              <AuthRoute exact path="/needs/:id([0-9a-zA-Z]{17})" component={ViewNeed} />
              <AuthRoute exact path="/needs/:id([0-9a-zA-Z]{17})/edit" component={EditNeed} />
              {/* donations routes */}
              <AuthRoute exact path="/donations" component={Donations} />
              <AuthRoute exact path="/donations/:id([0-9a-zA-Z]{17})" component={DonationsView} />
              <AuthRoute exact path="/donate/:id([0-9a-zA-Z]{17})" component={MakeDonation} />

              {/* offers routes */}
              <AuthRoute exact path="/offers" component={Offers} />
              {/* profile routes */}
              <AuthRoute path="/profile" component={Profile} />
              <AuthRoute exact path="/verify-email/:token" component={VerifyEmail} />
              <Route exact path="/404" component={NotFound} />
              <Redirect exact from="*" to="/404" />
            </Switch>
          </MainLayout>
        </Switch>
      </BrowserRouter>
    </LocaleProvider>
  </IntlProvider>
);

export default App;
