import React from 'react';
import PropTypes from 'prop-types';
import { Meteor } from 'meteor/meteor';
import { Link } from 'react-router-dom';
import {
  Menu, Icon, Spin, message,
} from 'antd';
import { FormattedMessage } from 'react-intl';
import { withTracker } from 'meteor/react-meteor-data';
import Organizations from '../../api/Organizations';

const switchOrg = (orgId, name) => {
  Meteor.call('organizations.methods.switch_organization', { orgId }, (err, res) => {
    if (err) {
      message.error('something went wrong');
    } else {
      message.success(`Switched to ${name} `);
    }
  });
};

const ProfileMenu = ({ orgs, ready, activeOrg }) => (
  <Menu
    mode="vertical"
    theme="dark"
  >
    <Menu.Item key={0}>
      <Link
        to="/profile"
        className="profile__org-link"
      >
        <Icon type="idcard" />
        <FormattedMessage id="profileMenu.myAccount" defaultMessage="My Account" />
      </Link>
    </Menu.Item>
    <Menu.Divider />
    <Menu.ItemGroup title={(
      <span>
        <Icon type="bank" />
        <span style={{ marginLeft: 10 }}>
          <FormattedMessage id="profileMenu.myOrgs" defaultMessage="My organizations" />
        </span>
      </span>)}
    >
      {ready ? orgs.map((org) => {
        if (activeOrg === org._id) {
          return (
            <Menu.Item
              key={org._id}
              disabled
            >
              <p>
                <span style={{ marginRight: 10 }}>{`${org.name.ar}`}</span>
                <Icon type="check" />
                {/* <FormattedMessage
                  id="profileMenu.myOrgs.current"
                  defaultMessage="(current)"
                /> */}
              </p>
            </Menu.Item>
          );
        }
        return (
          <Menu.Item key={org._id} onClick={() => switchOrg(org._id, org.name.en)}>
            {`${org.name.ar}`}
          </Menu.Item>

        );
      })
        : <Spin size="small" />}
    </Menu.ItemGroup>

    <Menu.Divider />
    <Menu.Item onClick={e => Meteor.logout()}>
      <p>
        <Icon type="logout" />
        <FormattedMessage id="profileMenu.logout" defaultMessage="Logout" />
      </p>
    </Menu.Item>
  </Menu>
);

ProfileMenu.defaultProps = {
  activeOrg: '',
};

ProfileMenu.propTypes = {
  ready: PropTypes.bool.isRequired,
  orgs: PropTypes.array.isRequired,
  activeOrg: PropTypes.string,
};

export default withTracker((props) => {
  const orgsHandle = Meteor.subscribe('user.organizations');
  if (!orgsHandle.ready()) {
    return {
      ready: true,
      orgs: [],
      activeOrg: '',
    };
  }
  const userOrgs = Organizations.find({}, { sort: { _id: 'DESC' } }).fetch();
  return {
    ready: true,
    orgs: userOrgs,
    activeOrg: Meteor.user() && Meteor.user().organizationId,
  };
})(ProfileMenu);
