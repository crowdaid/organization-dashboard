import React, { Component } from 'react';
import {
  Icon, Calendar, Badge, Spin, Tooltip,
} from 'antd';
import { Meteor } from 'meteor/meteor';
import { Link } from 'react-router-dom';
import { FormattedMessage } from 'react-intl';
import { withTracker } from 'meteor/react-meteor-data';
import CasesCollection from '../../api/Cases';


class CasesCalendar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fullscreen: sessionStorage.getItem('calFullscreen') === 'true',
      casesDates: [],
      loading: false,
    };
    this.enterFullscreen = this.enterFullscreen.bind(this);
    this.exitFullscreen = this.exitFullscreen.bind(this);
    this.dateCellRender = this.dateCellRender.bind(this);
  }

  componentWillReceiveProps({ loading, cases }) {
    this.setState({ casesDates: cases, loading });
  }

  getCasesDates(value) {
    const { casesDates } = this.state;
    const list = casesDates.filter(c => value.isSame(c.dueDate, 'day'));
    return list;
  }

  exitFullscreen() {
    this.setState({ fullscreen: false });
  }

  enterFullscreen() {
    this.setState({ fullscreen: true });
  }


  dateCellRender(value) {
    const caseData = this.getCasesDates(value);
    const { fullscreen } = this.state;
    if (fullscreen) {
      return (
        <ul className="events">
          {
            caseData.map(c => (
              <li title={`View ${c.title}`} key={c._id}>
                <Link to={`/cases/${c._id}`}>
                  <Badge status="processing" text={c.title} />
                </Link>
              </li>
            ))
          }
        </ul>
      );
    }
    return <p />;
  }

  render() {
    const { fullscreen, loading } = this.state;
    // set fullscreen in sessionStorage to remember option
    sessionStorage.setItem('calFullscreen', fullscreen);
    const calFullscreen = sessionStorage.getItem('calFullscreen') === 'true';

    return (
      <div className="cases-calender">
        <div className="cases-calender__note">
          <h4 style={{ color: 'gray' }}>
            <Icon type="bars" />
            {'  '}
            <FormattedMessage
              id="dashboard.calendar.lable"
              defaultMessage="Cases due dates calendar"
            />
          </h4>
          {fullscreen
            ? (
              <Icon
                className="cases-calender__note-icon"
                title="Exit Fullscreen calendar"
                type="fullscreen-exit"
                style={{ border: '1px solid #3498db', padding: '.6rem', borderRadius: 4 }}
                onClick={this.exitFullscreen}
              />
            )
            : (
              <Tooltip
                placement="left"
                title={(
                  <FormattedMessage
                    id="dashboard.calendar.enter"
                    defaultMessage="Fullscreen mood to see the cases due dates"
                  />
              )}
              >
                <Icon
                  className="cases-calender__note-icon"
                  type="fullscreen"
                  onClick={this.enterFullscreen}
                />
              </Tooltip>
            )
              }
        </div>
        {loading
          ? <Calendar mode="month" dateCellRender={this.dateCellRender} fullscreen={calFullscreen} />
          : <Spin className="cases-calender__spinner" />}
      </div>
    );
  }
}
export default withTracker((props) => {
  const casesHandle = Meteor.subscribe('CasesTabular', {
    filter: {},
    limit: 100,
  });
  if (!casesHandle.ready()) {
    return ({
      loading: false,
      cases: [],
    });
  }
  const cases = CasesCollection.find({}, {
    sort: { _id: 'DESC' },
  }).fetch();
  return ({
    loading: true,
    cases,
  });
})(CasesCalendar);
