import React from 'react';
import { Avatar } from 'antd';
import { withTracker } from 'meteor/react-meteor-data';
import { Meteor } from 'meteor/meteor';
import Files from '../../api/Files';
import Beneficiaries from '../../api/Beneficiaries';

const BenAvatar = ({ photo, shape, size }) => (
  <Avatar
    shape={shape || 'square'}
    src={photo}
    icon="user"
    alt="beneficiary image"
    size={size || 'default'}
  />
);

export default withTracker(({ benId }) => {
  const photoHandle = Meteor.subscribe('files.beneficiaries.image', benId);

  if (!photoHandle.ready()) {
    return {
      loading: true,
      photo: '',
    };
  }

  const ben = Beneficiaries.findOne({ _id: benId });

  const photo = Files.findOne({ _id: ben.photo, 'meta.type': 'photo' });

  return {
    photo: (photo && photo.link()) || '',
  };
})(BenAvatar);
