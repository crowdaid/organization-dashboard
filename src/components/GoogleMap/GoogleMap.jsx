import React from 'react';
import PropTypes from 'prop-types';
import GoogleMapReact from 'google-map-react';
import { FormattedHTMLMessage } from 'react-intl';
import { Icon } from 'antd';

const Marker = ({ lat, lng }) => (
  <div className="google-map__marker" lat={lat} lng={lng}>
    <Icon type="environment" theme="filled" />
  </div>
);
const MAPOPTIONS = maps => ({
  mapTypeId: maps.MapTypeId.ROADMAP,
});

class GoogleMap extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: '',
      center: props.point && props.point.location.coordinates,
      zoom: 11,
      draggable: true,
      lat: 30.0444,
      lng: 31.2357,
      address: '',
      geocoder: null,
    };
    this.searchBoxRef = React.createRef();
    this.handleChange = this.handleChange.bind(this);
    // eslint-disable-next-line no-underscore-dangle
    this._onPositionChange = this._onPositionChange.bind(this);
    this.onCircleInteraction = this.onCircleInteraction.bind(this);
    this.getPositionPoint = this.getPositionPoint.bind(this);
    this.onCircleInteraction = this.onCircleInteraction.bind(this);
    this.handleAddressChange = this.handleAddressChange.bind(this);
  }

  componentWillReceiveProps(newProps) {
    const { point } = newProps;
    if (point && point.address) {
      const { address, location: { coordinates } } = point;
      const lat = coordinates[0];
      const lng = coordinates[1];
      this.setState({
        address, center: [lat, lng], lat, lng,
      });
    } else {
      this.setState({ center: [30.0444, 31.2357] });
    }
  }

  onCircleInteraction(childKey, childProps, mouse) {
    this.setState({
      draggable: false,
      lat: mouse.lat,
      lng: mouse.lng,
    });
  }

  getPositionPoint(childKey, childProps, mouse) {
    this.setState({ center: [mouse.lat, mouse.lng], draggable: true });
    const { geocoder } = this.state;
    this.geocodeLatLng(geocoder, mouse);
    this.handleChange();
  }

  _onPositionChange = ({ center, zoom }) => {
    this.setState({
      center,
      zoom,
    });
  };


  handleChange = () => {
    const { onChange } = this.props;
    const {
      address, lat, lng,
    } = this.state;
    const coumingPoint = {
      address,
      location: {
        type: 'Point',
        coordinates: [lat, lng],
      },
    };

    // Update the parent
    onChange(coumingPoint);
  };

  geocodeLatLng(geocoder, mouse) {
    const latlng = { lat: mouse.lat, lng: mouse.lng };
    geocoder.geocode({ location: latlng }, (results, status) => {
      if (status === 'OK') {
        if (results[0]) {
          const address = results[0].formatted_address;
          this.setState({ address });
        }
      }
    });
  }

  handleAddressChange() {
    const address = this.searchBoxRef.current.value;
    this.setState({ address });
  }

  handleSearch(map, maps) {
    // for reverse address encoder
    const geocoder = new maps.Geocoder();
    this.setState({ geocoder });

    const searchBox = new maps.places.SearchBox(this.searchBoxRef.current);
    map.addListener('bounds_changed', () => {
      searchBox.setBounds(map.getBounds());
    });
    let markers = [];

    searchBox.addListener('places_changed', () => {
      const places = searchBox.getPlaces();
      if (places.length === 0) {
        return;
      }

      // Clear out the old markers.
      markers.forEach((marker) => {
        marker.setMap(null);
      });
      markers = [];

      // For each place, get the icon, name and location.
      const bounds = new maps.LatLngBounds();
      places.forEach((place) => {
        if (!place.geometry) {
          this.setState({ error: 'Returned place contains no geometry' });
          return;
        }
        const icon = {
          url: place.icon,
          size: new maps.Size(71, 71),
          origin: new maps.Point(0, 0),
          anchor: new maps.Point(17, 34),
          scaledSize: new maps.Size(25, 25),
        };
        const lat = place.geometry.location.lat();
        const lng = place.geometry.location.lng();
        this.setState({
          address: place.formatted_address, lat, lng, center: [lat, lng],
        });
        // change the parent point also on search
        this.handleChange();

        // Create a marker for each place.
        markers.push(
          new maps.Marker({
            map,
            icon,
            title: place.name,
            position: place.geometry.location,
          }),
        );

        if (place.geometry.viewport) {
          // Only geocodes have viewport.
          bounds.union(place.geometry.viewport);
        } else {
          bounds.extend(place.geometry.location);
        }
      });
      map.fitBounds(bounds);
    });
  }

  render() {
    const {
      lat, lng, draggable, zoom, center, address, error,
    } = this.state;
    if (!center) return <div>loading...</div>;
    if (error) return <div>{`Something went wrong, ${error}`}</div>;
    return (
      <div style={{ height: '70vh', width: '100%' }}>
        <input
          id="searchBox"
          className="google-map__search-box"
          onChange={this.handleAddressChange}
          value={address}
          placeholder="Search for place..."
          ref={this.searchBoxRef}
        />
        <p className="google-map__note">
          <FormattedHTMLMessage
            id="googleMap.note"
            defaultMessage={`
                  NOTE:  You should update your address, If
                  the location address you choose is appear as
                  <b> Unnamed Road .</b>`}
          />
        </p>
        <GoogleMapReact
          bootstrapURLKeys={{
            libraries: 'places',
            key: 'AIzaSyDKcGgshQbWd1dv2loiM0nk6z9oqxx5tJY',
          }}
          options={MAPOPTIONS}
          draggable={draggable}
          // eslint-disable-next-line no-underscore-dangle
          onChange={this._onPositionChange}
          center={center || [30.0444, 31.2357]}
          zoom={zoom}
          onChildMouseDown={this.onCircleInteraction}
          onChildMouseUp={this.getPositionPoint}
          onChildMouseMove={this.onCircleInteraction}
          onGoogleApiLoaded={({ map, maps }) => { this.handleSearch(map, maps); }}
          yesIWantToUseGoogleMapApiInternals
          defaultCenter={[30.0444, 31.2357]}
        >
          <Marker lat={lat} lng={lng} />

        </GoogleMapReact>
      </div>
    );
  }
}

GoogleMap.defaultProps = {
  onChange: () => {},
  point: null,
};

GoogleMap.propTypes = {
  onChange: PropTypes.func,
  point: PropTypes.object,
};
export default GoogleMap;

//  onGoogleApiLoaded={({map,maps})=>{this.handleSearch(map,maps)}}
// yesIWantToUseGoogleMapApiInternals={true}
