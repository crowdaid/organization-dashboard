import React from 'react';
import { Layout } from 'antd';
import { FormattedMessage } from 'react-intl';
import { version } from '../../../package.json';

const Footer = props => (
  <Layout.Footer className="footer">
    <b>
      <FormattedMessage id="footer.version" defaultMessage="Version" />
      <span className="footer__version">{version}</span>
      <p>
        <FormattedMessage
          id="footer.credits"
          defaultMessage="crowd aid ©2019 created by Nezam.io"
        />
      </p>
    </b>
  </Layout.Footer>
);

export default Footer;
