import React from 'react';
import Highlighter from 'react-highlight-words';
import {
  Input, Button, Icon,
} from 'antd';
import { FormattedMessage } from 'react-intl';
/** Tables filters and sorting params */
//  Beneficiaries filters
export const beneficiariesFilters = {

  benType: [
    { text: 'Individual', value: 'individual' },
    { text: 'Animal', value: 'animal' },
    { text: 'Group', value: 'group' },
    { text: 'Family', value: 'family' },
    { text: 'Organization', value: 'organization' },
    { text: 'Place', value: 'place' },
  ],
  benStatus: [
    { text: 'pending', value: 'pending' },
    { text: 'verified', value: 'verified' },
    { text: 'moreInfoNeeded', value: 'moreInfoNeeded' },
    { text: 'rejected', value: 'rejected' },
  ],
};
export const urgencyFlags = ['immediate', 'today', 'thisWeek', 'thisMonth', 'thisYear', 'N/A', 'setDate'];

//  Cases filters
export const casesFilters = {
  status: [
    { text: 'Draft', value: 'draft' },
    { text: 'Approved', value: 'approved' },
    { text: 'Pending', value: 'pending' },
    { text: 'Rejected', value: 'rejected' },
    { text: 'Fulfilled', value: 'fulfilled' },
    { text: 'Terminated', value: 'terminated' },
  ],
  urgency: [
    { text: 'Immediate', value: 'immediate' },
    { text: 'Today', value: 'today' },
    { text: 'This Week', value: 'thisWeek' },
    { text: 'This Month', value: 'thisMonth' },
    { text: 'This Year', value: 'thisYear' },
    { text: 'N/A', value: 'N/A' },
  ],
};
//  needs filters
export const needsFilters = {
  status: [
    { text: 'untouched', value: 'untouched' },
    { text: 'touched', value: 'touched' },
    { text: 'Fulfilled', value: 'fulfilled' },
  ],
  urgency: [
    { text: 'Immediate', value: 'immediate' },
    { text: 'Today', value: 'today' },
    { text: 'This Week', value: 'thisWeek' },
    { text: 'This Month', value: 'thisMonth' },
    { text: 'This Year', value: 'thisYear' },
    { text: 'N/A', value: 'N/A' },
  ],
  frequency: [
    { text: 'Once', value: 'Once' },
    { text: 'Monthly', value: 'Monthly' },
    { text: 'Yearly', value: 'Yearly' },
  ],
  unit: [
    { text: 'EGP', value: 'EGP' },
    { text: 'Item', value: 'Item' },
  ],
};
//  donations filters['money', 'items', 'noquantity']
export const donationsFilters = {
  donationMethods: [
    { text: 'No Money', value: 'NoMoney' },
    { text: 'Cash on door', value: 'Cash on door' },
    { text: 'Credit card', value: 'Credit card' },
  ],
  donationStatuses: [
    { text: 'Pending', value: 'Pending' },
    { text: 'Collected', value: 'Collected' },
    { text: 'Failed', value: 'Failed' },
    { text: 'Cancelled', value: 'Cancelled' },
  ],
  type: [
    { text: 'money', value: 'money' },
    { text: 'items', value: 'items' },
    { text: 'noquantity', value: 'noquantity' },
  ],
};

// search Tables
export const searchTables = {
  getColumnSearchProps: (dataIndex, title, self) => ({
    filterDropdown: ({
      // eslint-disable-next-line react/prop-types
      setSelectedKeys, selectedKeys, confirm, clearFilters,
    }) => (
      <div style={{ padding: 8 }}>
        <Input
          // eslint-disable-next-line no-param-reassign
          ref={(node) => { self.searchInput = node; }}
          placeholder={`Search ${title}`}
          value={selectedKeys[0]}
          onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
          onPressEnter={() => self.handleSearch(selectedKeys, confirm)}
          style={{ width: 188, marginBottom: 8, display: 'block' }}
        />
        <Button
          type="primary"
          onClick={() => self.handleSearch(selectedKeys, confirm)}
          icon="search"
          size="small"
          style={{ width: 90, marginRight: 8 }}
        >
          <FormattedMessage id="tablesFilters.search" defaultMessage="Search" />
        </Button>
        <Button
          onClick={() => self.handleReset(clearFilters)}
          size="small"
          style={{ width: 90 }}
        >
          <FormattedMessage id="tablesFilters.reset" defaultMessage="Reset" />
        </Button>
      </div>
    ),
    filterIcon: filtered => <Icon type="search" style={{ color: filtered ? '#1890ff' : undefined }} />,
    onFilter: (value, record) => record[dataIndex]
      .toString().toLowerCase().includes(value.toLowerCase()),
    onFilterDropdownVisibleChange: (visible) => {
      if (visible) {
        setTimeout(() => self.searchInput.select());
      }
    },
    render: text => (
      <Highlighter
        highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
        searchWords={[self.state.searchText]}
        autoEscape
        textToHighlight={(text && text.toString()) || ''}
      />
    ),
  }),

  handleSearch(selectedKeys, confirm) {
    confirm();
    this.setState({ searchText: selectedKeys[0] });
  },
  handleReset(clearFilters) {
    clearFilters();
    this.setState({ searchText: '' });
  },
};
