import {
  Map, MapControl, TileLayer, withLeaflet, Marker,
} from 'react-leaflet';
import L from 'leaflet';
import React from 'react';
import PropTypes from 'prop-types';
import { GeoSearchControl, OpenStreetMapProvider } from 'leaflet-geosearch';

import icon from './img/maker-icon.svg';

const provider = new OpenStreetMapProvider();

const markerIcon = new L.Icon({
  iconUrl: icon,
  iconRetinaUrl: icon,
  iconAnchor: null,
  popupAnchor: null,
  shadowUrl: null,
  shadowSize: null,
  shadowAnchor: null,
  iconSize: new L.Point(40, 55),
});

class Search extends MapControl {
  createLeafletElement(props) {
    this.escapingLinting = ''; // MapControl requires to implement this function not as static.
    return GeoSearchControl({
      provider,
      showMarker: false,
      showPopup: false,
      autoClose: true,
      retainZoomLevel: true,
      animateZoom: true,
      keepResult: true,
      searchLabel: 'search',
    });
  }
}

export const AddressSearch = withLeaflet(Search);

export default class MapWithAddressSearch extends React.Component {
  render() {
    const {
      initCenter, markerPosition, zoom, mapRef, handleClick, updateMarkerPosition, markerRef, readOnly,
    } = this.props;
    return (
      <div>
        { readOnly
          ? (
            <Map center={initCenter} zoom={zoom}>
              <TileLayer
                attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
              />
              <Marker
                icon={markerIcon}
                position={markerPosition}
              />
            </Map>
          )
          : (
            <Map center={initCenter} zoom={zoom} ref={mapRef} onClick={handleClick}>
              <TileLayer
                attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
              />
              <AddressSearch />
              <Marker
                icon={markerIcon}
                draggable
                onDragend={updateMarkerPosition}
                position={markerPosition}
                ref={markerRef}
              />
            </Map>
          ) }
      </div>
    );
  }
}

MapWithAddressSearch.defaultProps = {
  initCenter: [39, -104],
  markerPosition: [39, -104],
  zoom: 2,
  readOnly: false,
  mapRef: undefined,
  markerRef: undefined,
  handleClick: undefined,
  updateMarkerPosition: undefined,
};

MapWithAddressSearch.propTypes = {
  mapRef: PropTypes.object,
  markerRef: PropTypes.object,
  initCenter: PropTypes.array,
  handleClick: PropTypes.func,
  updateMarkerPosition: PropTypes.func,
  markerPosition: PropTypes.array,
  zoom: PropTypes.number,
  readOnly: PropTypes.bool,
};
