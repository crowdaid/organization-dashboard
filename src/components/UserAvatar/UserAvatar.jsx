import React from 'react';
import { Avatar } from 'antd';
import PropTypes from 'prop-types';
import { withTracker } from 'meteor/react-meteor-data';
import { Meteor } from 'meteor/meteor';
import Files from '../../api/Files';

const UserAvatar = ({ avatar, userName }) => {
  if (avatar) {
    return (
      <Avatar src={avatar} size="large" alt="avatar" />
    );
  }
  return (
    <Avatar
      size="large"
      style={{ backgroundColor: '#6c5ce7', fontSize: '2rem', verticalAlign: 'middle' }}
      alt="avatar"
    >
      {userName[0] && userName[0].toUpperCase()}
    </Avatar>
  );
};

UserAvatar.defaultProps = {
  userName: 'User',
  avatar: '',
};

UserAvatar.propTypes = {
  userName: PropTypes.string,
  avatar: PropTypes.string,
};


export default withTracker(({ userId }) => {
  const userHandle = Meteor.subscribe('loggedUser', userId);
  const avatarHandle = Meteor.subscribe('files.avatar', userId);
  if (!userHandle.ready() && !avatarHandle.ready()) {
    return {
      userName: '',
    };
  }
  const user = Meteor.users.findOne({ _id: userId });
  if (!(user && user.profile)) {
    return {
      userName: user.emails[0].address,
    };
  }
  // load the user avatar
  const avatar = Files.findOne({ _id: user.profile.avatar, 'meta.type': 'avatar' });
  const { firstName } = user.profile;
  return {
    ready: true,
    userName: firstName.trim() || '',
    avatar: (avatar && avatar.link()) || '',
  };
})(UserAvatar);
