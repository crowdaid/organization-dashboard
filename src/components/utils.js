/** this is a herper file contain helper methods like validation on emails etc... */
import { Meteor } from 'meteor/meteor';
import { Roles } from 'meteor/alanning:roles';
import { Accounts } from 'meteor/accounts-base';
import Organizations from '../api/Organizations';
// validate email
export const validateEmail = (email) => {
  const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
};

// national number
export const validateNationalNum = (nationalNum) => {
  const re = /^(2|3)[0-9][1-9][0-1][1-9][0-3][1-9]\d{7}$/;
  return re.test(String(nationalNum));
};

// mobile number
export const validateMobileNum = (mobile) => {
  const re = /^(0)(1)(0|1|2)[0-9]\d{7}$/;
  return re.test(String(mobile));
};

const checkAdmin = (userId = Meteor.userId()) => Roles.userIsInRole(userId, ['superadmin', 'admin']);
const checkSuperAdmin = (userId = Meteor.userId()) => Roles.userIsInRole(userId, ['superadmin']);

export const Authorize = (roles, opts) => {
  const userId = Meteor.userId();
  const authorized = Roles.userIsInRole(userId, roles);
  if (authorized) return true;
  if (opts === true || (opts && opts.ret)) return false;
  throw new Meteor.Error('Unauthorized', 'Your are not authorized to do this action');
};

export const isOwner = (userId) => {
  Meteor.subscribe('loggedUser');
  const orgsHandle = Meteor.subscribe('user.organizations');
  if (orgsHandle.ready()) {
    const organizationId = (Meteor.user() && Meteor.user().organizationId) || '';
    const org = Organizations.findOne({ _id: organizationId });
    const owner = Roles.userIsInRole(userId, ['owner']);
    if (owner && org && org.owner === Meteor.user().emails[0].address) {
      return { isReady: orgsHandle.ready(), orgOwner: true };
    }
    return { isReady: orgsHandle.ready(), orgOwner: false };
  }
  return { isReady: orgsHandle.ready(), orgOwner: false };
};

export const isEmployee = userId => Roles.userIsInRole(userId, ['employee']);

export const isStaff = userId => Roles.userIsInRole(userId, ['manager', 'employee']);

export const userCheck = (userId, methodName) => {
  if (!Meteor.userId()) {
    throw new Meteor.Error(
      `${methodName}.non-user`,
      'only logged in users can do this action',
    );
  }

  if (userId !== Meteor.userId() && !checkAdmin(Meteor.userId())) {
    throw new Meteor.Error(
      `${methodName}.incosistant_userId`,
      "client provided userId doesn't match server userId",
    );
  }
};

export const isAdmin = checkAdmin;
export const isSuperAdmin = checkSuperAdmin;

/**
 * check the user roles and sign him in
 * @param {String} email the user vaild email
 * @param {String} password the user password
 * @param {Array:string} as list of the user roles to check
 * @param {Function} [callback] callback after login function with error if any
 */
export const loginAs = (email, password, as, callback) => {
  Accounts.callLoginMethod({
    methodArguments: [{ pass: password, as, email }],
    userCallback(error) {
      if (error) {
        callback(error);
      } else {
        callback();
      }
    },
  });
};
